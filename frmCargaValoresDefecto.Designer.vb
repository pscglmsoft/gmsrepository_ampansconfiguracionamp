<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCargaValoresDefecto
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents cmdIndicadores As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdCerrar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCargaValoresDefecto))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.cmdIndicadores = New AxXtremeSuiteControls.AxPushButton
		Me.cmdCerrar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdIndicadores, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCerrar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Carga valores predeterminados"
		Me.ClientSize = New System.Drawing.Size(471, 174)
		Me.Location = New System.Drawing.Point(360, 328)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmCargaValoresDefecto"
		cmdIndicadores.OcxState = CType(resources.GetObject("cmdIndicadores.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdIndicadores.Size = New System.Drawing.Size(109, 27)
		Me.cmdIndicadores.Location = New System.Drawing.Point(336, 24)
		Me.cmdIndicadores.TabIndex = 2
		Me.cmdIndicadores.Name = "cmdIndicadores"
		cmdCerrar.OcxState = CType(resources.GetObject("cmdCerrar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCerrar.Size = New System.Drawing.Size(83, 27)
		Me.cmdCerrar.Location = New System.Drawing.Point(192, 140)
		Me.cmdCerrar.TabIndex = 0
		Me.cmdCerrar.Name = "cmdCerrar"
		Me.Label1.Text = "Carga definici�n indicadores"
		Me.Label1.Size = New System.Drawing.Size(301, 15)
		Me.Label1.Location = New System.Drawing.Point(20, 32)
		Me.Label1.TabIndex = 1
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 16
		Me.Line1.X2 = 75
		Me.Line1.Y1 = 130
		Me.Line1.Y2 = 130
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 16
		Me.Line2.X2 = 75
		Me.Line2.Y1 = 131
		Me.Line2.Y2 = 131
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdCerrar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdIndicadores, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(cmdIndicadores)
		Me.Controls.Add(cmdCerrar)
		Me.Controls.Add(Label1)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
