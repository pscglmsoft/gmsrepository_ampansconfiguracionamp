Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParImputCEE
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub frmParImputCEE_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmParImputCEE_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmParImputCEE_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParImputCEE_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		If txtEmpresa.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtActividadJardineria_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadJardineria.GotFocus
		XGotFocus(Me, txtActividadJardineria)
	End Sub
	
	Private Sub txtActividadJardineria_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadJardineria.DoubleClick
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informar previamente la empresa", MsgBoxStyle.Information, Me.Text)
			txtEmpresa.Focus()
			Exit Sub
		End If
		ConsultaTaula(Me, txtActividadJardineria)
	End Sub
	
	Private Sub txtActividadJardineria_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadJardineria.LostFocus
		XLostFocus(Me, txtActividadJardineria)
	End Sub
	
	Private Sub txtActividadJardineria_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtActividadJardineria.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informar previamente la empresa", MsgBoxStyle.Information, Me.Text)
			txtActividadJardineria.Text = ""
			txtEmpresa.Focus()
			GoTo EventExitSub
		End If
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtActividadManipulados_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadManipulados.GotFocus
		XGotFocus(Me, txtActividadManipulados)
	End Sub
	
	Private Sub txtActividadManipulados_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadManipulados.DoubleClick
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informar previamente la empresa", MsgBoxStyle.Information, Me.Text)
			txtEmpresa.Focus()
			Exit Sub
		End If
		ConsultaTaula(Me, txtActividadManipulados)
	End Sub
	
	Private Sub txtActividadManipulados_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadManipulados.LostFocus
		XLostFocus(Me, txtActividadManipulados)
	End Sub
	
	Private Sub txtActividadManipulados_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtActividadManipulados.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informar previamente la empresa", MsgBoxStyle.Information, Me.Text)
			txtActividadManipulados.Text = ""
			txtEmpresa.Focus()
			GoTo EventExitSub
		End If
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtActividadMantenimiento_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadMantenimiento.GotFocus
		XGotFocus(Me, txtActividadMantenimiento)
	End Sub
	
	Private Sub txtActividadMantenimiento_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadMantenimiento.DoubleClick
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informar previamente la empresa", MsgBoxStyle.Information, Me.Text)
			txtEmpresa.Focus()
			Exit Sub
		End If
		ConsultaTaula(Me, txtActividadMantenimiento)
	End Sub
	
	Private Sub txtActividadMantenimiento_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActividadMantenimiento.LostFocus
		XLostFocus(Me, txtActividadMantenimiento)
	End Sub
	
	Private Sub txtActividadMantenimiento_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtActividadMantenimiento.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informar previamente la empresa", MsgBoxStyle.Information, Me.Text)
			txtActividadMantenimiento.Text = ""
			txtEmpresa.Focus()
			GoTo EventExitSub
		End If
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
