<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParImputCEE
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtXecFiltroMonMan As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtActividadJardineria As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtActividadManipulados As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtActividadMantenimiento As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParImputCEE))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtXecFiltroMonMan = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtActividadJardineria = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtActividadManipulados = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtActividadMantenimiento = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Parametros referentes a imputaciones"
		Me.ClientSize = New System.Drawing.Size(569, 274)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-PARAMETROS CEE IMPUTACIONES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParImputCEE"
		Me.txtXecFiltroMonMan.AutoSize = False
		Me.txtXecFiltroMonMan.Size = New System.Drawing.Size(413, 103)
		Me.txtXecFiltroMonMan.Location = New System.Drawing.Point(134, 106)
		Me.txtXecFiltroMonMan.TabIndex = 11
		Me.txtXecFiltroMonMan.Tag = "5"
		Me.txtXecFiltroMonMan.AcceptsReturn = True
		Me.txtXecFiltroMonMan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtXecFiltroMonMan.BackColor = System.Drawing.SystemColors.Window
		Me.txtXecFiltroMonMan.CausesValidation = True
		Me.txtXecFiltroMonMan.Enabled = True
		Me.txtXecFiltroMonMan.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtXecFiltroMonMan.HideSelection = True
		Me.txtXecFiltroMonMan.ReadOnly = False
		Me.txtXecFiltroMonMan.Maxlength = 0
		Me.txtXecFiltroMonMan.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtXecFiltroMonMan.MultiLine = False
		Me.txtXecFiltroMonMan.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtXecFiltroMonMan.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtXecFiltroMonMan.TabStop = True
		Me.txtXecFiltroMonMan.Visible = True
		Me.txtXecFiltroMonMan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtXecFiltroMonMan.Name = "txtXecFiltroMonMan"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(62, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(134, 11)
		Me.txtEmpresa.Maxlength = 6
		Me.txtEmpresa.TabIndex = 1
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(348, 19)
		Me.Text1.Location = New System.Drawing.Point(199, 11)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtActividadJardineria.AutoSize = False
		Me.txtActividadJardineria.Size = New System.Drawing.Size(31, 19)
		Me.txtActividadJardineria.Location = New System.Drawing.Point(134, 35)
		Me.txtActividadJardineria.Maxlength = 3
		Me.txtActividadJardineria.TabIndex = 4
		Me.txtActividadJardineria.Tag = "2"
		Me.txtActividadJardineria.AcceptsReturn = True
		Me.txtActividadJardineria.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtActividadJardineria.BackColor = System.Drawing.SystemColors.Window
		Me.txtActividadJardineria.CausesValidation = True
		Me.txtActividadJardineria.Enabled = True
		Me.txtActividadJardineria.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtActividadJardineria.HideSelection = True
		Me.txtActividadJardineria.ReadOnly = False
		Me.txtActividadJardineria.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtActividadJardineria.MultiLine = False
		Me.txtActividadJardineria.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtActividadJardineria.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtActividadJardineria.TabStop = True
		Me.txtActividadJardineria.Visible = True
		Me.txtActividadJardineria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtActividadJardineria.Name = "txtActividadJardineria"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(378, 19)
		Me.Text2.Location = New System.Drawing.Point(168, 35)
		Me.Text2.TabIndex = 5
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtActividadManipulados.AutoSize = False
		Me.txtActividadManipulados.Size = New System.Drawing.Size(31, 19)
		Me.txtActividadManipulados.Location = New System.Drawing.Point(134, 59)
		Me.txtActividadManipulados.Maxlength = 3
		Me.txtActividadManipulados.TabIndex = 7
		Me.txtActividadManipulados.Tag = "3"
		Me.txtActividadManipulados.AcceptsReturn = True
		Me.txtActividadManipulados.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtActividadManipulados.BackColor = System.Drawing.SystemColors.Window
		Me.txtActividadManipulados.CausesValidation = True
		Me.txtActividadManipulados.Enabled = True
		Me.txtActividadManipulados.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtActividadManipulados.HideSelection = True
		Me.txtActividadManipulados.ReadOnly = False
		Me.txtActividadManipulados.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtActividadManipulados.MultiLine = False
		Me.txtActividadManipulados.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtActividadManipulados.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtActividadManipulados.TabStop = True
		Me.txtActividadManipulados.Visible = True
		Me.txtActividadManipulados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtActividadManipulados.Name = "txtActividadManipulados"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(378, 19)
		Me.Text3.Location = New System.Drawing.Point(168, 59)
		Me.Text3.TabIndex = 8
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtActividadMantenimiento.AutoSize = False
		Me.txtActividadMantenimiento.Size = New System.Drawing.Size(31, 19)
		Me.txtActividadMantenimiento.Location = New System.Drawing.Point(134, 83)
		Me.txtActividadMantenimiento.Maxlength = 3
		Me.txtActividadMantenimiento.TabIndex = 10
		Me.txtActividadMantenimiento.Tag = "4"
		Me.txtActividadMantenimiento.AcceptsReturn = True
		Me.txtActividadMantenimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtActividadMantenimiento.BackColor = System.Drawing.SystemColors.Window
		Me.txtActividadMantenimiento.CausesValidation = True
		Me.txtActividadMantenimiento.Enabled = True
		Me.txtActividadMantenimiento.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtActividadMantenimiento.HideSelection = True
		Me.txtActividadMantenimiento.ReadOnly = False
		Me.txtActividadMantenimiento.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtActividadMantenimiento.MultiLine = False
		Me.txtActividadMantenimiento.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtActividadMantenimiento.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtActividadMantenimiento.TabStop = True
		Me.txtActividadMantenimiento.Visible = True
		Me.txtActividadMantenimiento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtActividadMantenimiento.Name = "txtActividadMantenimiento"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(378, 19)
		Me.Text4.Location = New System.Drawing.Point(168, 83)
		Me.Text4.TabIndex = 12
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(385, 241)
		Me.cmdAceptar.TabIndex = 13
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(475, 241)
		Me.cmdGuardar.TabIndex = 14
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label2.Text = "Retorna 'W' 0 si monitor v�lido, 1 si no v�lido. Espera variable 'Personal' con NIF personal"
		Me.Label2.ForeColor = System.Drawing.Color.FromARGB(0, 0, 128)
		Me.Label2.Size = New System.Drawing.Size(435, 15)
		Me.Label2.Location = New System.Drawing.Point(122, 212)
		Me.Label2.TabIndex = 17
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Xecute filtro personal monitor manipulados"
		Me.Label1.Size = New System.Drawing.Size(121, 75)
		Me.Label1.Location = New System.Drawing.Point(10, 108)
		Me.Label1.TabIndex = 16
		Me.Label1.Tag = "5"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(121, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Actividad jardiner�a"
		Me.lbl2.Size = New System.Drawing.Size(121, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 39)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Actividad manipulados"
		Me.lbl3.Size = New System.Drawing.Size(121, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 63)
		Me.lbl3.TabIndex = 6
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Actividad mantenimiento"
		Me.lbl4.Size = New System.Drawing.Size(121, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 87)
		Me.lbl4.TabIndex = 9
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 240)
		Me.lblLock.TabIndex = 15
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 234
		Me.Line1.Y2 = 234
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 235
		Me.Line2.Y2 = 235
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtXecFiltroMonMan)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtActividadJardineria)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtActividadManipulados)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtActividadMantenimiento)
		Me.Controls.Add(Text4)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
