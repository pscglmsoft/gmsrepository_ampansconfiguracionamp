Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParametresERP
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdExplorar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExplorar.ClickEvent
		MDI.CommonDialog1Open.ShowDialog()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmParametresERP.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmParametresERP_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		XGotFocusForm(Me)
	End Sub
	
	Private Sub frmParametresERP_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		ResetForm(Me)
	End Sub
	
	Private Sub frmParametresERP_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmdExplorar.Picture = frmImatges.IcoImageList.Images.Item("Explorer")
	End Sub
	
	Private Sub frmParametresERP_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParametresERP_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub txtArticle_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArticle.DoubleClick
		ConsultaTaula(Me, txtArticle)
	End Sub
	
	Private Sub txtArticle_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArticle.GotFocus
		XGotFocus(Me, txtArticle)
	End Sub
	
	Private Sub txtArticle_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArticle.LostFocus
		XLostFocus(Me, txtArticle)
	End Sub
	
	Private Sub txtArticle_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtArticle.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtArticle)
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtClient_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClient.DoubleClick
		If txtEmpresa.Text = "" Then Exit Sub
		ConsultaTaula(Me, txtClient)
	End Sub
	
	Private Sub txtClient_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClient.GotFocus
		XGotFocus(Me, txtClient)
	End Sub
	
	Private Sub txtClient_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClient.LostFocus
		XLostFocus(Me, txtClient)
	End Sub
	
	Private Sub txtClient_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtClient.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtClient)
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtMarges_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMarges.DoubleClick
		If txtEmpresa.Text = "" Then Exit Sub
		ConsultaTaula(Me, txtMarges,  , 27 & S & "M")
	End Sub
	
	Private Sub txtMarges_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMarges.GotFocus
		XGotFocus(Me, txtMarges)
	End Sub
	
	Private Sub txtMarges_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMarges.LostFocus
		XLostFocus(Me, txtMarges)
	End Sub
	
	Private Sub txtMarges_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMarges.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtMarges, 27 & S & "M")
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPathFicheros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFicheros.GotFocus
		XGotFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathFicheros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFicheros.LostFocus
		Call XLostFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtSubactivitat_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubactivitat.DoubleClick
		If txtEmpresa.Text = "" Then Exit Sub
		ConsultaTaula(Me, txtSubactivitat,  , "27" & S & "S")
	End Sub
	
	Private Sub txtSubactivitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubactivitat.GotFocus
		XGotFocus(Me, txtSubactivitat)
	End Sub
	
	Private Sub txtSubactivitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubactivitat.LostFocus
		XLostFocus(Me, txtSubactivitat)
	End Sub
	
	Private Sub txtSubactivitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubactivitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, "27" & S & "S")
		If Cancel = True Then
			SelTxT(Me)
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtsucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtsucursal.DoubleClick
		If txtClient.Text = "" Then Exit Sub
		ConsultaTaula(Me, txtsucursal,  , txtClient.Text)
	End Sub
	
	Private Sub txtsucursal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtsucursal.GotFocus
		XGotFocus(Me, txtsucursal)
	End Sub
	
	Private Sub txtsucursal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtsucursal.LostFocus
		XLostFocus(Me, txtsucursal)
	End Sub
	
	Private Sub txtsucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtsucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtsucursal, txtClient.Text)
		eventArgs.Cancel = Cancel
	End Sub
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
End Class
