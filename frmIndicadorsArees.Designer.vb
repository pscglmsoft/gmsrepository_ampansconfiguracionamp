<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadorsArees
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents grdActivitats As FlexCell.Grid
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcio As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadorsArees))
		Me.components = New System.ComponentModel.Container()
		Me.grdActivitats = New FlexCell.Grid
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtDescripcio = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True

		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Indicadors arees"
		Me.ClientSize = New System.Drawing.Size(492, 319)
		Me.Location = New System.Drawing.Point(351, 269)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORS AREES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadorsArees"

		Me.grdActivitats.Size = New System.Drawing.Size(469, 201)
		Me.grdActivitats.Location = New System.Drawing.Point(10, 68)
Me.grdActivitats.BorderStyle = FlexCell.BorderStyleEnum.FixedSingle
		Me.grdActivitats.TabIndex = 7
		Me.grdActivitats.Name = "grdActivitats"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(31, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 11)
		Me.txtCodi.Maxlength = 2
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtDescripcio.AutoSize = False
		Me.txtDescripcio.Size = New System.Drawing.Size(336, 19)
		Me.txtDescripcio.Location = New System.Drawing.Point(124, 35)
		Me.txtDescripcio.Maxlength = 30
		Me.txtDescripcio.TabIndex = 3
		Me.txtDescripcio.Tag = "2"
		Me.txtDescripcio.AcceptsReturn = True
		Me.txtDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcio.CausesValidation = True
		Me.txtDescripcio.Enabled = True
		Me.txtDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcio.HideSelection = True
		Me.txtDescripcio.ReadOnly = False
		Me.txtDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcio.MultiLine = False
		Me.txtDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcio.TabStop = True
		Me.txtDescripcio.Visible = True
		Me.txtDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcio.Name = "txtDescripcio"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(299, 279)
		Me.cmdAceptar.TabIndex = 4
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(389, 279)
		Me.cmdGuardar.TabIndex = 5
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 39)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(12, 278)
		Me.lblLock.TabIndex = 6
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()

		Me.Controls.Add(grdActivitats)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtDescripcio)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lblLock)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
