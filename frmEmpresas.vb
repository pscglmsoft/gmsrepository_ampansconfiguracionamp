Option Strict Off
Option Explicit On
Friend Class frmEmpresas
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub FGetReg()
		DisplayDadesEmpresa()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub chkIRPF_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIRPF.GotFocus
		XGotFocus(Me, chkIRPF)
	End Sub
	
	Private Sub chkIRPF_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIRPF.LostFocus
		XLostFocus(Me, chkIRPF)
	End Sub
	
	Private Sub chkIRPF_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles chkIRPF.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, chkIRPF)
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbClaseIvaCli_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbClaseIvaCli.GotFocus
		XGotFocus(Me, cmbClaseIvaCli)
	End Sub
	
	Private Sub cmbClaseIvaCli_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbClaseIvaCli.LostFocus
		XLostFocus(Me, cmbClaseIvaCli)
	End Sub
	
	Private Sub cmbIVA_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbIVA.GotFocus
		XGotFocus(Me, cmbIVA)
	End Sub
	
	Private Sub cmbIVA_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbIVA.LostFocus
		XLostFocus(Me, cmbIVA)
	End Sub
	
	Private Sub cmbTipoSerieVen_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipoSerieVen.GotFocus
		XGotFocus(Me, cmbTipoSerieVen)
	End Sub
	
	Private Sub cmbTipoSerieVen_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipoSerieVen.LostFocus
		XLostFocus(Me, cmbTipoSerieVen)
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmEmpresas_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub frmEmpresas_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		XGotFocusForm(Me)
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		CreaMatriuCombosForm(Me, "cmbMonedaBase,cmbIdiomaPredeterminado")
		CarregaCombosMatriu()
	End Sub
	
	Private Sub frmEmpresas_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmEmpresas_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodigoEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoEmpresa.GotFocus
		XGotFocus(Me, txtCodigoEmpresa)
	End Sub
	
	Private Sub txtCodigoEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoEmpresa.DoubleClick
		ConsultaTaula(Me, txtCodigoEmpresa)
	End Sub
	
	Private Sub txtCodigoEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoEmpresa.LostFocus
		If txtCodigoEmpresa.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigoEmpresa)
		If ABM = "AL" Then DisplayDadesEmpresa()
	End Sub
	
	Private Sub txtNombreEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreEmpresa.GotFocus
		XGotFocus(Me, txtNombreEmpresa)
	End Sub
	
	Private Sub txtNombreEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreEmpresa.LostFocus
		XLostFocus(Me, txtNombreEmpresa)
	End Sub
	
	Private Sub txtNombreFiscal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreFiscal.GotFocus
		XGotFocus(Me, txtNombreFiscal)
	End Sub
	
	Private Sub txtNombreFiscal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreFiscal.LostFocus
		XLostFocus(Me, txtNombreFiscal)
	End Sub
	
	Private Sub txtDireccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccion.GotFocus
		XGotFocus(Me, txtDireccion)
	End Sub
	
	Private Sub txtDireccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccion.LostFocus
		XLostFocus(Me, txtDireccion)
	End Sub
	
	Private Sub cmbPerfilUsuarios_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPerfilUsuarios.GotFocus
		XGotFocus(Me, cmbPerfilUsuarios)
	End Sub
	
	Private Sub cmbPerfilUsuarios_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPerfilUsuarios.LostFocus
		XLostFocus(Me, cmbPerfilUsuarios)
	End Sub
	
	Private Sub txtPoblacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.GotFocus
		XGotFocus(Me, txtPoblacion)
	End Sub
	
	Private Sub txtPoblacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.LostFocus
		XLostFocus(Me, txtPoblacion)
	End Sub
	
	
	Private Sub txtProvincia_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProvincia.GotFocus
		XGotFocus(Me, txtProvincia)
	End Sub
	
	Private Sub txtProvincia_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProvincia.LostFocus
		XLostFocus(Me, txtProvincia)
	End Sub
	
	Private Sub txtNif_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNif.GotFocus
		XGotFocus(Me, txtNif)
	End Sub
	
	Private Sub txtNif_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNif.LostFocus
		XLostFocus(Me, txtNif)
	End Sub
	
	Private Sub txtSerie_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSerie.GotFocus
		XGotFocus(Me, txtSerie)
	End Sub
	
	Private Sub txtSerie_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSerie.LostFocus
		XLostFocus(Me, txtSerie)
	End Sub
	
	Private Sub txtSerie_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSerie.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtSerie)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSubdireccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubdireccion.GotFocus
		XGotFocus(Me, txtSubDireccion)
	End Sub
	
	Private Sub txtSubdireccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubdireccion.LostFocus
		XLostFocus(Me, txtSubDireccion)
	End Sub
	
	Private Sub txtSubdireccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubdireccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTelefono_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefono.GotFocus
		XGotFocus(Me, txtTelefono)
	End Sub
	
	Private Sub txtTelefono_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefono.LostFocus
		XLostFocus(Me, txtTelefono)
	End Sub
	
	Private Sub txtFax_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFax.GotFocus
		XGotFocus(Me, txtFax)
	End Sub
	
	Private Sub txtFax_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFax.LostFocus
		XLostFocus(Me, txtFax)
	End Sub
	
	Private Sub txtEmail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.GotFocus
		XGotFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.LostFocus
		XLostFocus(Me, txtEmail)
	End Sub
	
	Private Sub cmbMonedaBase_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbMonedaBase.GotFocus
		XGotFocus(Me, cmbMonedaBase)
	End Sub
	
	Private Sub cmbMonedaBase_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbMonedaBase.LostFocus
		XLostFocus(Me, cmbMonedaBase)
	End Sub
	
	Private Sub cmbIdiomaPredeterminado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbIdiomaPredeterminado.GotFocus
		XGotFocus(Me, cmbIdiomaPredeterminado)
	End Sub
	
	Private Sub cmbIdiomaPredeterminado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbIdiomaPredeterminado.LostFocus
		XLostFocus(Me, cmbIdiomaPredeterminado)
	End Sub
	
	Private Sub txtUrl_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUrl.GotFocus
		XGotFocus(Me, txtUrl)
	End Sub
	
	Private Sub txtUrl_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUrl.LostFocus
		XLostFocus(Me, txtUrl)
	End Sub
	
	Private Sub txtUrl_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUrl.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub DisplayDadesEmpresa()
		Dim RegistreEmpresa As String
		If txtCodigoEmpresa.Text = "" Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtCodigoEmpresa.Text
		RegistreEmpresa = CacheXecute("S VALUE=^GEMPRESA(P1)")
		'txtNombreEmpresa.Text = Piece(RegistreEmpresa, S, 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtNombreFiscal.Text = Piece(RegistreEmpresa, S, 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtDireccion.Text = Piece(RegistreEmpresa, S, 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtSubDireccion.Text = Piece(RegistreEmpresa, S, 4)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtPoblacion.Text = Piece(RegistreEmpresa, S, 5)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtProvincia.Text = Piece(RegistreEmpresa, S, 6)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtNif.Text = Piece(RegistreEmpresa, S, 7)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTelefono.Text = Piece(RegistreEmpresa, S, 8)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtFax.Text = Piece(RegistreEmpresa, S, 9)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtEmail.Text = Piece(RegistreEmpresa, S, 10)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtUrl.Text = Piece(RegistreEmpresa, S, 11)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SituaCombo(cmbMonedaBase, Piece(RegistreEmpresa, S, 12))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SituaCombo(cmbIdiomaPredeterminado, Piece(RegistreEmpresa, S, 13))
	End Sub
End Class
