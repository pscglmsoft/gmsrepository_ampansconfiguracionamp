Option Strict Off
Option Explicit On
Friend Class frmIndicadorsArees
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub FGetReg()
		DisplayActivitats()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub frmIndicadorsArees_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadorsArees_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		DefinirGrids()
	End Sub
	
	Private Sub frmIndicadorsArees_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmIndicadorsArees_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub grdActivitats_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdActivitats.DoubleClick
		If grdActivitats.MouseRow < 1 Then Exit Sub
		If grdActivitats.ActiveCell.Row < 1 Then Exit Sub
		With frmIndicadorsDeActivitats
			.Area = txtCodi.Text
			.FlagExtern = True
			.txtActivitat.Text = grdActivitats.Cell(grdActivitats.ActiveCell.Row, 1).Text
			.Show()
			.ABM = GetReg(frmIndicadorsDeActivitats)
		End With
	End Sub
	
	Private Sub grdActivitats_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdActivitats.MouseUp
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdActivitats", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Modificar",  , grdActivitats.ActiveCell.Row > 1, XPIcon("Edit"),  ,  ,  ,  , "Mod")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou",  , txtCodi.Text <> "", XPIcon("New"),  ,  ,  ,  , "New")
			End With
		End With
		XpExecutaMenu(Me, "grdActivitats")
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub DefinirGrids()
		IniciFGrid(grdActivitats, False)
		With grdActivitats
			.Cols = 4
			.Rows = 1
			.SelectionMode = SelectionModeEnum.ByRow
			
			.Cell(0, 2).Text = "Codi"
			.Cell(0, 3).Text = "Descripci�"
			
			.Column(1).CellType = FlexCell.CellTypeEnum.TextBox
			.Column(2).CellType = FlexCell.CellTypeEnum.TextBox
			
			.Column(2).Alignment = AlignmentEnum.CenterCenter
			
			.Column(0).Width = 0
			.Column(1).Width = 0
			.Column(2).Width = 60
			.Column(3).Width = 60
			
		End With
		FGCentrarTitolsColumnes(grdActivitats)
	End Sub
	
	Private Sub DisplayActivitats()
		CarregaFGrid(grdActivitats, "GACT^ENT.G.INDIAUX", txtCodi.Text)
	End Sub
	
	'UPGRADE_NOTE: Menu se actualiz� a Menu_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub ResMenu(ByRef Menu As String, ByRef Key As String)
		Select Case Menu
			Case "grdActivitats"
				Select Case Key
					Case "Mod"
grdActivitats_DoubleClick(grdActivitats, New System.EventArgs())
					Case "New"
						With frmIndicadorsDeActivitats
							.Area = txtCodi.Text
							.FlagExtern = True
							.Show()
							.txtArea.Enabled = False
						End With
				End Select
		End Select
	End Sub
End Class
