<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParamtresVaris
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtUsuario As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtColaborador As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents chkPuedeGestionar As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtFechaBaja As DataControl.GmsData
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParamtresVaris))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtUsuario = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtColaborador = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.chkPuedeGestionar = New AxXtremeSuiteControls.AxCheckBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.txtFechaBaja = New DataControl.GmsData
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkPuedeGestionar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()

		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Paramtres varis"
		Me.ClientSize = New System.Drawing.Size(486, 190)
		Me.Location = New System.Drawing.Point(352, 288)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "U-PARAMETRES_VARIS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParamtresVaris"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(42, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(124, 11)
		Me.txtEmpresa.Maxlength = 4
		Me.txtEmpresa.TabIndex = 1
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(259, 19)
		Me.Text1.Location = New System.Drawing.Point(169, 11)
		Me.Text1.TabIndex = 7
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtUsuario.AutoSize = False
		Me.txtUsuario.Size = New System.Drawing.Size(93, 19)
		Me.txtUsuario.Location = New System.Drawing.Point(124, 35)
		Me.txtUsuario.Maxlength = 15
		Me.txtUsuario.TabIndex = 2
		Me.txtUsuario.Tag = "*2"
		Me.txtUsuario.AcceptsReturn = True
		Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuario.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuario.CausesValidation = True
		Me.txtUsuario.Enabled = True
		Me.txtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuario.HideSelection = True
		Me.txtUsuario.ReadOnly = False
		Me.txtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuario.MultiLine = False
		Me.txtUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuario.TabStop = True
		Me.txtUsuario.Visible = True
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuario.Name = "txtUsuario"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(259, 19)
		Me.Text2.Location = New System.Drawing.Point(220, 35)
		Me.Text2.TabIndex = 9
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtColaborador.AutoSize = False
		Me.txtColaborador.Size = New System.Drawing.Size(93, 19)
		Me.txtColaborador.Location = New System.Drawing.Point(124, 59)
		Me.txtColaborador.Maxlength = 15
		Me.txtColaborador.TabIndex = 3
		Me.txtColaborador.Tag = "3"
		Me.txtColaborador.AcceptsReturn = True
		Me.txtColaborador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtColaborador.BackColor = System.Drawing.SystemColors.Window
		Me.txtColaborador.CausesValidation = True
		Me.txtColaborador.Enabled = True
		Me.txtColaborador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtColaborador.HideSelection = True
		Me.txtColaborador.ReadOnly = False
		Me.txtColaborador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtColaborador.MultiLine = False
		Me.txtColaborador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtColaborador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtColaborador.TabStop = True
		Me.txtColaborador.Visible = True
		Me.txtColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtColaborador.Name = "txtColaborador"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(259, 19)
		Me.Text3.Location = New System.Drawing.Point(220, 59)
		Me.Text3.TabIndex = 11
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		chkPuedeGestionar.OcxState = CType(resources.GetObject("chkPuedeGestionar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPuedeGestionar.Size = New System.Drawing.Size(157, 19)
		Me.chkPuedeGestionar.Location = New System.Drawing.Point(10, 83)
		Me.chkPuedeGestionar.TabIndex = 4
Me.chkPuedeGestionar.Tag = "4"
		Me.chkPuedeGestionar.Name = "chkPuedeGestionar"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(293, 157)
		Me.cmdAceptar.TabIndex = 6
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(383, 157)
		Me.cmdGuardar.TabIndex = 12
		Me.cmdGuardar.Name = "cmdGuardar"

		Me.txtFechaBaja.Size = New System.Drawing.Size(89, 20)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 106)
		Me.txtFechaBaja.TabIndex = 5
Me.txtFechaBaja.Tag = "5"
		Me.txtFechaBaja.Name = "txtFechaBaja"
		Me.lbl4.Text = "Fecha baja"
		Me.lbl4.Size = New System.Drawing.Size(75, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 110)
		Me.lbl4.TabIndex = 14
		Me.lbl4.Tag = "5"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Usuario"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 39)
		Me.lbl2.TabIndex = 8
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Colaborador"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 63)
		Me.lbl3.TabIndex = 10
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 156)
		Me.lblLock.TabIndex = 13
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 150
		Me.Line1.Y2 = 150
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 151
		Me.Line2.Y2 = 151
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"

		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPuedeGestionar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtUsuario)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtColaborador)
		Me.Controls.Add(Text3)
		Me.Controls.Add(chkPuedeGestionar)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
