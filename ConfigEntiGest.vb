Option Strict Off
Option Explicit On
Module configEntiGest
	
	Public Sub VarEmpresa(Optional ByRef Configurador As Boolean = False)
		CacheNetejaParametres()
		MCache.PDELIM = vbNullChar
		MCache.P1 = EMP
		MCache.P2 = tv
		CacheXecute("D INI^ENT.G.VAR")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Vars.RegEmpresa = Piece(MCache.PLIST, vbNullChar, 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Vars.RegEmpresaModul = Piece(MCache.PLIST, vbNullChar, 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Vars.RegEmpresaSistema = Piece(MCache.PLIST, vbNullChar, 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Vars.RegParaGMS = Piece(MCache.PLIST, vbNullChar, 4)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Vars.RegParaAplModul = Piece(MCache.PLIST, vbNullChar, 5)
		
		If Modul = "ENT.CET" Then
			CacheNetejaParametres()
			CacheXecute("D INI^EINICI") 'Carrega les taules de l'EKON de nom�s consulta.
		End If
	End Sub
End Module
