Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmIndicadoresParametrizacionGrupos
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	Private intNumParam As Short
	
	Public Overrides Sub FGetReg()
		AmagaEtiquetes()
		HabilitaParametres()
	End Sub
	Private Sub HabilitaParametres()
		Dim strResult As String
		CacheNetejaParametres()
		MCache.P1 = txtGruposIndicadores.Text
		strResult = CacheXecute("S VALUE=^IND(P1)")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		intNumParam = Val(Piece(strResult, S, 8))
		If intNumParam = 0 Then Exit Sub
		Select Case intNumParam
			Case 1
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP1.Text = Piece(strResult, S, 4) : txtParametro1.Enabled = True : txtParametro1.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
			Case 2
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP1.Text = Piece(strResult, S, 4) : txtParametro1.Enabled = True : txtParametro1.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP2.Text = Piece(strResult, S, 5) : txtParametro2.Enabled = True : txtParametro2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
			Case 3
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP1.Text = Piece(strResult, S, 4) : txtParametro1.Enabled = True : txtParametro1.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP2.Text = Piece(strResult, S, 5) : txtParametro2.Enabled = True : txtParametro2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP3.Text = Piece(strResult, S, 6) : txtParametro3.Enabled = True : txtParametro3.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
			Case 4
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP1.Text = Piece(strResult, S, 4) : txtParametro1.Enabled = True : txtParametro1.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP2.Text = Piece(strResult, S, 5) : txtParametro2.Enabled = True : txtParametro2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP3.Text = Piece(strResult, S, 6) : txtParametro3.Enabled = True : txtParametro3.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				labelP4.Text = Piece(strResult, S, 7) : txtParametro4.Enabled = True : txtParametro4.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorEnabled)
		End Select
		txtParametro1.Focus()
	End Sub
	Private Sub AmagaEtiquetes()
		labelP1.Text = "Par�metro 1" : txtParametro1.Enabled = False : txtParametro1.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
		labelP2.Text = "Par�metro 2" : txtParametro2.Enabled = False : txtParametro2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
		labelP3.Text = "Par�metro 3" : txtParametro3.Enabled = False : txtParametro3.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
		labelP4.Text = "Par�metro 4" : txtParametro4.Enabled = False : txtParametro4.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
		
	End Sub
	Public Overrides Sub Inici()
		ResetForm(Me)
		AmagaEtiquetes()
	End Sub
	
	Private Sub cmdCopiaEmp_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCopiaEmp.ClickEvent
		If txtEmpresa.Text = "" Or txtGruposIndicadores.Text = "" Then Exit Sub
		If Not GravaRegistre(Me) Then Exit Sub
		If xMsgBox("�Est� seguro de sobreescribir estos datos para todas las empresas?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.No Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtEmpresa.Text
		MCache.P2 = txtGruposIndicadores.Text
		CacheXecute("D DUPPGI^ENT.G.INDIAUX")
		xMsgBox("Proceso finalizado", MsgBoxStyle.Information, Me.Text)
	End Sub
	
	Private Sub frmIndicadoresParametrizacionGrupos_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		'  Select Case intNumParam
		'    Case 1:
		'      If txtParametro1 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 1", vbInformation, Me.Caption
		'        txtParametro1.SetFocus
		'        Exit Sub
		'      End If
		'    Case 2:
		'      If txtParametro1 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 1", vbInformation, Me.Caption
		'        txtParametro1.SetFocus
		'        Exit Sub
		'      End If
		'      If txtParametro2 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 2", vbInformation, Me.Caption
		'        txtParametro2.SetFocus
		'        Exit Sub
		'      End If
		'    Case 3:
		'      If txtParametro1 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 1", vbInformation, Me.Caption
		'        txtParametro1.SetFocus
		'        Exit Sub
		'      End If
		'      If txtParametro2 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 2", vbInformation, Me.Caption
		'        txtParametro2.SetFocus
		'        Exit Sub
		'      End If
		'      If txtParametro3 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 3", vbInformation, Me.Caption
		'        txtParametro3.SetFocus
		'        Exit Sub
		'      End If
		'    Case 4:
		'      If txtParametro1 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 1", vbInformation, Me.Caption
		'        txtParametro1.SetFocus
		'        Exit Sub
		'      End If
		'      If txtParametro2 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 2", vbInformation, Me.Caption
		'        txtParametro2.SetFocus
		'        Exit Sub
		'      End If
		'      If txtParametro3 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 3", vbInformation, Me.Caption
		'        txtParametro3.SetFocus
		'        Exit Sub
		'      End If
		'      If txtParametro4 = "" Then
		'        xMsgBox "Es necesario informar el par�metro 4", vbInformation, Me.Caption
		'        txtParametro4.SetFocus
		'        Exit Sub
		'      End If
		'  End Select
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadoresParametrizacionGrupos_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadoresParametrizacionGrupos_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmIndicadoresParametrizacionGrupos_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		If txtEmpresa.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtGruposIndicadores_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGruposIndicadores.GotFocus
		XGotFocus(Me, txtGruposIndicadores)
	End Sub
	
	Private Sub txtGruposIndicadores_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGruposIndicadores.DoubleClick
		ConsultaTaula(Me, txtGruposIndicadores)
	End Sub
	
	Private Sub txtGruposIndicadores_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGruposIndicadores.LostFocus
		If txtGruposIndicadores.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtGruposIndicadores)
	End Sub
	
	Private Sub txtGruposIndicadores_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtGruposIndicadores.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		If ABM = "AL" And txtGruposIndicadores.Text <> "" Then HabilitaParametres()
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtParametro1_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro1.GotFocus
		XGotFocus(Me, txtParametro1)
	End Sub
	
	Private Sub txtParametro1_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro1.LostFocus
		XLostFocus(Me, txtParametro1)
	End Sub
	
	Private Sub txtParametro1_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtParametro1.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtParametro2_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro2.GotFocus
		XGotFocus(Me, txtParametro2)
	End Sub
	
	Private Sub txtParametro2_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro2.LostFocus
		XLostFocus(Me, txtParametro2)
	End Sub
	
	Private Sub txtParametro2_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtParametro2.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtParametro3_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro3.GotFocus
		XGotFocus(Me, txtParametro3)
	End Sub
	
	Private Sub txtParametro3_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro3.LostFocus
		XLostFocus(Me, txtParametro3)
	End Sub
	
	Private Sub txtParametro3_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtParametro3.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtParametro4_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro4.GotFocus
		XGotFocus(Me, txtParametro4)
	End Sub
	
	Private Sub txtParametro4_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro4.LostFocus
		XLostFocus(Me, txtParametro4)
	End Sub
	
	Private Sub txtParametro4_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtParametro4.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
