Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParametrosDietas
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmParametrosDietas.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmParametrosDietas_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		XGotFocusForm(Me)
	End Sub
	
	Private Sub frmParametrosDietas_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub frmParametrosDietas_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmParametrosDietas_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParametrosDietas_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			SelTxT(Me)
			GoTo EventExitSub
		End If
		ABM = GetReg(Me)
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCuentaKilometraje_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaKilometraje.GotFocus
		XGotFocus(Me, txtCuentaKilometraje)
	End Sub
	
	Private Sub txtCuentaKilometraje_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaKilometraje.LostFocus
		XLostFocus(Me, txtCuentaKilometraje)
	End Sub
	
	Private Sub txtCuentaDietas_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaDietas.GotFocus
		XGotFocus(Me, txtCuentaDietas)
	End Sub
	
	Private Sub txtCuentaDietas_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaDietas.LostFocus
		XLostFocus(Me, txtCuentaDietas)
	End Sub
	
	Private Sub txtCuentaParkings_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaParkings.GotFocus
		XGotFocus(Me, txtCuentaParkings)
	End Sub
	
	Private Sub txtCuentaParkings_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaParkings.LostFocus
		XLostFocus(Me, txtCuentaParkings)
	End Sub
	
	Private Sub txtCuentaPeajes_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaPeajes.GotFocus
		XGotFocus(Me, txtCuentaPeajes)
	End Sub
	
	Private Sub txtCuentaPeajes_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaPeajes.LostFocus
		XLostFocus(Me, txtCuentaPeajes)
	End Sub
	
	Private Sub txtCuentaOtros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaOtros.GotFocus
		XGotFocus(Me, txtCuentaOtros)
	End Sub
	
	Private Sub txtCuentaOtros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaOtros.LostFocus
		XLostFocus(Me, txtCuentaOtros)
	End Sub
	
	Private Sub txtCuentaCaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaCaja.GotFocus
		XGotFocus(Me, txtCuentaCaja)
	End Sub
	
	Private Sub txtCuentaCaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCuentaCaja.LostFocus
		XLostFocus(Me, txtCuentaCaja)
	End Sub
	
	Private Sub txtCategoriaOperacio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCategoriaOperacio.GotFocus
		XGotFocus(Me, txtCategoriaOperacio)
	End Sub
	
	Private Sub txtCategoriaOperacio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCategoriaOperacio.LostFocus
		XLostFocus(Me, txtCategoriaOperacio)
	End Sub
	
	Private Sub txtHoteles_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoteles.GotFocus
		XGotFocus(Me, txtHoteles)
	End Sub
	
	Private Sub txtHoteles_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoteles.LostFocus
		XLostFocus(Me, txtHoteles)
	End Sub
	
	Private Sub txtImporteDietaCompleta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImporteDietaCompleta.GotFocus
		XGotFocus(Me, txtImporteDietaCompleta)
	End Sub
	
	Private Sub txtImporteDietaCompleta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImporteDietaCompleta.LostFocus
		XLostFocus(Me, txtImporteDietaCompleta)
	End Sub
	
	Private Sub txtImporteMediaDieta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImporteMediaDieta.GotFocus
		XGotFocus(Me, txtImporteMediaDieta)
	End Sub
	
	Private Sub txtImporteMediaDieta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImporteMediaDieta.LostFocus
		XLostFocus(Me, txtImporteMediaDieta)
	End Sub
	
	Private Sub txtMedios_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMedios.GotFocus
		XGotFocus(Me, txtMedios)
	End Sub
	
	Private Sub txtMedios_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMedios.LostFocus
		XLostFocus(Me, txtMedios)
	End Sub
	
	Private Sub txtPrecioKilometro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrecioKilometro.GotFocus
		XGotFocus(Me, txtPrecioKilometro)
	End Sub
	
	Private Sub txtPrecioKilometro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrecioKilometro.LostFocus
		XLostFocus(Me, txtPrecioKilometro)
	End Sub
	
	Private Sub txtSerieDocuments_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSerieDocuments.GotFocus
		XGotFocus(Me, txtSerieDocuments)
	End Sub
	
	Private Sub txtSerieDocuments_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSerieDocuments.LostFocus
		XLostFocus(Me, txtSerieDocuments)
	End Sub
	
	Private Sub txtDiarioContable_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiarioContable.GotFocus
		XGotFocus(Me, txtDiarioContable)
	End Sub
	
	Private Sub txtDiarioContable_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiarioContable.LostFocus
		XLostFocus(Me, txtDiarioContable)
	End Sub
	
	Private Sub txtFactorDietas_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDietas.GotFocus
		XGotFocus(Me, txtFactorDietas)
	End Sub
	
	Private Sub txtFactorDietas_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDietas.LostFocus
		XLostFocus(Me, txtFactorDietas)
	End Sub
	
	Private Sub txtFactorDietas_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFactorDietas.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFactorDietasIRPF_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDietasIRPF.GotFocus
		XGotFocus(Me, txtFactorDietasIRPF)
	End Sub
	
	Private Sub txtFactorDietasIRPF_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDietasIRPF.LostFocus
		XLostFocus(Me, txtFactorDietasIRPF)
	End Sub
	
	Private Sub txtFactorDietasIRPF_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFactorDietasIRPF.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFactorDesplazamientos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDesplazamientos.GotFocus
		XGotFocus(Me, txtFactorDesplazamientos)
	End Sub
	
	Private Sub txtFactorDesplazamientos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDesplazamientos.LostFocus
		XLostFocus(Me, txtFactorDesplazamientos)
	End Sub
	
	Private Sub txtFactorDesplazamientos_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFactorDesplazamientos.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFactorDesplazamientosI_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDesplazamientosI.GotFocus
		XGotFocus(Me, txtFactorDesplazamientosI)
	End Sub
	
	Private Sub txtFactorDesplazamientosI_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorDesplazamientosI.LostFocus
		XLostFocus(Me, txtFactorDesplazamientosI)
	End Sub
	
	Private Sub txtFactorDesplazamientosI_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFactorDesplazamientosI.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFactorPeajes_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorPeajes.GotFocus
		XGotFocus(Me, txtFactorPeajes)
	End Sub
	
	Private Sub txtFactorPeajes_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFactorPeajes.LostFocus
		XLostFocus(Me, txtFactorPeajes)
	End Sub
	
	Private Sub txtFactorPeajes_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFactorPeajes.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
End Class
