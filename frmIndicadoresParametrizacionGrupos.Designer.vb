<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadoresParametrizacionGrupos
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents cmdCopiaEmp As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents txtGruposIndicadores As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtParametro1 As System.Windows.Forms.TextBox
	Public WithEvents txtParametro2 As System.Windows.Forms.TextBox
	Public WithEvents txtParametro3 As System.Windows.Forms.TextBox
	Public WithEvents txtParametro4 As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents labelP1 As System.Windows.Forms.Label
	Public WithEvents labelP2 As System.Windows.Forms.Label
	Public WithEvents labelP3 As System.Windows.Forms.Label
	Public WithEvents labelP4 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadoresParametrizacionGrupos))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.cmdCopiaEmp = New AxXtremeSuiteControls.AxPushButton
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.txtGruposIndicadores = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtParametro1 = New System.Windows.Forms.TextBox
		Me.txtParametro2 = New System.Windows.Forms.TextBox
		Me.txtParametro3 = New System.Windows.Forms.TextBox
		Me.txtParametro4 = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.labelP1 = New System.Windows.Forms.Label
		Me.labelP2 = New System.Windows.Forms.Label
		Me.labelP3 = New System.Windows.Forms.Label
		Me.labelP4 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdCopiaEmp, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Parametrizaci�n grupos indicadores"
		Me.ClientSize = New System.Drawing.Size(580, 227)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORES GRUPOS PARAMETRI"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadoresParametrizacionGrupos"
		cmdCopiaEmp.OcxState = CType(resources.GetObject("cmdCopiaEmp.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCopiaEmp.Size = New System.Drawing.Size(23, 23)
		Me.cmdCopiaEmp.Location = New System.Drawing.Point(550, 12)
		Me.cmdCopiaEmp.TabIndex = 17
Me.cmdCopiaEmp.Tag = "Execute"
		Me.cmdCopiaEmp.Name = "cmdCopiaEmp"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(384, 19)
		Me.Text2.Location = New System.Drawing.Point(165, 14)
		Me.Text2.TabIndex = 15
		Me.Text2.Tag = "^1"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(42, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(120, 14)
		Me.txtEmpresa.Maxlength = 4
		Me.txtEmpresa.TabIndex = 1
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.txtGruposIndicadores.AutoSize = False
		Me.txtGruposIndicadores.Size = New System.Drawing.Size(31, 19)
		Me.txtGruposIndicadores.Location = New System.Drawing.Point(120, 37)
		Me.txtGruposIndicadores.Maxlength = 3
		Me.txtGruposIndicadores.TabIndex = 2
		Me.txtGruposIndicadores.Tag = "*2"
		Me.txtGruposIndicadores.AcceptsReturn = True
		Me.txtGruposIndicadores.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGruposIndicadores.BackColor = System.Drawing.SystemColors.Window
		Me.txtGruposIndicadores.CausesValidation = True
		Me.txtGruposIndicadores.Enabled = True
		Me.txtGruposIndicadores.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGruposIndicadores.HideSelection = True
		Me.txtGruposIndicadores.ReadOnly = False
		Me.txtGruposIndicadores.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGruposIndicadores.MultiLine = False
		Me.txtGruposIndicadores.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGruposIndicadores.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGruposIndicadores.TabStop = True
		Me.txtGruposIndicadores.Visible = True
		Me.txtGruposIndicadores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGruposIndicadores.Name = "txtGruposIndicadores"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(418, 19)
		Me.Text1.Location = New System.Drawing.Point(154, 37)
		Me.Text1.TabIndex = 7
		Me.Text1.Tag = "^2"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtParametro1.AutoSize = False
		Me.txtParametro1.Size = New System.Drawing.Size(369, 19)
		Me.txtParametro1.Location = New System.Drawing.Point(204, 73)
		Me.txtParametro1.Maxlength = 100
		Me.txtParametro1.TabIndex = 3
		Me.txtParametro1.Tag = "3"
		Me.txtParametro1.AcceptsReturn = True
		Me.txtParametro1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtParametro1.BackColor = System.Drawing.SystemColors.Window
		Me.txtParametro1.CausesValidation = True
		Me.txtParametro1.Enabled = True
		Me.txtParametro1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtParametro1.HideSelection = True
		Me.txtParametro1.ReadOnly = False
		Me.txtParametro1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtParametro1.MultiLine = False
		Me.txtParametro1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtParametro1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtParametro1.TabStop = True
		Me.txtParametro1.Visible = True
		Me.txtParametro1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtParametro1.Name = "txtParametro1"
		Me.txtParametro2.AutoSize = False
		Me.txtParametro2.Size = New System.Drawing.Size(369, 19)
		Me.txtParametro2.Location = New System.Drawing.Point(204, 97)
		Me.txtParametro2.Maxlength = 100
		Me.txtParametro2.TabIndex = 4
		Me.txtParametro2.Tag = "4"
		Me.txtParametro2.AcceptsReturn = True
		Me.txtParametro2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtParametro2.BackColor = System.Drawing.SystemColors.Window
		Me.txtParametro2.CausesValidation = True
		Me.txtParametro2.Enabled = True
		Me.txtParametro2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtParametro2.HideSelection = True
		Me.txtParametro2.ReadOnly = False
		Me.txtParametro2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtParametro2.MultiLine = False
		Me.txtParametro2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtParametro2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtParametro2.TabStop = True
		Me.txtParametro2.Visible = True
		Me.txtParametro2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtParametro2.Name = "txtParametro2"
		Me.txtParametro3.AutoSize = False
		Me.txtParametro3.Size = New System.Drawing.Size(369, 19)
		Me.txtParametro3.Location = New System.Drawing.Point(204, 121)
		Me.txtParametro3.Maxlength = 100
		Me.txtParametro3.TabIndex = 5
		Me.txtParametro3.Tag = "5"
		Me.txtParametro3.AcceptsReturn = True
		Me.txtParametro3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtParametro3.BackColor = System.Drawing.SystemColors.Window
		Me.txtParametro3.CausesValidation = True
		Me.txtParametro3.Enabled = True
		Me.txtParametro3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtParametro3.HideSelection = True
		Me.txtParametro3.ReadOnly = False
		Me.txtParametro3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtParametro3.MultiLine = False
		Me.txtParametro3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtParametro3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtParametro3.TabStop = True
		Me.txtParametro3.Visible = True
		Me.txtParametro3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtParametro3.Name = "txtParametro3"
		Me.txtParametro4.AutoSize = False
		Me.txtParametro4.Size = New System.Drawing.Size(369, 19)
		Me.txtParametro4.Location = New System.Drawing.Point(204, 145)
		Me.txtParametro4.Maxlength = 100
		Me.txtParametro4.TabIndex = 6
		Me.txtParametro4.Tag = "6"
		Me.txtParametro4.AcceptsReturn = True
		Me.txtParametro4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtParametro4.BackColor = System.Drawing.SystemColors.Window
		Me.txtParametro4.CausesValidation = True
		Me.txtParametro4.Enabled = True
		Me.txtParametro4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtParametro4.HideSelection = True
		Me.txtParametro4.ReadOnly = False
		Me.txtParametro4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtParametro4.MultiLine = False
		Me.txtParametro4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtParametro4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtParametro4.TabStop = True
		Me.txtParametro4.Visible = True
		Me.txtParametro4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtParametro4.Name = "txtParametro4"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(381, 193)
		Me.cmdAceptar.TabIndex = 12
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(471, 193)
		Me.cmdGuardar.TabIndex = 13
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "Empresa"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(6, 18)
		Me.Label1.TabIndex = 16
		Me.Label1.Tag = "1"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Grupos indicadores"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(6, 41)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "2"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.labelP1.Text = "Par�metro 1"
		Me.labelP1.Size = New System.Drawing.Size(199, 15)
		Me.labelP1.Location = New System.Drawing.Point(6, 77)
		Me.labelP1.TabIndex = 8
		Me.labelP1.Tag = "3"
		Me.labelP1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.labelP1.BackColor = System.Drawing.SystemColors.Control
		Me.labelP1.Enabled = True
		Me.labelP1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.labelP1.Cursor = System.Windows.Forms.Cursors.Default
		Me.labelP1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.labelP1.UseMnemonic = True
		Me.labelP1.Visible = True
		Me.labelP1.AutoSize = False
		Me.labelP1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.labelP1.Name = "labelP1"
		Me.labelP2.Text = "Par�metro 2"
		Me.labelP2.Size = New System.Drawing.Size(199, 15)
		Me.labelP2.Location = New System.Drawing.Point(6, 101)
		Me.labelP2.TabIndex = 9
		Me.labelP2.Tag = "4"
		Me.labelP2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.labelP2.BackColor = System.Drawing.SystemColors.Control
		Me.labelP2.Enabled = True
		Me.labelP2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.labelP2.Cursor = System.Windows.Forms.Cursors.Default
		Me.labelP2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.labelP2.UseMnemonic = True
		Me.labelP2.Visible = True
		Me.labelP2.AutoSize = False
		Me.labelP2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.labelP2.Name = "labelP2"
		Me.labelP3.Text = "Par�metro 3"
		Me.labelP3.Size = New System.Drawing.Size(199, 15)
		Me.labelP3.Location = New System.Drawing.Point(6, 125)
		Me.labelP3.TabIndex = 10
		Me.labelP3.Tag = "5"
		Me.labelP3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.labelP3.BackColor = System.Drawing.SystemColors.Control
		Me.labelP3.Enabled = True
		Me.labelP3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.labelP3.Cursor = System.Windows.Forms.Cursors.Default
		Me.labelP3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.labelP3.UseMnemonic = True
		Me.labelP3.Visible = True
		Me.labelP3.AutoSize = False
		Me.labelP3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.labelP3.Name = "labelP3"
		Me.labelP4.Text = "Par�metro 4"
		Me.labelP4.Size = New System.Drawing.Size(199, 15)
		Me.labelP4.Location = New System.Drawing.Point(6, 149)
		Me.labelP4.TabIndex = 11
		Me.labelP4.Tag = "6"
		Me.labelP4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.labelP4.BackColor = System.Drawing.SystemColors.Control
		Me.labelP4.Enabled = True
		Me.labelP4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.labelP4.Cursor = System.Windows.Forms.Cursors.Default
		Me.labelP4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.labelP4.UseMnemonic = True
		Me.labelP4.Visible = True
		Me.labelP4.AutoSize = False
		Me.labelP4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.labelP4.Name = "labelP4"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 192)
		Me.lblLock.TabIndex = 14
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 186
		Me.Line1.Y2 = 186
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 187
		Me.Line2.Y2 = 187
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdCopiaEmp, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(cmdCopiaEmp)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(txtGruposIndicadores)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtParametro1)
		Me.Controls.Add(txtParametro2)
		Me.Controls.Add(txtParametro3)
		Me.Controls.Add(txtParametro4)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(labelP1)
		Me.Controls.Add(labelP2)
		Me.Controls.Add(labelP3)
		Me.Controls.Add(labelP4)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
