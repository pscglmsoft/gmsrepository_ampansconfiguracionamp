Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmUsuarisPerColaboradors
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub chkIncidencies_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIncidencies.GotFocus
		XGotFocus(Me, chkIncidencies)
	End Sub
	
	Private Sub chkIncidencies_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIncidencies.LostFocus
		XLostFocus(Me, chkIncidencies)
	End Sub
	
	Private Sub chkPermBloqFact_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermBloqFact.GotFocus
		XGotFocus(Me, chkPermBloqFact)
	End Sub
	
	Private Sub chkPermBloqFact_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermBloqFact.LostFocus
		XLostFocus(Me, chkPermBloqFact)
	End Sub
	
	Private Sub chkPermisModCCCC_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermisModCCCC.GotFocus
		XGotFocus(Me, chkPermisModCCCC)
	End Sub
	
	Private Sub chkPermisModCCCC_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermisModCCCC.LostFocus
		XLostFocus(Me, chkPermisModCCCC)
	End Sub
	
	Private Sub chkTancarFeines_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTancarFeines.GotFocus
		XGotFocus(Me, chkTancarFeines)
	End Sub
	
	Private Sub chkTancarFeines_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTancarFeines.LostFocus
		XLostFocus(Me, chkTancarFeines)
	End Sub
	
	Private Sub chkTancarTasques_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTancarTasques.GotFocus
		XGotFocus(Me, chkTancarTasques)
	End Sub
	
	Private Sub chkTancarTasques_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTancarTasques.LostFocus
		XLostFocus(Me, chkTancarTasques)
	End Sub
	
	Private Sub frmUsuarisPerColaboradors_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub frmUsuarisPerColaboradors_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		XGotFocusForm(Me)
	End Sub
	
	Private Sub frmUsuarisPerColaboradors_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuarisPerColaboradors_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodigoUsu_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoUsu.GotFocus
		XGotFocus(Me, txtCodigoUsu)
	End Sub
	
	Private Sub txtCodigoUsu_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoUsu.DoubleClick
		ConsultaTaula(Me, txtCodigoUsu)
	End Sub
	
	Private Sub txtCodigoUsu_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoUsu.LostFocus
		XLostFocus(Me, txtCodigoUsu)
		ABM = GetReg(Me)
	End Sub
	
	Private Sub txtCodigoUsu_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigoUsu.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCodigoCol_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoCol.GotFocus
		XGotFocus(Me, txtCodigoCol)
	End Sub
	
	Private Sub txtCodigoCol_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoCol.DoubleClick
		ConsultaTaula(Me, txtCodigoCol)
	End Sub
	
	Private Sub txtCodigoCol_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoCol.LostFocus
		XLostFocus(Me, txtCodigoCol)
	End Sub
	
	Private Sub txtCodigoCol_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigoCol.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub chkAdmin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAdmin.GotFocus
		XGotFocus(Me, chkAdmin)
	End Sub
	
	Private Sub chkAdmin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAdmin.LostFocus
		XLostFocus(Me, chkAdmin)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		XLostFocus(Me, txtEmpresa)
		ABM = GetReg(Me)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtMail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMail.GotFocus
		XGotFocus(Me, txtMail)
	End Sub
	
	Private Sub txtMail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMail.LostFocus
		XLostFocus(Me, txtMail)
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
End Class
