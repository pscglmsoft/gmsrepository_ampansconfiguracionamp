Option Strict Off
Option Explicit On
Friend Class frmAvisoCorreos
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic FlagExtern As Boolean
	''OKPublic RegistreExtern As String
	Public Empresa As String
	Public XModul As String
	Public cMail As String
	
	
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmbCodigoMail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCodigoMail.GotFocus
		XGotFocus(Me, cmbCodigoMail)
	End Sub
	
	Private Sub cmbCodigoMail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCodigoMail.LostFocus
		If cmbCodigoMail.Columns(1).Value <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, cmbCodigoMail)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmAvisoCorreos.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmAvisoCorreos_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		'CarregaUsuaris
		'CarregaGrups
		'CarregaCentres
		Application.DoEvents()
		MontaArbreUs()
		MontaArbreGrups()
		MontaArbreCentres()
	End Sub
	
	Private Sub frmAvisoCorreos_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		Empresa = ""
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		GravaTree()
		GravaLlistaDist()
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Me.Unload()
		'Inici
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		GravaTree()
		GravaLlistaDist()
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmAvisoCorreos_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmbModulo.Enabled = False
		cmbCodigoMail.Enabled = False
		DisplayDescripcio(Me, txtEmpresa)
		txtEmpresa.Enabled = False
		
		'    MontaArbreUs
		'    MontaArbreGrups
		'    MontaArbreCentres
	End Sub
	
	Private Sub frmAvisoCorreos_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmAvisoCorreos_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub grdListaD_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdListaD.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdListaD", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Marcar todos",  ,  , XPIcon("Plus"),  ,  ,  ,  , "M")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Desmarcar todos",  ,  , XPIcon("Minus"),  ,  ,  ,  , "D")
			End With
		End With
		XpExecutaMenu(Me, "grdListaD", eventArgs.X, eventArgs.Y)
		
	End Sub
	
	
	
	Private Sub TabCorreus_Selected(sender As Object, ByVal eventArgs As TabControlEventArgs) Handles TabCorreus.Selected
		
		Select Case eventArgs.TabPageIndex
			Case 1
				'FGCreaWorkGrid grdUsuarios, "USUARIS"
				'FGCreaWorkGrid grdGrupos, "GRUPS"
				'FGCreaWorkGrid grdCentros, "CENTRES"
				
				'GravaLlistaDist
				If GravaRegistre(Me) = False Then
					Exit Sub
				End If
				GravaTree()
				CarregaLlistaDist()
				
		End Select
	End Sub
	
	Private Sub TvCentros_AfterCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.TreeViewEventArgs) Handles TvCentros.AfterCheck
		Dim Node As System.Windows.Forms.TreeNode = eventArgs.Node
		MarcaNode(Node)
	End Sub
	
	Private Sub TvGrupos_AfterCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.TreeViewEventArgs) Handles TvGrupos.AfterCheck
		Dim Node As System.Windows.Forms.TreeNode = eventArgs.Node
		MarcaNode(Node)
	End Sub
	
	Private Sub tvUsuaris_AfterCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.TreeViewEventArgs) Handles tvUsuaris.AfterCheck
		Dim Node As System.Windows.Forms.TreeNode = eventArgs.Node
		MarcaNode(Node)
	End Sub
	
	Private Sub txtCentroEmision_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroEmision.DoubleClick
		ConsultaTaula(Me, txtCentroEmision)
	End Sub
	
	Private Sub txtCentroEmision_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroEmision.GotFocus
		XGotFocus(Me, txtCentroEmision)
	End Sub
	
	Private Sub txtCentroEmision_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroEmision.LostFocus
		XLostFocus(Me, txtCentroEmision)
	End Sub
	
	Private Sub txtCentroEmision_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentroEmision.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		If txtEmpresa.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbModulo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbModulo.GotFocus
		XGotFocus(Me, cmbModulo)
	End Sub
	
	Private Sub cmbModulo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbModulo.LostFocus
		If cmbModulo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, cmbModulo)
	End Sub
	
	
	
	Private Sub chkMarcado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMarcado.GotFocus
		XGotFocus(Me, chkMarcado)
	End Sub
	
	Private Sub chkMarcado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMarcado.LostFocus
		XLostFocus(Me, chkMarcado)
	End Sub
	
	'Private Sub CarregaUsuaris()
	'    CarregaFGrid grdUsuarios, "GridUsuaris^ENT.G.AUX0001", XModul & S & cMail & S & Empresa
	'End Sub
	
	'Private Sub CarregaGrups()
	'    CarregaFGrid grdGrupos, "GridGrups^ENT.G.AUX0001", XModul & S & cMail & S & Empresa
	'End Sub
	
	'Private Sub CarregaCentres()
	'    If Empresa = "" Then Exit Sub
	'    CarregaFGrid grdCentros, "GridCentres^ENT.G.AUX0001", XModul & S & cMail & S & Empresa
	'End Sub
	
	Private Sub CarregaLlistaDist()
		CarregaFGrid(grdListaD, "GridLlistaDist^ENT.G.AUX0001", XModul & S & cMail & S & Empresa)
	End Sub
	
	Private Sub GravaLlistaDist()
		'CarregaLlistaDist
		FGCreaWorkGrid(grdListaD, "LLISTAEX")
		CacheNetejaParametres()
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto cmbModulo.Columns().Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MCache.P1 = cmbModulo.Columns(1).Value
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto cmbCodigoMail.Columns().Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MCache.P2 = cmbCodigoMail.Columns(1).Value
		MCache.P3 = txtEmpresa.Text
		CacheXecute("D GravaLlistaDist^ENT.G.AUX0001")
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim a As String
		Dim I As Short
		Select Case NomMenu
			Case "grdListaD"
				Select Case KeyMenu
					Case "M"
						FGCheckGrid(grdListaD, True, 7)
					Case "D"
						FGCheckGrid(grdListaD, False, 7)
				End Select
				
		End Select
	End Sub
	Private Sub MontaArbreUs()
		CarregaTreeView(tvUsuaris, "CTreeUsuaris^ENT.G.AUX0001", XModul & S & cMail & S & Empresa, True)
	End Sub
	
	Private Sub MontaArbreGrups()
		CarregaTreeView(TvGrupos, "CTreeGrups^ENT.G.AUX0001", XModul & S & cMail & S & Empresa, True)
	End Sub
	
	Private Sub MontaArbreCentres()
		CarregaTreeView(TvCentros, "CTreeCentres^ENT.G.AUX0001", XModul & S & cMail & S & Empresa, True)
	End Sub
	
	Private Sub GravaTree()
		Dim DadesUsu As String
		Dim DadesGrups As String
		Dim DadesCen As String
		
		DadesUsu = GetCheckedNodes(tvUsuaris, 2)
		DadesGrups = GetCheckedNodes(TvGrupos, 2)
		DadesCen = GetCheckedNodes(TvCentros, 2)
		
		CacheNetejaParametres()
		MCache.P1 = XModul
		MCache.P2 = cMail
		MCache.P3 = Empresa
		MCache.P4 = DadesUsu
		MCache.P5 = DadesGrups
		MCache.P6 = DadesCen
		CacheXecute("D GravaTree^ENT.G.AUX0001")
	End Sub
	
Overrides 	Sub DeleteReg()
		DelReg(Me)
		Me.Unload()
	End Sub
End Class
