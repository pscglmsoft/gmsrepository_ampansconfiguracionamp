Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParametrosSocios
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	'UPGRADE_WARNING: Form evento frmParametrosSocios.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmParametrosSocios_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		XGotFocusForm(Me)
	End Sub
	
	Private Sub frmParametrosSocios_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		ResetForm(Me)
	End Sub
	
	Private Sub frmParametrosSocios_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	Private Sub frmParametrosSocios_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParametrosSocios_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cmbPerfilContable_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPerfilContable.GotFocus
		XGotFocus(Me, cmbPerfilContable)
	End Sub
	
	Private Sub cmbPerfilContable_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPerfilContable.LostFocus
		XLostFocus(Me, cmbPerfilContable)
	End Sub
	
	Private Sub txtClaseIVA_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClaseIVA.DoubleClick
		ConsultaTaula(Me, txtClaseIVA)
	End Sub
	
	Private Sub txtClaseIVA_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClaseIVA.GotFocus
		XGotFocus(Me, txtClaseIVA)
	End Sub
	
	Private Sub txtClaseIVA_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClaseIVA.LostFocus
		XLostFocus(Me, txtClaseIVA)
	End Sub
	
	Private Sub txtClaseIVA_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtClaseIVA.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtClaseIVA)
		If Cancel Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtSerie_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSerie.GotFocus
		XGotFocus(Me, txtSerie)
	End Sub
	
	Private Sub txtSerie_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSerie.LostFocus
		XLostFocus(Me, txtSerie)
	End Sub
	
	Private Sub txtTipoIVA_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoIVA.DoubleClick
		ConsultaTaula(Me, txtTipoIVA)
	End Sub
	
	Private Sub txtTipoIVA_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoIVA.GotFocus
		XGotFocus(Me, txtTipoIVA)
	End Sub
	
	Private Sub txtTipoIVA_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoIVA.LostFocus
		XLostFocus(Me, txtTipoIVA)
	End Sub
	
	Private Sub txtTipoIVA_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoIVA.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTipoIVA)
		If Cancel Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
End Class
