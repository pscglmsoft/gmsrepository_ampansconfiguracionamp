<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParametrosSocios
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtTipoIVA As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtSerie As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtClaseIVA As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbPerfilContable As DataControl.GmsCombo
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParametrosSocios))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtTipoIVA = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtSerie = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtClaseIVA = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.cmbPerfilContable = New DataControl.GmsCombo
		Me.lbl11 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()

		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Par�metros socios"
		Me.ClientSize = New System.Drawing.Size(453, 205)
		Me.Location = New System.Drawing.Point(502, 319)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-SOCIOS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParametrosSocios"
		Me.txtTipoIVA.AutoSize = False
		Me.txtTipoIVA.Size = New System.Drawing.Size(52, 19)
		Me.txtTipoIVA.Location = New System.Drawing.Point(110, 66)
		Me.txtTipoIVA.Maxlength = 5
		Me.txtTipoIVA.TabIndex = 2
		Me.txtTipoIVA.Tag = "3"
		Me.txtTipoIVA.AcceptsReturn = True
		Me.txtTipoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoIVA.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoIVA.CausesValidation = True
		Me.txtTipoIVA.Enabled = True
		Me.txtTipoIVA.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoIVA.HideSelection = True
		Me.txtTipoIVA.ReadOnly = False
		Me.txtTipoIVA.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoIVA.MultiLine = False
		Me.txtTipoIVA.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoIVA.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoIVA.TabStop = True
		Me.txtTipoIVA.Visible = True
		Me.txtTipoIVA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoIVA.Name = "txtTipoIVA"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(274, 19)
		Me.Text2.Location = New System.Drawing.Point(165, 66)
		Me.Text2.TabIndex = 13
		Me.Text2.Tag = "^3"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtSerie.AutoSize = False
		Me.txtSerie.Size = New System.Drawing.Size(52, 19)
		Me.txtSerie.Location = New System.Drawing.Point(110, 90)
		Me.txtSerie.Maxlength = 5
		Me.txtSerie.TabIndex = 3
		Me.txtSerie.Tag = "4"
		Me.txtSerie.AcceptsReturn = True
		Me.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSerie.BackColor = System.Drawing.SystemColors.Window
		Me.txtSerie.CausesValidation = True
		Me.txtSerie.Enabled = True
		Me.txtSerie.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSerie.HideSelection = True
		Me.txtSerie.ReadOnly = False
		Me.txtSerie.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSerie.MultiLine = False
		Me.txtSerie.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSerie.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSerie.TabStop = True
		Me.txtSerie.Visible = True
		Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSerie.Name = "txtSerie"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(274, 19)
		Me.Text3.Location = New System.Drawing.Point(165, 42)
		Me.Text3.TabIndex = 10
		Me.Text3.Tag = "^2"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtClaseIVA.AutoSize = False
		Me.txtClaseIVA.Size = New System.Drawing.Size(52, 19)
		Me.txtClaseIVA.Location = New System.Drawing.Point(110, 42)
		Me.txtClaseIVA.Maxlength = 5
		Me.txtClaseIVA.TabIndex = 1
		Me.txtClaseIVA.Tag = "2"
		Me.txtClaseIVA.AcceptsReturn = True
		Me.txtClaseIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtClaseIVA.BackColor = System.Drawing.SystemColors.Window
		Me.txtClaseIVA.CausesValidation = True
		Me.txtClaseIVA.Enabled = True
		Me.txtClaseIVA.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClaseIVA.HideSelection = True
		Me.txtClaseIVA.ReadOnly = False
		Me.txtClaseIVA.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClaseIVA.MultiLine = False
		Me.txtClaseIVA.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClaseIVA.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtClaseIVA.TabStop = True
		Me.txtClaseIVA.Visible = True
		Me.txtClaseIVA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtClaseIVA.Name = "txtClaseIVA"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(52, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(110, 19)
		Me.txtEmpresa.Maxlength = 5
		Me.txtEmpresa.TabIndex = 0
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(274, 19)
		Me.Text1.Location = New System.Drawing.Point(165, 19)
		Me.Text1.TabIndex = 8
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(266, 168)
		Me.cmdAceptar.TabIndex = 5
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(356, 168)
		Me.cmdGuardar.TabIndex = 6
		Me.cmdGuardar.Name = "cmdGuardar"

		Me.cmbPerfilContable.Size = New System.Drawing.Size(145, 19)
		Me.cmbPerfilContable.Location = New System.Drawing.Point(110, 114)
		Me.cmbPerfilContable.TabIndex = 4
Me.cmbPerfilContable.Tag = "5"
		Me.cmbPerfilContable.Name = "cmbPerfilContable"
		Me.lbl11.Text = "Perfil contable"
		Me.lbl11.Size = New System.Drawing.Size(97, 15)
		Me.lbl11.Location = New System.Drawing.Point(10, 118)
		Me.lbl11.TabIndex = 15
		Me.lbl11.Tag = "5"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.Label3.Text = "Tipo IVA"
		Me.Label3.Size = New System.Drawing.Size(97, 15)
		Me.Label3.Location = New System.Drawing.Point(10, 68)
		Me.Label3.TabIndex = 14
		Me.Label3.Tag = "3"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Serie"
		Me.Label2.Size = New System.Drawing.Size(97, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 92)
		Me.Label2.TabIndex = 12
		Me.Label2.Tag = "4"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Clase IVA ventas"
		Me.Label1.Size = New System.Drawing.Size(97, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 44)
		Me.Label1.TabIndex = 11
		Me.Label1.Tag = "2"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 6
		Me.Line2.X2 = 65
		Me.Line2.Y1 = 157
		Me.Line2.Y2 = 157
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 6
		Me.Line1.X2 = 65
		Me.Line1.Y1 = 156
		Me.Line1.Y2 = 156
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(97, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 21)
		Me.lbl1.TabIndex = 7
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 164)
		Me.lblLock.TabIndex = 9
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"

		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtTipoIVA)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtSerie)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtClaseIVA)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(cmbPerfilContable)
		Me.Controls.Add(lbl11)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lblLock)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
