<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParametrosAplicacion
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents chkPermitirNIFRepetidos As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents chkVerificarNifs As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents txtIVAServeis As System.Windows.Forms.TextBox
	Public WithEvents txtCodigoPais As System.Windows.Forms.TextBox
	Public WithEvents txtRetencion As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtConstant1 As System.Windows.Forms.TextBox
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParametrosAplicacion))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.chkPermitirNIFRepetidos = New AxXtremeSuiteControls.AxCheckBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.chkVerificarNifs = New AxXtremeSuiteControls.AxCheckBox
		Me.txtIVAServeis = New System.Windows.Forms.TextBox
		Me.txtCodigoPais = New System.Windows.Forms.TextBox
		Me.txtRetencion = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.txtConstant1 = New System.Windows.Forms.TextBox
		Me.lbl2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lbl1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkPermitirNIFRepetidos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkVerificarNifs, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Par�metros aplicaciones"
		Me.ClientSize = New System.Drawing.Size(383, 210)
		Me.Location = New System.Drawing.Point(487, 309)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-PARAMETRES APL.ENT"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParametrosAplicacion"
		chkPermitirNIFRepetidos.OcxState = CType(resources.GetObject("chkPermitirNIFRepetidos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPermitirNIFRepetidos.Size = New System.Drawing.Size(167, 17)
		Me.chkPermitirNIFRepetidos.Location = New System.Drawing.Point(14, 112)
		Me.chkPermitirNIFRepetidos.TabIndex = 6
Me.chkPermitirNIFRepetidos.Tag = "8"
		Me.chkPermitirNIFRepetidos.Name = "chkPermitirNIFRepetidos"
		Me.Text1.AutoSize = False
		Me.Text1.Size = New System.Drawing.Size(147, 19)
		Me.Text1.Location = New System.Drawing.Point(222, 13)
		Me.Text1.TabIndex = 11
		Me.Text1.Tag = "^2"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.BackColor = System.Drawing.SystemColors.Window
		Me.Text1.CausesValidation = True
		Me.Text1.Enabled = True
		Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		chkVerificarNifs.OcxState = CType(resources.GetObject("chkVerificarNifs.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkVerificarNifs.Size = New System.Drawing.Size(167, 17)
		Me.chkVerificarNifs.Location = New System.Drawing.Point(14, 88)
		Me.chkVerificarNifs.TabIndex = 5
Me.chkVerificarNifs.Tag = "7"
		Me.chkVerificarNifs.Name = "chkVerificarNifs"
		Me.txtIVAServeis.AutoSize = False
		Me.txtIVAServeis.Size = New System.Drawing.Size(51, 19)
		Me.txtIVAServeis.Location = New System.Drawing.Point(168, 13)
		Me.txtIVAServeis.Maxlength = 12
		Me.txtIVAServeis.TabIndex = 2
		Me.txtIVAServeis.Tag = "2"
		Me.txtIVAServeis.AcceptsReturn = True
		Me.txtIVAServeis.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtIVAServeis.BackColor = System.Drawing.SystemColors.Window
		Me.txtIVAServeis.CausesValidation = True
		Me.txtIVAServeis.Enabled = True
		Me.txtIVAServeis.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtIVAServeis.HideSelection = True
		Me.txtIVAServeis.ReadOnly = False
		Me.txtIVAServeis.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtIVAServeis.MultiLine = False
		Me.txtIVAServeis.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtIVAServeis.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtIVAServeis.TabStop = True
		Me.txtIVAServeis.Visible = True
		Me.txtIVAServeis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtIVAServeis.Name = "txtIVAServeis"
		Me.txtCodigoPais.AutoSize = False
		Me.txtCodigoPais.Size = New System.Drawing.Size(51, 19)
		Me.txtCodigoPais.Location = New System.Drawing.Point(168, 61)
		Me.txtCodigoPais.Maxlength = 1
		Me.txtCodigoPais.TabIndex = 4
		Me.txtCodigoPais.Tag = "4"
		Me.txtCodigoPais.AcceptsReturn = True
		Me.txtCodigoPais.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoPais.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoPais.CausesValidation = True
		Me.txtCodigoPais.Enabled = True
		Me.txtCodigoPais.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoPais.HideSelection = True
		Me.txtCodigoPais.ReadOnly = False
		Me.txtCodigoPais.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoPais.MultiLine = False
		Me.txtCodigoPais.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoPais.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoPais.TabStop = True
		Me.txtCodigoPais.Visible = True
		Me.txtCodigoPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoPais.Name = "txtCodigoPais"
		Me.txtRetencion.AutoSize = False
		Me.txtRetencion.Size = New System.Drawing.Size(51, 19)
		Me.txtRetencion.Location = New System.Drawing.Point(168, 37)
		Me.txtRetencion.Maxlength = 50
		Me.txtRetencion.TabIndex = 3
		Me.txtRetencion.Tag = "3"
		Me.txtRetencion.AcceptsReturn = True
		Me.txtRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtRetencion.BackColor = System.Drawing.SystemColors.Window
		Me.txtRetencion.CausesValidation = True
		Me.txtRetencion.Enabled = True
		Me.txtRetencion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtRetencion.HideSelection = True
		Me.txtRetencion.ReadOnly = False
		Me.txtRetencion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtRetencion.MultiLine = False
		Me.txtRetencion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtRetencion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtRetencion.TabStop = True
		Me.txtRetencion.Visible = True
		Me.txtRetencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtRetencion.Name = "txtRetencion"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(196, 176)
		Me.cmdAceptar.TabIndex = 12
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(286, 176)
		Me.cmdGuardar.TabIndex = 13
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.txtConstant1.AutoSize = False
		Me.txtConstant1.Size = New System.Drawing.Size(18, 19)
		Me.txtConstant1.Location = New System.Drawing.Point(168, 13)
		Me.txtConstant1.Maxlength = 1
		Me.txtConstant1.TabIndex = 1
		Me.txtConstant1.Tag = "*1"
		Me.txtConstant1.AcceptsReturn = True
		Me.txtConstant1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtConstant1.BackColor = System.Drawing.SystemColors.Window
		Me.txtConstant1.CausesValidation = True
		Me.txtConstant1.Enabled = True
		Me.txtConstant1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtConstant1.HideSelection = True
		Me.txtConstant1.ReadOnly = False
		Me.txtConstant1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtConstant1.MultiLine = False
		Me.txtConstant1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtConstant1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtConstant1.TabStop = True
		Me.txtConstant1.Visible = True
		Me.txtConstant1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtConstant1.Name = "txtConstant1"
		Me.lbl2.Text = "% IVA de servicios"
		Me.lbl2.Size = New System.Drawing.Size(153, 15)
		Me.lbl2.Location = New System.Drawing.Point(14, 17)
		Me.lbl2.TabIndex = 7
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.Label1.Text = "Codigo pais Espa�a"
		Me.Label1.Size = New System.Drawing.Size(153, 15)
		Me.Label1.Location = New System.Drawing.Point(14, 64)
		Me.Label1.TabIndex = 10
		Me.Label1.Tag = "4"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl3.Text = "%Retenci�n I.R.P.F."
		Me.lbl3.Size = New System.Drawing.Size(153, 15)
		Me.lbl3.Location = New System.Drawing.Point(14, 41)
		Me.lbl3.TabIndex = 8
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lblLock.Text = "Registro bloqueado por otra sesion de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(10, 176)
		Me.lblLock.TabIndex = 9
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 10
		Me.Line1.X2 = 69
		Me.Line1.Y1 = 168
		Me.Line1.Y2 = 168
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 10
		Me.Line2.X2 = 69
		Me.Line2.Y1 = 169
		Me.Line2.Y2 = 169
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		Me.lbl1.Text = "Tipo par�metro"
		Me.lbl1.Size = New System.Drawing.Size(153, 15)
		Me.lbl1.Location = New System.Drawing.Point(14, 16)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkVerificarNifs, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPermitirNIFRepetidos, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(chkPermitirNIFRepetidos)
		Me.Controls.Add(Text1)
		Me.Controls.Add(chkVerificarNifs)
		Me.Controls.Add(txtIVAServeis)
		Me.Controls.Add(txtCodigoPais)
		Me.Controls.Add(txtRetencion)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(txtConstant1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
