<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParametresUsuaris
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtPathCursos As System.Windows.Forms.TextBox
	Public WithEvents txtPathFicheros As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Command1 As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdCursos As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParametresUsuaris))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtPathCursos = New System.Windows.Forms.TextBox
		Me.txtPathFicheros = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Command1 = New AxXtremeSuiteControls.AxPushButton
		Me.cmdCursos = New AxXtremeSuiteControls.AxPushButton
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lbl1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Command1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCursos, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Parametres usuaris"
		Me.ClientSize = New System.Drawing.Size(562, 130)
		Me.Location = New System.Drawing.Point(412, 380)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "IM-PARAMETRES USUARIS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParametresUsuaris"
		Me.txtPathCursos.AutoSize = False
		Me.txtPathCursos.Size = New System.Drawing.Size(329, 19)
		Me.txtPathCursos.Location = New System.Drawing.Point(192, 35)
		Me.txtPathCursos.Maxlength = 50
		Me.txtPathCursos.TabIndex = 5
		Me.txtPathCursos.Tag = "3"
		Me.txtPathCursos.AcceptsReturn = True
		Me.txtPathCursos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathCursos.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathCursos.CausesValidation = True
		Me.txtPathCursos.Enabled = True
		Me.txtPathCursos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathCursos.HideSelection = True
		Me.txtPathCursos.ReadOnly = False
		Me.txtPathCursos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathCursos.MultiLine = False
		Me.txtPathCursos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathCursos.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathCursos.TabStop = True
		Me.txtPathCursos.Visible = True
		Me.txtPathCursos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathCursos.Name = "txtPathCursos"
		Me.txtPathFicheros.AutoSize = False
		Me.txtPathFicheros.Size = New System.Drawing.Size(329, 19)
		Me.txtPathFicheros.Location = New System.Drawing.Point(192, 9)
		Me.txtPathFicheros.Maxlength = 50
		Me.txtPathFicheros.TabIndex = 3
		Me.txtPathFicheros.Tag = "2"
		Me.txtPathFicheros.AcceptsReturn = True
		Me.txtPathFicheros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathFicheros.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathFicheros.CausesValidation = True
		Me.txtPathFicheros.Enabled = True
		Me.txtPathFicheros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathFicheros.HideSelection = True
		Me.txtPathFicheros.ReadOnly = False
		Me.txtPathFicheros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathFicheros.MultiLine = False
		Me.txtPathFicheros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathFicheros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathFicheros.TabStop = True
		Me.txtPathFicheros.Visible = True
		Me.txtPathFicheros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathFicheros.Name = "txtPathFicheros"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(350, 96)
		Me.cmdAceptar.TabIndex = 7
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(440, 96)
		Me.cmdGuardar.TabIndex = 8
		Me.cmdGuardar.Name = "cmdGuardar"
		Command1.OcxState = CType(resources.GetObject("Command1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.Command1.Size = New System.Drawing.Size(27, 27)
		Me.Command1.Location = New System.Drawing.Point(524, 6)
		Me.Command1.TabIndex = 4
Me.Command1.Tag = "FolderSearch"
		Me.Command1.Name = "Command1"
		cmdCursos.OcxState = CType(resources.GetObject("cmdCursos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCursos.Size = New System.Drawing.Size(27, 27)
		Me.cmdCursos.Location = New System.Drawing.Point(524, 32)
		Me.cmdCursos.TabIndex = 6
Me.cmdCursos.Tag = "FolderSearch"
		Me.cmdCursos.Name = "cmdCursos"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(24, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(192, 9)
		Me.txtEmpresa.Maxlength = 5
		Me.txtEmpresa.TabIndex = 1
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.Text = "2"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Label1.Text = "Path documentaci�n"
		Me.Label1.Size = New System.Drawing.Size(177, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 39)
		Me.Label1.TabIndex = 10
		Me.Label1.Tag = "3"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl2.Text = "Path ficheros fotografias usuaris"
		Me.lbl2.Size = New System.Drawing.Size(177, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 13)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 96)
		Me.lblLock.TabIndex = 9
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 90
		Me.Line1.Y2 = 90
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 91
		Me.Line2.Y2 = 91
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		Me.lbl1.Text = "Fixa"
		Me.lbl1.Size = New System.Drawing.Size(25, 15)
		Me.lbl1.Location = New System.Drawing.Point(162, 13)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		CType(Me.cmdCursos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Command1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtPathCursos)
		Me.Controls.Add(txtPathFicheros)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Command1)
		Me.Controls.Add(cmdCursos)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
