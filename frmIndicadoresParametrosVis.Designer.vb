<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadoresParametrosVis
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtObservacionesParametro As System.Windows.Forms.TextBox
	Public WithEvents chkRequiereRecalculo As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents lblRecalcul As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents GroupBox1 As DataControl.GmsGroupBox
	Public WithEvents txtGrupoIndicadores As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtIndicador As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtParametro As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadoresParametrosVis))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.GroupBox1 = New DataControl.GmsGroupBox
		Me.txtObservacionesParametro = New System.Windows.Forms.TextBox
		Me.chkRequiereRecalculo = New AxXtremeSuiteControls.AxCheckBox
		Me.lblRecalcul = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.txtGrupoIndicadores = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtIndicador = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtParametro = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkRequiereRecalculo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Par�metros indicadores visualizaci�n"
		Me.ClientSize = New System.Drawing.Size(565, 295)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORES PARAMETROS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadoresParametrosVis"

		Me.GroupBox1.Size = New System.Drawing.Size(551, 101)
		Me.GroupBox1.Location = New System.Drawing.Point(8, 58)
Me.GroupBox1.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameNone
Me.GroupBox1.BackColor = System.Drawing.ColorTranslator.FromOle(64)
		Me.GroupBox1.TabIndex = 11
		Me.GroupBox1.Name = "GroupBox1"
		Me.txtObservacionesParametro.AutoSize = False
		Me.txtObservacionesParametro.Size = New System.Drawing.Size(431, 75)
		Me.txtObservacionesParametro.Location = New System.Drawing.Point(116, 2)
		Me.txtObservacionesParametro.Maxlength = 250
		Me.txtObservacionesParametro.MultiLine = True
		Me.txtObservacionesParametro.TabIndex = 12
		Me.txtObservacionesParametro.Tag = "4"
		Me.txtObservacionesParametro.AcceptsReturn = True
		Me.txtObservacionesParametro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesParametro.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesParametro.CausesValidation = True
		Me.txtObservacionesParametro.Enabled = True
		Me.txtObservacionesParametro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesParametro.HideSelection = True
		Me.txtObservacionesParametro.ReadOnly = False
		Me.txtObservacionesParametro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesParametro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesParametro.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesParametro.TabStop = True
		Me.txtObservacionesParametro.Visible = True
		Me.txtObservacionesParametro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesParametro.Name = "txtObservacionesParametro"
		chkRequiereRecalculo.OcxState = CType(resources.GetObject("chkRequiereRecalculo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkRequiereRecalculo.Size = New System.Drawing.Size(181, 23)
		Me.chkRequiereRecalculo.Location = New System.Drawing.Point(6, 76)
		Me.chkRequiereRecalculo.TabIndex = 15
Me.chkRequiereRecalculo.Tag = "5"
		Me.chkRequiereRecalculo.Name = "chkRequiereRecalculo"
		Me.lblRecalcul.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.lblRecalcul.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblRecalcul.Size = New System.Drawing.Size(541, 17)
		Me.lblRecalcul.Location = New System.Drawing.Point(6, 80)
		Me.lblRecalcul.TabIndex = 14
		Me.lblRecalcul.BackColor = System.Drawing.SystemColors.Control
		Me.lblRecalcul.Enabled = True
		Me.lblRecalcul.Cursor = System.Windows.Forms.Cursors.Default
Me.lblRecalcul.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblRecalcul.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblRecalcul.UseMnemonic = True
		Me.lblRecalcul.Visible = True
		Me.lblRecalcul.AutoSize = False
		Me.lblRecalcul.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblRecalcul.Name = "lblRecalcul"
		Me.lbl5.Text = "Observaciones par�metro"
		Me.lbl5.Size = New System.Drawing.Size(113, 15)
		Me.lbl5.Location = New System.Drawing.Point(2, 4)
		Me.lbl5.TabIndex = 13
		Me.lbl5.Tag = "4"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.txtGrupoIndicadores.AutoSize = False
		Me.txtGrupoIndicadores.Size = New System.Drawing.Size(31, 19)
		Me.txtGrupoIndicadores.Location = New System.Drawing.Point(124, 11)
		Me.txtGrupoIndicadores.Maxlength = 3
		Me.txtGrupoIndicadores.TabIndex = 1
		Me.txtGrupoIndicadores.Tag = "*1"
		Me.txtGrupoIndicadores.AcceptsReturn = True
		Me.txtGrupoIndicadores.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGrupoIndicadores.BackColor = System.Drawing.SystemColors.Window
		Me.txtGrupoIndicadores.CausesValidation = True
		Me.txtGrupoIndicadores.Enabled = True
		Me.txtGrupoIndicadores.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGrupoIndicadores.HideSelection = True
		Me.txtGrupoIndicadores.ReadOnly = False
		Me.txtGrupoIndicadores.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGrupoIndicadores.MultiLine = False
		Me.txtGrupoIndicadores.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGrupoIndicadores.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGrupoIndicadores.TabStop = True
		Me.txtGrupoIndicadores.Visible = True
		Me.txtGrupoIndicadores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGrupoIndicadores.Name = "txtGrupoIndicadores"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(394, 19)
		Me.Text2.Location = New System.Drawing.Point(159, 11)
		Me.Text2.TabIndex = 2
		Me.Text2.Tag = "^1"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtIndicador.AutoSize = False
		Me.txtIndicador.Size = New System.Drawing.Size(31, 19)
		Me.txtIndicador.Location = New System.Drawing.Point(124, 35)
		Me.txtIndicador.Maxlength = 3
		Me.txtIndicador.TabIndex = 4
		Me.txtIndicador.Tag = "*2"
		Me.txtIndicador.AcceptsReturn = True
		Me.txtIndicador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtIndicador.BackColor = System.Drawing.SystemColors.Window
		Me.txtIndicador.CausesValidation = True
		Me.txtIndicador.Enabled = True
		Me.txtIndicador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtIndicador.HideSelection = True
		Me.txtIndicador.ReadOnly = False
		Me.txtIndicador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtIndicador.MultiLine = False
		Me.txtIndicador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtIndicador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtIndicador.TabStop = True
		Me.txtIndicador.Visible = True
		Me.txtIndicador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtIndicador.Name = "txtIndicador"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(396, 19)
		Me.Text3.Location = New System.Drawing.Point(159, 35)
		Me.Text3.TabIndex = 5
		Me.Text3.Tag = "^2"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtParametro.AutoSize = False
		Me.txtParametro.Size = New System.Drawing.Size(431, 77)
		Me.txtParametro.Location = New System.Drawing.Point(124, 163)
		Me.txtParametro.Maxlength = 250
		Me.txtParametro.MultiLine = True
		Me.txtParametro.TabIndex = 7
		Me.txtParametro.Tag = "3"
		Me.txtParametro.AcceptsReturn = True
		Me.txtParametro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtParametro.BackColor = System.Drawing.SystemColors.Window
		Me.txtParametro.CausesValidation = True
		Me.txtParametro.Enabled = True
		Me.txtParametro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtParametro.HideSelection = True
		Me.txtParametro.ReadOnly = False
		Me.txtParametro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtParametro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtParametro.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtParametro.TabStop = True
		Me.txtParametro.Visible = True
		Me.txtParametro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtParametro.Name = "txtParametro"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(379, 255)
		Me.cmdAceptar.TabIndex = 8
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(469, 255)
		Me.cmdGuardar.TabIndex = 9
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl2.Text = "Grupo indicadores"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 15)
		Me.lbl2.TabIndex = 0
		Me.lbl2.Tag = "1"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Indicador"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 39)
		Me.lbl3.TabIndex = 3
		Me.lbl3.Tag = "2"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Par�metro"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 165)
		Me.lbl4.TabIndex = 6
		Me.lbl4.Tag = "3"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 254)
		Me.lblLock.TabIndex = 10
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 248
		Me.Line1.Y2 = 248
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 249
		Me.Line2.Y2 = 249
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkRequiereRecalculo, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(GroupBox1)
		Me.Controls.Add(txtGrupoIndicadores)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtIndicador)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtParametro)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.GroupBox1.Controls.Add(txtObservacionesParametro)
		Me.GroupBox1.Controls.Add(chkRequiereRecalculo)
		Me.GroupBox1.Controls.Add(lblRecalcul)
		Me.GroupBox1.Controls.Add(lbl5)
		Me.GroupBox1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
