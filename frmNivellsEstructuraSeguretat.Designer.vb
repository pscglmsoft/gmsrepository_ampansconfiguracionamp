<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmNivellsEstructuraSeguretat
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents chkVeureTot As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents trvEstructura As System.Windows.Forms.TreeView
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtUsuari As System.Windows.Forms.TextBox
	Public WithEvents cmdCerrar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents ImageList3 As System.Windows.Forms.ImageList
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmNivellsEstructuraSeguretat))
		Me.components = New System.ComponentModel.Container()
		Me.chkVeureTot = New AxXtremeSuiteControls.AxCheckBox
		Me.trvEstructura = New System.Windows.Forms.TreeView
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtUsuari = New System.Windows.Forms.TextBox
		Me.cmdCerrar = New AxXtremeSuiteControls.AxPushButton
		Me.ImageList3 = New System.Windows.Forms.ImageList
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkVeureTot, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCerrar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Niveles de estructura. Seguridad por usuario"
		Me.ClientSize = New System.Drawing.Size(464, 544)
		Me.Location = New System.Drawing.Point(468, 294)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmNivellsEstructuraSeguretat"
		chkVeureTot.OcxState = CType(resources.GetObject("chkVeureTot.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkVeureTot.Size = New System.Drawing.Size(175, 19)
		Me.chkVeureTot.Location = New System.Drawing.Point(16, 36)
		Me.chkVeureTot.TabIndex = 6
		Me.chkVeureTot.Name = "chkVeureTot"
		Me.trvEstructura.CausesValidation = True
		Me.trvEstructura.Size = New System.Drawing.Size(419, 433)
		Me.trvEstructura.Location = New System.Drawing.Point(20, 64)
		Me.trvEstructura.TabIndex = 3
		Me.trvEstructura.HideSelection = False
		Me.trvEstructura.Indent = 2
		Me.trvEstructura.LabelEdit = False
		Me.trvEstructura.CheckBoxes = True
		Me.trvEstructura.ImageList = ImageList3
		Me.trvEstructura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.trvEstructura.Name = "trvEstructura"
		Me.Text2.AutoSize = False
		Me.Text2.Enabled = False
		Me.Text2.Size = New System.Drawing.Size(267, 19)
		Me.Text2.Location = New System.Drawing.Point(172, 12)
		Me.Text2.TabIndex = 2
		Me.Text2.Tag = "^1"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.BackColor = System.Drawing.SystemColors.Window
		Me.Text2.CausesValidation = True
		Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtUsuari.AutoSize = False
		Me.txtUsuari.Size = New System.Drawing.Size(97, 19)
		Me.txtUsuari.Location = New System.Drawing.Point(72, 12)
		Me.txtUsuari.TabIndex = 0
		Me.txtUsuari.Tag = "1####I-USUARIS#1#1#"
		Me.txtUsuari.AcceptsReturn = True
		Me.txtUsuari.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuari.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuari.CausesValidation = True
		Me.txtUsuari.Enabled = True
		Me.txtUsuari.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuari.HideSelection = True
		Me.txtUsuari.ReadOnly = False
		Me.txtUsuari.Maxlength = 0
		Me.txtUsuari.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuari.MultiLine = False
		Me.txtUsuari.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuari.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuari.TabStop = True
		Me.txtUsuari.Visible = True
		Me.txtUsuari.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuari.Name = "txtUsuari"
		cmdCerrar.OcxState = CType(resources.GetObject("cmdCerrar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCerrar.Size = New System.Drawing.Size(83, 27)
		Me.cmdCerrar.Location = New System.Drawing.Point(352, 504)
		Me.cmdCerrar.TabIndex = 4
		Me.cmdCerrar.Name = "cmdCerrar"
		Me.ImageList3.ImageSize = New System.Drawing.Size(17, 15)
		Me.ImageList3.TransparentColor = System.Drawing.Color.FromARGB(192, 192, 192)
Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList3.Images.SetKeyName(0, "")
		Me.ImageList3.Images.SetKeyName(1, "")
		Me.ImageList3.Images.SetKeyName(2, "")
		Me.ImageList3.Images.SetKeyName(3, "")
		Me.ImageList3.Images.SetKeyName(4, "")
		Me.ImageList3.Images.SetKeyName(5, "")
		Me.ImageList3.Images.SetKeyName(6, "")
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(260, 504)
		Me.cmdGuardar.TabIndex = 5
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "Usuario"
		Me.Label1.Size = New System.Drawing.Size(53, 15)
		Me.Label1.Location = New System.Drawing.Point(16, 14)
		Me.Label1.TabIndex = 1
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdCerrar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkVeureTot, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(chkVeureTot)
		Me.Controls.Add(trvEstructura)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtUsuari)
		Me.Controls.Add(cmdCerrar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
