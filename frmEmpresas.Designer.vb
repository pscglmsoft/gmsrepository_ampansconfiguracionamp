<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEmpresas
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtCodigoEmpresa As System.Windows.Forms.TextBox
	Public WithEvents txtNombreEmpresa As System.Windows.Forms.TextBox
	Public WithEvents cmbClaseIvaCli As DataControl.GmsCombo
	Public WithEvents cmbIVA As DataControl.GmsCombo
	Public WithEvents txtSerie As System.Windows.Forms.TextBox
	Public WithEvents chkIRPF As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmbTipoSerieVen As DataControl.GmsCombo
	Public WithEvents cmbPerfilUsuarios As DataControl.GmsCombo
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label29 As System.Windows.Forms.Label
	Public WithEvents Label39 As System.Windows.Forms.Label
	Public WithEvents XPFrame303 As DataControl.GmsGroupBox
	Public WithEvents lbl19 As System.Windows.Forms.Label
	Public WithEvents lbl18 As System.Windows.Forms.Label
	Public WithEvents TabControlPage2 As GmsTabControl.GmsTabPage
	Public WithEvents txtEmail As System.Windows.Forms.TextBox
	Public WithEvents txtSubDireccion As System.Windows.Forms.TextBox
	Public WithEvents txtFax As System.Windows.Forms.TextBox
	Public WithEvents txtTelefono As System.Windows.Forms.TextBox
	Public WithEvents txtNif As System.Windows.Forms.TextBox
	Public WithEvents txtProvincia As System.Windows.Forms.TextBox
	Public WithEvents txtPoblacion As System.Windows.Forms.TextBox
	Public WithEvents txtDireccion As System.Windows.Forms.TextBox
	Public WithEvents txtNombreFiscal As System.Windows.Forms.TextBox
	Public WithEvents txtUrl As System.Windows.Forms.TextBox
	Public WithEvents cmbMonedaBase As DataControl.GmsCombo
	Public WithEvents cmbIdiomaPredeterminado As DataControl.GmsCombo
	Public WithEvents lbl13 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents FrameDadesEmpresa As DataControl.GmsGroupBox
	Public WithEvents TabControlPage1 As GmsTabControl.GmsTabPage
	Public WithEvents TabControl1 As DataControl.GmsTabControl
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEmpresas))
		Me.components = New System.ComponentModel.Container()
		Me.txtCodigoEmpresa = New System.Windows.Forms.TextBox
		Me.txtNombreEmpresa = New System.Windows.Forms.TextBox
		Me.TabControl1 = New DataControl.GmsTabControl
		Me.TabControlPage2 = New GmsTabControl.GmsTabPage
		Me.cmbClaseIvaCli = New DataControl.GmsCombo
		Me.cmbIVA = New DataControl.GmsCombo
		Me.XPFrame303 = New DataControl.GmsGroupBox
		Me.txtSerie = New System.Windows.Forms.TextBox
		Me.chkIRPF = New AxXtremeSuiteControls.AxCheckBox
		Me.cmbTipoSerieVen = New DataControl.GmsCombo
		Me.cmbPerfilUsuarios = New DataControl.GmsCombo
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label29 = New System.Windows.Forms.Label
		Me.Label39 = New System.Windows.Forms.Label
		Me.lbl19 = New System.Windows.Forms.Label
		Me.lbl18 = New System.Windows.Forms.Label
		Me.TabControlPage1 = New GmsTabControl.GmsTabPage
		Me.FrameDadesEmpresa = New DataControl.GmsGroupBox
		Me.txtEmail = New System.Windows.Forms.TextBox
		Me.txtSubDireccion = New System.Windows.Forms.TextBox
		Me.txtFax = New System.Windows.Forms.TextBox
		Me.txtTelefono = New System.Windows.Forms.TextBox
		Me.txtNif = New System.Windows.Forms.TextBox
		Me.txtProvincia = New System.Windows.Forms.TextBox
		Me.txtPoblacion = New System.Windows.Forms.TextBox
		Me.txtDireccion = New System.Windows.Forms.TextBox
		Me.txtNombreFiscal = New System.Windows.Forms.TextBox
		Me.txtUrl = New System.Windows.Forms.TextBox
		Me.cmbMonedaBase = New DataControl.GmsCombo
		Me.cmbIdiomaPredeterminado = New DataControl.GmsCombo
		Me.lbl13 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.XPFrame303.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.FrameDadesEmpresa.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True


		CType(Me.chkIRPF, System.ComponentModel.ISupportInitialize).BeginInit()



		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()



		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Mantenimiento de empresas"
		Me.ClientSize = New System.Drawing.Size(705, 510)
		Me.Location = New System.Drawing.Point(193, 162)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-EMPRESAS ENT"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmEmpresas"
		Me.txtCodigoEmpresa.AutoSize = False
		Me.txtCodigoEmpresa.Size = New System.Drawing.Size(53, 19)
		Me.txtCodigoEmpresa.Location = New System.Drawing.Point(120, 8)
		Me.txtCodigoEmpresa.Maxlength = 4
		Me.txtCodigoEmpresa.TabIndex = 0
		Me.txtCodigoEmpresa.Tag = "*1"
		Me.txtCodigoEmpresa.AcceptsReturn = True
		Me.txtCodigoEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoEmpresa.CausesValidation = True
		Me.txtCodigoEmpresa.Enabled = True
		Me.txtCodigoEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoEmpresa.HideSelection = True
		Me.txtCodigoEmpresa.ReadOnly = False
		Me.txtCodigoEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoEmpresa.MultiLine = False
		Me.txtCodigoEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoEmpresa.TabStop = True
		Me.txtCodigoEmpresa.Visible = True
		Me.txtCodigoEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoEmpresa.Name = "txtCodigoEmpresa"
		Me.txtNombreEmpresa.AutoSize = False
		Me.txtNombreEmpresa.Enabled = False
		Me.txtNombreEmpresa.Size = New System.Drawing.Size(414, 19)
		Me.txtNombreEmpresa.Location = New System.Drawing.Point(177, 8)
		Me.txtNombreEmpresa.Maxlength = 40
		Me.txtNombreEmpresa.TabIndex = 44
		Me.txtNombreEmpresa.Tag = "^1"
		Me.txtNombreEmpresa.AcceptsReturn = True
		Me.txtNombreEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreEmpresa.CausesValidation = True
		Me.txtNombreEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreEmpresa.HideSelection = True
		Me.txtNombreEmpresa.ReadOnly = False
		Me.txtNombreEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreEmpresa.MultiLine = False
		Me.txtNombreEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreEmpresa.TabStop = True
		Me.txtNombreEmpresa.Visible = True
		Me.txtNombreEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreEmpresa.Name = "txtNombreEmpresa"

		Me.TabControl1.Size = New System.Drawing.Size(695, 435)
		Me.TabControl1.Location = New System.Drawing.Point(6, 36)
		Me.TabControl1.TabIndex = 22
		Me.TabControl1.Name = "TabControl1"

		Me.TabControlPage2.Size = New System.Drawing.Size(691, 411)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
Me.TabControlPage2.BackColor = System.Drawing.ColorTranslator.FromOle(16777215)
		Me.TabControlPage2.TabIndex = 24
Me.TabControlPage2.Text = "Par�metros"
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"

		Me.cmbClaseIvaCli.Size = New System.Drawing.Size(261, 19)
		Me.cmbClaseIvaCli.Location = New System.Drawing.Point(216, 44)
		Me.cmbClaseIvaCli.TabIndex = 13
Me.cmbClaseIvaCli.Tag = "18###########2"
		Me.cmbClaseIvaCli.Name = "cmbClaseIvaCli"

		Me.cmbIVA.Size = New System.Drawing.Size(261, 19)
		Me.cmbIVA.Location = New System.Drawing.Point(216, 68)
		Me.cmbIVA.TabIndex = 14
Me.cmbIVA.Tag = "19###########2"
		Me.cmbIVA.Name = "cmbIVA"

		Me.XPFrame303.Size = New System.Drawing.Size(649, 117)
		Me.XPFrame303.Location = New System.Drawing.Point(20, 102)
Me.XPFrame303.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder
		Me.XPFrame303.TabIndex = 27
Me.XPFrame303.Text = "Ventas"
		Me.XPFrame303.Name = "XPFrame303"
		Me.txtSerie.AutoSize = False
		Me.txtSerie.Size = New System.Drawing.Size(47, 19)
		Me.txtSerie.Location = New System.Drawing.Point(252, 26)
		Me.txtSerie.Maxlength = 10
		Me.txtSerie.TabIndex = 15
		Me.txtSerie.Tag = "33###########2"
		Me.txtSerie.AcceptsReturn = True
		Me.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSerie.BackColor = System.Drawing.SystemColors.Window
		Me.txtSerie.CausesValidation = True
		Me.txtSerie.Enabled = True
		Me.txtSerie.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSerie.HideSelection = True
		Me.txtSerie.ReadOnly = False
		Me.txtSerie.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSerie.MultiLine = False
		Me.txtSerie.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSerie.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSerie.TabStop = True
		Me.txtSerie.Visible = True
		Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSerie.Name = "txtSerie"
		chkIRPF.OcxState = CType(resources.GetObject("chkIRPF.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkIRPF.Size = New System.Drawing.Size(245, 19)
		Me.chkIRPF.Location = New System.Drawing.Point(20, 50)
		Me.chkIRPF.TabIndex = 17
Me.chkIRPF.Tag = "86###########2"
		Me.chkIRPF.Name = "chkIRPF"

		Me.cmbTipoSerieVen.Size = New System.Drawing.Size(145, 19)
		Me.cmbTipoSerieVen.Location = New System.Drawing.Point(480, 26)
		Me.cmbTipoSerieVen.TabIndex = 16
Me.cmbTipoSerieVen.Tag = "34###########2"
		Me.cmbTipoSerieVen.Name = "cmbTipoSerieVen"

		Me.cmbPerfilUsuarios.Size = New System.Drawing.Size(261, 19)
		Me.cmbPerfilUsuarios.Location = New System.Drawing.Point(252, 74)
		Me.cmbPerfilUsuarios.TabIndex = 18
Me.cmbPerfilUsuarios.Tag = "25###########2"
		Me.cmbPerfilUsuarios.Name = "cmbPerfilUsuarios"
		Me.Label1.Text = "Serie de facturaci�n de las quotas"
		Me.Label1.Size = New System.Drawing.Size(221, 15)
		Me.Label1.Location = New System.Drawing.Point(20, 28)
		Me.Label1.TabIndex = 30
		Me.Label1.Tag = "33"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Label29.Text = "Perfil contable predeterminado usuarios"
		Me.Label29.Size = New System.Drawing.Size(221, 15)
		Me.Label29.Location = New System.Drawing.Point(20, 76)
		Me.Label29.TabIndex = 29
		Me.Label29.Tag = "25"
		Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label29.BackColor = System.Drawing.SystemColors.Control
		Me.Label29.Enabled = True
		Me.Label29.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label29.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label29.UseMnemonic = True
		Me.Label29.Visible = True
		Me.Label29.AutoSize = False
		Me.Label29.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label29.Name = "Label29"
		Me.Label39.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label39.Text = "Tipo serie de facturas"
		Me.Label39.Size = New System.Drawing.Size(142, 15)
		Me.Label39.Location = New System.Drawing.Point(322, 30)
		Me.Label39.TabIndex = 28
		Me.Label39.Tag = "34"
		Me.Label39.BackColor = System.Drawing.SystemColors.Control
		Me.Label39.Enabled = True
		Me.Label39.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label39.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label39.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label39.UseMnemonic = True
		Me.Label39.Visible = True
		Me.Label39.AutoSize = False
		Me.Label39.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label39.Name = "Label39"
		Me.lbl19.Text = "C�digo IVA predeterminado"
		Me.lbl19.Size = New System.Drawing.Size(194, 15)
		Me.lbl19.Location = New System.Drawing.Point(24, 72)
		Me.lbl19.TabIndex = 26
		Me.lbl19.Tag = "19"
		Me.lbl19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl19.BackColor = System.Drawing.SystemColors.Control
		Me.lbl19.Enabled = True
		Me.lbl19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl19.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl19.UseMnemonic = True
		Me.lbl19.Visible = True
		Me.lbl19.AutoSize = False
		Me.lbl19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl19.Name = "lbl19"
		Me.lbl18.Text = "Clase IVA predeterminada usuarios"
		Me.lbl18.Size = New System.Drawing.Size(194, 15)
		Me.lbl18.Location = New System.Drawing.Point(24, 48)
Me.lbl18.BackColor = System.Drawing.ColorTranslator.FromOle(16777215)
		Me.lbl18.TabIndex = 25
		Me.lbl18.Tag = "18"
		Me.lbl18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl18.BackColor = System.Drawing.SystemColors.Control
		Me.lbl18.Enabled = True
		Me.lbl18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl18.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl18.UseMnemonic = True
		Me.lbl18.Visible = True
		Me.lbl18.AutoSize = False
		Me.lbl18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl18.Name = "lbl18"

		Me.TabControlPage1.Size = New System.Drawing.Size(691, 411)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 23
Me.TabControlPage1.Text = "Datos generales"
		Me.TabControlPage1.Name = "TabControlPage1"

		Me.FrameDadesEmpresa.Size = New System.Drawing.Size(677, 399)
		Me.FrameDadesEmpresa.Location = New System.Drawing.Point(8, 6)
Me.FrameDadesEmpresa.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder
		Me.FrameDadesEmpresa.TabIndex = 31
		Me.FrameDadesEmpresa.Name = "FrameDadesEmpresa"
		Me.txtEmail.AutoSize = False
		Me.txtEmail.Size = New System.Drawing.Size(258, 19)
		Me.txtEmail.Location = New System.Drawing.Point(154, 224)
		Me.txtEmail.Maxlength = 25
		Me.txtEmail.TabIndex = 9
		Me.txtEmail.AcceptsReturn = True
		Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmail.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmail.CausesValidation = True
		Me.txtEmail.Enabled = True
		Me.txtEmail.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmail.HideSelection = True
		Me.txtEmail.ReadOnly = False
		Me.txtEmail.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmail.MultiLine = False
		Me.txtEmail.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmail.TabStop = True
		Me.txtEmail.Visible = True
		Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmail.Name = "txtEmail"
		Me.txtSubDireccion.AutoSize = False
		Me.txtSubDireccion.Size = New System.Drawing.Size(414, 19)
		Me.txtSubDireccion.Location = New System.Drawing.Point(154, 80)
		Me.txtSubDireccion.Maxlength = 40
		Me.txtSubDireccion.TabIndex = 3
		Me.txtSubDireccion.AcceptsReturn = True
		Me.txtSubDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubDireccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubDireccion.CausesValidation = True
		Me.txtSubDireccion.Enabled = True
		Me.txtSubDireccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubDireccion.HideSelection = True
		Me.txtSubDireccion.ReadOnly = False
		Me.txtSubDireccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubDireccion.MultiLine = False
		Me.txtSubDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubDireccion.TabStop = True
		Me.txtSubDireccion.Visible = True
		Me.txtSubDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubDireccion.Name = "txtSubDireccion"
		Me.txtFax.AutoSize = False
		Me.txtFax.Size = New System.Drawing.Size(104, 19)
		Me.txtFax.Location = New System.Drawing.Point(154, 200)
		Me.txtFax.Maxlength = 10
		Me.txtFax.TabIndex = 8
		Me.txtFax.AcceptsReturn = True
		Me.txtFax.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFax.BackColor = System.Drawing.SystemColors.Window
		Me.txtFax.CausesValidation = True
		Me.txtFax.Enabled = True
		Me.txtFax.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFax.HideSelection = True
		Me.txtFax.ReadOnly = False
		Me.txtFax.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFax.MultiLine = False
		Me.txtFax.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFax.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFax.TabStop = True
		Me.txtFax.Visible = True
		Me.txtFax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFax.Name = "txtFax"
		Me.txtTelefono.AutoSize = False
		Me.txtTelefono.Size = New System.Drawing.Size(103, 19)
		Me.txtTelefono.Location = New System.Drawing.Point(154, 176)
		Me.txtTelefono.Maxlength = 9
		Me.txtTelefono.TabIndex = 7
		Me.txtTelefono.AcceptsReturn = True
		Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefono.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefono.CausesValidation = True
		Me.txtTelefono.Enabled = True
		Me.txtTelefono.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefono.HideSelection = True
		Me.txtTelefono.ReadOnly = False
		Me.txtTelefono.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefono.MultiLine = False
		Me.txtTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefono.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefono.TabStop = True
		Me.txtTelefono.Visible = True
		Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefono.Name = "txtTelefono"
		Me.txtNif.AutoSize = False
		Me.txtNif.Size = New System.Drawing.Size(104, 19)
		Me.txtNif.Location = New System.Drawing.Point(154, 152)
		Me.txtNif.Maxlength = 10
		Me.txtNif.TabIndex = 6
		Me.txtNif.AcceptsReturn = True
		Me.txtNif.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNif.BackColor = System.Drawing.SystemColors.Window
		Me.txtNif.CausesValidation = True
		Me.txtNif.Enabled = True
		Me.txtNif.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNif.HideSelection = True
		Me.txtNif.ReadOnly = False
		Me.txtNif.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNif.MultiLine = False
		Me.txtNif.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNif.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNif.TabStop = True
		Me.txtNif.Visible = True
		Me.txtNif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNif.Name = "txtNif"
		Me.txtProvincia.AutoSize = False
		Me.txtProvincia.Size = New System.Drawing.Size(207, 19)
		Me.txtProvincia.Location = New System.Drawing.Point(154, 128)
		Me.txtProvincia.Maxlength = 20
		Me.txtProvincia.TabIndex = 5
		Me.txtProvincia.AcceptsReturn = True
		Me.txtProvincia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProvincia.BackColor = System.Drawing.SystemColors.Window
		Me.txtProvincia.CausesValidation = True
		Me.txtProvincia.Enabled = True
		Me.txtProvincia.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProvincia.HideSelection = True
		Me.txtProvincia.ReadOnly = False
		Me.txtProvincia.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProvincia.MultiLine = False
		Me.txtProvincia.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProvincia.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProvincia.TabStop = True
		Me.txtProvincia.Visible = True
		Me.txtProvincia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtProvincia.Name = "txtProvincia"
		Me.txtPoblacion.AutoSize = False
		Me.txtPoblacion.Size = New System.Drawing.Size(259, 19)
		Me.txtPoblacion.Location = New System.Drawing.Point(154, 104)
		Me.txtPoblacion.Maxlength = 25
		Me.txtPoblacion.TabIndex = 4
		Me.txtPoblacion.AcceptsReturn = True
		Me.txtPoblacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPoblacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtPoblacion.CausesValidation = True
		Me.txtPoblacion.Enabled = True
		Me.txtPoblacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPoblacion.HideSelection = True
		Me.txtPoblacion.ReadOnly = False
		Me.txtPoblacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPoblacion.MultiLine = False
		Me.txtPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPoblacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPoblacion.TabStop = True
		Me.txtPoblacion.Visible = True
		Me.txtPoblacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPoblacion.Name = "txtPoblacion"
		Me.txtDireccion.AutoSize = False
		Me.txtDireccion.Size = New System.Drawing.Size(414, 19)
		Me.txtDireccion.Location = New System.Drawing.Point(154, 56)
		Me.txtDireccion.Maxlength = 40
		Me.txtDireccion.TabIndex = 2
		Me.txtDireccion.AcceptsReturn = True
		Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDireccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDireccion.CausesValidation = True
		Me.txtDireccion.Enabled = True
		Me.txtDireccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDireccion.HideSelection = True
		Me.txtDireccion.ReadOnly = False
		Me.txtDireccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDireccion.MultiLine = False
		Me.txtDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDireccion.TabStop = True
		Me.txtDireccion.Visible = True
		Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDireccion.Name = "txtDireccion"
		Me.txtNombreFiscal.AutoSize = False
		Me.txtNombreFiscal.Size = New System.Drawing.Size(414, 19)
		Me.txtNombreFiscal.Location = New System.Drawing.Point(154, 32)
		Me.txtNombreFiscal.Maxlength = 40
		Me.txtNombreFiscal.TabIndex = 1
		Me.txtNombreFiscal.AcceptsReturn = True
		Me.txtNombreFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreFiscal.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreFiscal.CausesValidation = True
		Me.txtNombreFiscal.Enabled = True
		Me.txtNombreFiscal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreFiscal.HideSelection = True
		Me.txtNombreFiscal.ReadOnly = False
		Me.txtNombreFiscal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreFiscal.MultiLine = False
		Me.txtNombreFiscal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreFiscal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreFiscal.TabStop = True
		Me.txtNombreFiscal.Visible = True
		Me.txtNombreFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreFiscal.Name = "txtNombreFiscal"
		Me.txtUrl.AutoSize = False
		Me.txtUrl.Size = New System.Drawing.Size(258, 19)
		Me.txtUrl.Location = New System.Drawing.Point(154, 248)
		Me.txtUrl.Maxlength = 25
		Me.txtUrl.TabIndex = 10
		Me.txtUrl.AcceptsReturn = True
		Me.txtUrl.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUrl.BackColor = System.Drawing.SystemColors.Window
		Me.txtUrl.CausesValidation = True
		Me.txtUrl.Enabled = True
		Me.txtUrl.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUrl.HideSelection = True
		Me.txtUrl.ReadOnly = False
		Me.txtUrl.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUrl.MultiLine = False
		Me.txtUrl.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUrl.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUrl.TabStop = True
		Me.txtUrl.Visible = True
		Me.txtUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUrl.Name = "txtUrl"

		Me.cmbMonedaBase.Size = New System.Drawing.Size(161, 21)
		Me.cmbMonedaBase.Location = New System.Drawing.Point(154, 272)
		Me.cmbMonedaBase.TabIndex = 11
Me.cmbMonedaBase.Tag = "#########^GMON(*)#1#1"
		Me.cmbMonedaBase.Name = "cmbMonedaBase"

		Me.cmbIdiomaPredeterminado.Size = New System.Drawing.Size(161, 21)
		Me.cmbIdiomaPredeterminado.Location = New System.Drawing.Point(154, 298)
		Me.cmbIdiomaPredeterminado.TabIndex = 12
Me.cmbIdiomaPredeterminado.Tag = "#########^WCOMBO(""GEN"",8,*)#1"
		Me.cmbIdiomaPredeterminado.Name = "cmbIdiomaPredeterminado"
		Me.lbl13.Text = "Direcci�n web"
		Me.lbl13.Size = New System.Drawing.Size(111, 15)
		Me.lbl13.Location = New System.Drawing.Point(32, 252)
		Me.lbl13.TabIndex = 43
		Me.lbl13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl13.BackColor = System.Drawing.SystemColors.Control
		Me.lbl13.Enabled = True
		Me.lbl13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl13.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl13.UseMnemonic = True
		Me.lbl13.Visible = True
		Me.lbl13.AutoSize = False
		Me.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl13.Name = "lbl13"
		Me.lbl10.Text = "Email"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(32, 228)
		Me.lbl10.TabIndex = 42
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl11.Text = "Sub-direcci�n"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(32, 84)
		Me.lbl11.TabIndex = 41
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl9.Text = "Fax"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(32, 204)
		Me.lbl9.TabIndex = 40
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl8.Text = "Telefono"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(32, 180)
		Me.lbl8.TabIndex = 39
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl7.Text = "Nif"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(32, 156)
		Me.lbl7.TabIndex = 38
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Provincia"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(32, 132)
		Me.lbl6.TabIndex = 37
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl5.Text = "Poblaci�n"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(32, 108)
		Me.lbl5.TabIndex = 36
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl4.Text = "Direcci�n"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(32, 60)
		Me.lbl4.TabIndex = 35
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl3.Text = "Nombre fiscal"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(32, 36)
		Me.lbl3.TabIndex = 34
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.Label4.Text = "Moneda base"
		Me.Label4.Size = New System.Drawing.Size(117, 15)
		Me.Label4.Location = New System.Drawing.Point(32, 276)
		Me.Label4.TabIndex = 33
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label5.Text = "Idioma predeterminado"
		Me.Label5.Size = New System.Drawing.Size(117, 15)
		Me.Label5.Location = New System.Drawing.Point(32, 302)
		Me.Label5.TabIndex = 32
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(524, 478)
		Me.cmdAceptar.TabIndex = 19
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(614, 478)
		Me.cmdGuardar.TabIndex = 21
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "C�digo de empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(6, 12)
		Me.lbl1.TabIndex = 45
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 478)
		Me.lblLock.TabIndex = 20
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameDadesEmpresa, System.ComponentModel.ISupportInitialize).EndInit()


		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.XPFrame303, System.ComponentModel.ISupportInitialize).EndInit()


		CType(Me.chkIRPF, System.ComponentModel.ISupportInitialize).EndInit()


		Me.Controls.Add(txtCodigoEmpresa)
		Me.Controls.Add(txtNombreEmpresa)
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lblLock)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControlPage2.Controls.Add(cmbClaseIvaCli)
		Me.TabControlPage2.Controls.Add(cmbIVA)
		Me.TabControlPage2.Controls.Add(XPFrame303)
		Me.TabControlPage2.Controls.Add(lbl19)
		Me.TabControlPage2.Controls.Add(lbl18)
		Me.XPFrame303.Controls.Add(txtSerie)
		Me.XPFrame303.Controls.Add(chkIRPF)
		Me.XPFrame303.Controls.Add(cmbTipoSerieVen)
		Me.XPFrame303.Controls.Add(cmbPerfilUsuarios)
		Me.XPFrame303.Controls.Add(Label1)
		Me.XPFrame303.Controls.Add(Label29)
		Me.XPFrame303.Controls.Add(Label39)
		Me.TabControlPage1.Controls.Add(FrameDadesEmpresa)
		Me.FrameDadesEmpresa.Controls.Add(txtEmail)
		Me.FrameDadesEmpresa.Controls.Add(txtSubDireccion)
		Me.FrameDadesEmpresa.Controls.Add(txtFax)
		Me.FrameDadesEmpresa.Controls.Add(txtTelefono)
		Me.FrameDadesEmpresa.Controls.Add(txtNif)
		Me.FrameDadesEmpresa.Controls.Add(txtProvincia)
		Me.FrameDadesEmpresa.Controls.Add(txtPoblacion)
		Me.FrameDadesEmpresa.Controls.Add(txtDireccion)
		Me.FrameDadesEmpresa.Controls.Add(txtNombreFiscal)
		Me.FrameDadesEmpresa.Controls.Add(txtUrl)
		Me.FrameDadesEmpresa.Controls.Add(cmbMonedaBase)
		Me.FrameDadesEmpresa.Controls.Add(cmbIdiomaPredeterminado)
		Me.FrameDadesEmpresa.Controls.Add(lbl13)
		Me.FrameDadesEmpresa.Controls.Add(lbl10)
		Me.FrameDadesEmpresa.Controls.Add(lbl11)
		Me.FrameDadesEmpresa.Controls.Add(lbl9)
		Me.FrameDadesEmpresa.Controls.Add(lbl8)
		Me.FrameDadesEmpresa.Controls.Add(lbl7)
		Me.FrameDadesEmpresa.Controls.Add(lbl6)
		Me.FrameDadesEmpresa.Controls.Add(lbl5)
		Me.FrameDadesEmpresa.Controls.Add(lbl4)
		Me.FrameDadesEmpresa.Controls.Add(lbl3)
		Me.FrameDadesEmpresa.Controls.Add(Label4)
		Me.FrameDadesEmpresa.Controls.Add(Label5)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.XPFrame303.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.FrameDadesEmpresa.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
