Option Strict Off
Option Explicit On
Friend Class frmSistemasGestion
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	Private rowAct As Integer
	
	Public Overrides Sub FGetReg()
		If lblLock.Visible = True Then
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			grdImpresosGraf.Enabled = False
		Else
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			grdImpresosGraf.Enabled = True
		End If
		CarregaGrd()
	End Sub
	
	Private Sub GravaImpresos()
		Dim A As String
		Dim I As Short
		A = ""
		For I = 1 To grdImpresosGraf.Rows - 1
			If grdImpresosGraf.Cell(I, 4).Text <> "" Then
				If A <> "" Then A = A & "|"
				A = A & grdImpresosGraf.Cell(I, 2).Text & S & grdImpresosGraf.Cell(I, 4).Text
			End If
		Next I
		CacheNetejaParametres()
		
		MCache.P1 = txtEmpresa.Text
		MCache.P2 = txtSistema.Text
		MCache.P3 = A
		CacheXecute("D GRIMP^ENT.G.AUX0001")
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub CarregaGrd()
		If txtEmpresa.Text = "" Or txtSistema.Text = "" Then Exit Sub
		CacheNetejaParametres()
		CarregaFGrid(grdImpresosGraf, "CGIMPG^ENT.G.AUX0001", Me.txtEmpresa.Text & PC & Me.txtSistema.Text, CStr(3))
	End Sub
	
	Private Sub chkNoCerrarContabilidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNoCerrarContabilidad.GotFocus
		XGotFocus(Me, chkNoCerrarContabilidad)
	End Sub
	
	Private Sub chkNoCerrarContabilidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNoCerrarContabilidad.LostFocus
		XLostFocus(Me, chkNoCerrarContabilidad)
	End Sub
	
	Private Sub chkNoCerrarContabilidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles chkNoCerrarContabilidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, chkNoCerrarContabilidad)
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbLogoPredeterminado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbLogoPredeterminado.GotFocus
		XGotFocus(Me, cmbLogoPredeterminado)
	End Sub
	
	Private Sub cmbLogoPredeterminado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbLogoPredeterminado.LostFocus
		XLostFocus(Me, cmbLogoPredeterminado)
	End Sub
	
	Private Sub frmSistemasGestion_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		GravaImpresos()
		
		ResetForm(Me)
	End Sub
	
	Private Sub DefinirGrids()
		
		IniciFGrid(grdImpresosGraf, False)
		With grdImpresosGraf
			.Cols = 5
			.Rows = 1
			.DisplayFocusRect = False
			.AllowUserResizing = False
			.FixedRowColStyle = FlexCell.FixedRowColStyleEnum.Flat
			
			.Cell(0, 2).Text = GetString("C�digo")
			.Cell(0, 3).Text = GetString(" Descripci�n")
			.Cell(0, 4).Text = GetString(" Espec�fico")
			
			.Column(2).CellType = FlexCell.CellTypeEnum.TextBox
			.Column(3).CellType = FlexCell.CellTypeEnum.TextBox
			.Column(4).CellType = FlexCell.CellTypeEnum.TextBox
			
			.Column(2).Locked = True
			.Column(3).Locked = True
			
			.Column(2).Alignment = AlignmentEnum.LeftCenter
			.Column(3).Alignment = AlignmentEnum.LeftCenter
			.Column(4).Alignment = AlignmentEnum.LeftCenter
			
			.Column(0).Width = 0
			.Column(1).Width = 0
			.Column(2).Width = 0
			.Column(3).Width = 350 '258
			.Column(4).Width = 164 '120
		End With
		FGAplicaIdioma(grdImpresosGraf)
		FGCentrarTitolsColumnes(grdImpresosGraf)
	End Sub
	
	Private Sub frmSistemasGestion_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		DefinirGrids()
	End Sub
	
	Private Sub frmSistemasGestion_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmSistemasGestion_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub grdImpresosGraf_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As Grid.LeaveCellEventArgs) Handles grdImpresosGraf.LeaveCell
		Dim A As String
		If eventArgs.Row > 0 And eventArgs.Col = 4 Then
			CacheNetejaParametres()
			MCache.P1 = Me.txtEmpresa
			MCache.P2 = Me.txtSistema
			MCache.P3 = grdImpresosGraf.Cell(eventArgs.Row, 2).Text
			MCache.P4 = grdImpresosGraf.Cell(eventArgs.Row, 4).Text
			A = CacheXecute("D SIMPG^ENT.G.AUX0001")
			If A <> "" Then
				xMsgBox(A, MsgBoxStyle.Critical, Me.Text)
				eventArgs.Cancel = True
			End If
		End If
	End Sub
	
	Private Sub TabControl1_Selected(sender As Object, ByVal eventArgs As TabControlEventArgs) Handles TabControl1.Selected
		If TabControl1.SelectedItem = 2 Then
			CarregaGrd()
			If rowAct > 0 Then
				MarcaRowFG(grdImpresosGraf, rowAct)
				rowAct = 0
			End If
		End If
	End Sub
	
	Private Sub txtDatosRegistro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDatosRegistro.GotFocus
		XGotFocus(Me, txtDatosRegistro)
	End Sub
	
	Private Sub txtDatosRegistro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDatosRegistro.LostFocus
		XLostFocus(Me, txtDatosRegistro)
	End Sub
	
	Private Sub txtEmail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.GotFocus
		XGotFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.LostFocus
		XLostFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmail.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtEmpresa)
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresaFinanzas_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresaFinanzas.GotFocus
		XGotFocus(Me, txtEmpresaFinanzas)
	End Sub
	
	Private Sub txtEmpresaFinanzas_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresaFinanzas.LostFocus
		XLostFocus(Me, txtEmpresaFinanzas)
	End Sub
	
	Private Sub txtFax_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFax.GotFocus
		XGotFocus(Me, txtFax)
	End Sub
	
	Private Sub txtFax_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFax.LostFocus
		XLostFocus(Me, txtFax)
	End Sub
	
	Private Sub txtFax_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFax.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNamesFin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNamesFin.GotFocus
		XGotFocus(Me, txtNamesFin)
	End Sub
	
	Private Sub txtNamesFin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNamesFin.LostFocus
		XLostFocus(Me, txtNamesFin)
	End Sub
	
	Private Sub txtNif_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNif.GotFocus
		XGotFocus(Me, txtNif)
	End Sub
	
	Private Sub txtNif_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNif.LostFocus
		XLostFocus(Me, txtNif)
	End Sub
	
	Private Sub txtNombreEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreEmpresa.GotFocus
		XGotFocus(Me, txtNombreEmpresa)
	End Sub
	
	Private Sub txtNombreEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreEmpresa.LostFocus
		XLostFocus(Me, txtNombreEmpresa)
	End Sub
	
	Private Sub txtNombreFiscal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreFiscal.GotFocus
		XGotFocus(Me, txtNombreFiscal)
	End Sub
	
	Private Sub txtNombreFiscal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreFiscal.LostFocus
		XLostFocus(Me, txtNombreFiscal)
	End Sub
	
	Private Sub txtSisGesFina_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSisGesFina.GotFocus
		XGotFocus(Me, txtSisGesFina)
	End Sub
	
	Private Sub txtSisGesFina_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSisGesFina.LostFocus
		XLostFocus(Me, txtSisGesFina)
	End Sub
	
	Private Sub txtSistema_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSistema.GotFocus
		XGotFocus(Me, txtSistema)
	End Sub
	
	Private Sub txtSistema_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSistema.LostFocus
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtSistema)
	End Sub
	
	Private Sub txtDireccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccion.GotFocus
		XGotFocus(Me, txtDireccion)
	End Sub
	
	Private Sub txtDireccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccion.LostFocus
		Call XLostFocus(Me, txtDireccion)
	End Sub
	
	Private Sub txtPoblacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.GotFocus
		XGotFocus(Me, txtPoblacion)
	End Sub
	
	Private Sub txtPoblacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.LostFocus
		Call XLostFocus(Me, txtPoblacion)
	End Sub
	
	Private Sub txtProvincia_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProvincia.GotFocus
		XGotFocus(Me, txtProvincia)
	End Sub
	
	Private Sub txtProvincia_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProvincia.LostFocus
		Call XLostFocus(Me, txtProvincia)
	End Sub
	
	Private Sub txtNamespaceFinanzas_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNamespaceFinanzas.GotFocus
		XGotFocus(Me, txtNamespaceFinanzas)
	End Sub
	
	Private Sub txtNamespaceFinanzas_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNamespaceFinanzas.LostFocus
		Call XLostFocus(Me, txtNamespaceFinanzas)
	End Sub
	
	Private Sub txtSubdireccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubdireccion.GotFocus
		XGotFocus(Me, txtSubDireccion)
	End Sub
	
	Private Sub txtSubdireccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubdireccion.LostFocus
		XLostFocus(Me, txtSubDireccion)
	End Sub
	
	Private Sub txtSubdireccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubdireccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTelefono_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefono.GotFocus
		XGotFocus(Me, txtTelefono)
	End Sub
	
	Private Sub txtTelefono_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefono.LostFocus
		XLostFocus(Me, txtTelefono)
	End Sub
	
	Private Sub txtUrl_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUrl.GotFocus
		XGotFocus(Me, txtUrl)
	End Sub
	
	Private Sub txtUrl_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUrl.LostFocus
		XLostFocus(Me, txtUrl)
	End Sub
	
	Private Sub txtUrl_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUrl.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
