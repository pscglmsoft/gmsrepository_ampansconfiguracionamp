<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParametrosDietas
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtFactorPeajes As System.Windows.Forms.TextBox
	Public WithEvents txtFactorDesplazamientosI As System.Windows.Forms.TextBox
	Public WithEvents txtFactorDesplazamientos As System.Windows.Forms.TextBox
	Public WithEvents txtFactorDietasIRPF As System.Windows.Forms.TextBox
	Public WithEvents txtFactorDietas As System.Windows.Forms.TextBox
	Public WithEvents txtImporteDietaCompleta As DataControl.GmsImports
	Public WithEvents txtPrecioKilometro As DataControl.GmsImports
	Public WithEvents txtMedios As System.Windows.Forms.TextBox
	Public WithEvents txtHoteles As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtCuentaKilometraje As System.Windows.Forms.TextBox
	Public WithEvents txtCuentaDietas As System.Windows.Forms.TextBox
	Public WithEvents txtCuentaParkings As System.Windows.Forms.TextBox
	Public WithEvents txtCuentaPeajes As System.Windows.Forms.TextBox
	Public WithEvents txtCuentaOtros As System.Windows.Forms.TextBox
	Public WithEvents txtCuentaCaja As System.Windows.Forms.TextBox
	Public WithEvents txtCategoriaOperacio As System.Windows.Forms.TextBox
	Public WithEvents txtSerieDocuments As System.Windows.Forms.TextBox
	Public WithEvents txtDiarioContable As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtImporteMediaDieta As DataControl.GmsImports
	Public WithEvents lbl25 As System.Windows.Forms.Label
	Public WithEvents lbl24 As System.Windows.Forms.Label
	Public WithEvents lbl23 As System.Windows.Forms.Label
	Public WithEvents lbl22 As System.Windows.Forms.Label
	Public WithEvents lbl21 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParametrosDietas))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtFactorPeajes = New System.Windows.Forms.TextBox
		Me.txtFactorDesplazamientosI = New System.Windows.Forms.TextBox
		Me.txtFactorDesplazamientos = New System.Windows.Forms.TextBox
		Me.txtFactorDietasIRPF = New System.Windows.Forms.TextBox
		Me.txtFactorDietas = New System.Windows.Forms.TextBox
		Me.txtImporteDietaCompleta = New DataControl.GmsImports
		Me.txtPrecioKilometro = New DataControl.GmsImports
		Me.txtMedios = New System.Windows.Forms.TextBox
		Me.txtHoteles = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtCuentaKilometraje = New System.Windows.Forms.TextBox
		Me.txtCuentaDietas = New System.Windows.Forms.TextBox
		Me.txtCuentaParkings = New System.Windows.Forms.TextBox
		Me.txtCuentaPeajes = New System.Windows.Forms.TextBox
		Me.txtCuentaOtros = New System.Windows.Forms.TextBox
		Me.txtCuentaCaja = New System.Windows.Forms.TextBox
		Me.txtCategoriaOperacio = New System.Windows.Forms.TextBox
		Me.txtSerieDocuments = New System.Windows.Forms.TextBox
		Me.txtDiarioContable = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.txtImporteMediaDieta = New DataControl.GmsImports
		Me.lbl25 = New System.Windows.Forms.Label
		Me.lbl24 = New System.Windows.Forms.Label
		Me.lbl23 = New System.Windows.Forms.Label
		Me.lbl22 = New System.Windows.Forms.Label
		Me.lbl21 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtImporteDietaCompleta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtPrecioKilometro, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtImporteMediaDieta, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Parametros dietas"
		Me.ClientSize = New System.Drawing.Size(498, 321)
		Me.Location = New System.Drawing.Point(444, 284)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-RRHH DIETES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParametrosDietas"
		Me.txtFactorPeajes.AutoSize = False
		Me.txtFactorPeajes.Size = New System.Drawing.Size(52, 19)
		Me.txtFactorPeajes.Location = New System.Drawing.Point(402, 178)
		Me.txtFactorPeajes.Maxlength = 5
		Me.txtFactorPeajes.TabIndex = 17
		Me.txtFactorPeajes.Tag = "25"
		Me.txtFactorPeajes.AcceptsReturn = True
		Me.txtFactorPeajes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFactorPeajes.BackColor = System.Drawing.SystemColors.Window
		Me.txtFactorPeajes.CausesValidation = True
		Me.txtFactorPeajes.Enabled = True
		Me.txtFactorPeajes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFactorPeajes.HideSelection = True
		Me.txtFactorPeajes.ReadOnly = False
		Me.txtFactorPeajes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFactorPeajes.MultiLine = False
		Me.txtFactorPeajes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFactorPeajes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFactorPeajes.TabStop = True
		Me.txtFactorPeajes.Visible = True
		Me.txtFactorPeajes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFactorPeajes.Name = "txtFactorPeajes"
		Me.txtFactorDesplazamientosI.AutoSize = False
		Me.txtFactorDesplazamientosI.Size = New System.Drawing.Size(52, 19)
		Me.txtFactorDesplazamientosI.Location = New System.Drawing.Point(402, 154)
		Me.txtFactorDesplazamientosI.Maxlength = 5
		Me.txtFactorDesplazamientosI.TabIndex = 16
		Me.txtFactorDesplazamientosI.Tag = "24"
		Me.txtFactorDesplazamientosI.AcceptsReturn = True
		Me.txtFactorDesplazamientosI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFactorDesplazamientosI.BackColor = System.Drawing.SystemColors.Window
		Me.txtFactorDesplazamientosI.CausesValidation = True
		Me.txtFactorDesplazamientosI.Enabled = True
		Me.txtFactorDesplazamientosI.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFactorDesplazamientosI.HideSelection = True
		Me.txtFactorDesplazamientosI.ReadOnly = False
		Me.txtFactorDesplazamientosI.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFactorDesplazamientosI.MultiLine = False
		Me.txtFactorDesplazamientosI.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFactorDesplazamientosI.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFactorDesplazamientosI.TabStop = True
		Me.txtFactorDesplazamientosI.Visible = True
		Me.txtFactorDesplazamientosI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFactorDesplazamientosI.Name = "txtFactorDesplazamientosI"
		Me.txtFactorDesplazamientos.AutoSize = False
		Me.txtFactorDesplazamientos.Size = New System.Drawing.Size(52, 19)
		Me.txtFactorDesplazamientos.Location = New System.Drawing.Point(402, 130)
		Me.txtFactorDesplazamientos.Maxlength = 5
		Me.txtFactorDesplazamientos.TabIndex = 15
		Me.txtFactorDesplazamientos.Tag = "23"
		Me.txtFactorDesplazamientos.AcceptsReturn = True
		Me.txtFactorDesplazamientos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFactorDesplazamientos.BackColor = System.Drawing.SystemColors.Window
		Me.txtFactorDesplazamientos.CausesValidation = True
		Me.txtFactorDesplazamientos.Enabled = True
		Me.txtFactorDesplazamientos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFactorDesplazamientos.HideSelection = True
		Me.txtFactorDesplazamientos.ReadOnly = False
		Me.txtFactorDesplazamientos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFactorDesplazamientos.MultiLine = False
		Me.txtFactorDesplazamientos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFactorDesplazamientos.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFactorDesplazamientos.TabStop = True
		Me.txtFactorDesplazamientos.Visible = True
		Me.txtFactorDesplazamientos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFactorDesplazamientos.Name = "txtFactorDesplazamientos"
		Me.txtFactorDietasIRPF.AutoSize = False
		Me.txtFactorDietasIRPF.Size = New System.Drawing.Size(52, 19)
		Me.txtFactorDietasIRPF.Location = New System.Drawing.Point(402, 106)
		Me.txtFactorDietasIRPF.Maxlength = 5
		Me.txtFactorDietasIRPF.TabIndex = 14
		Me.txtFactorDietasIRPF.Tag = "22"
		Me.txtFactorDietasIRPF.AcceptsReturn = True
		Me.txtFactorDietasIRPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFactorDietasIRPF.BackColor = System.Drawing.SystemColors.Window
		Me.txtFactorDietasIRPF.CausesValidation = True
		Me.txtFactorDietasIRPF.Enabled = True
		Me.txtFactorDietasIRPF.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFactorDietasIRPF.HideSelection = True
		Me.txtFactorDietasIRPF.ReadOnly = False
		Me.txtFactorDietasIRPF.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFactorDietasIRPF.MultiLine = False
		Me.txtFactorDietasIRPF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFactorDietasIRPF.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFactorDietasIRPF.TabStop = True
		Me.txtFactorDietasIRPF.Visible = True
		Me.txtFactorDietasIRPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFactorDietasIRPF.Name = "txtFactorDietasIRPF"
		Me.txtFactorDietas.AutoSize = False
		Me.txtFactorDietas.Size = New System.Drawing.Size(52, 19)
		Me.txtFactorDietas.Location = New System.Drawing.Point(402, 82)
		Me.txtFactorDietas.Maxlength = 5
		Me.txtFactorDietas.TabIndex = 13
		Me.txtFactorDietas.Tag = "21"
		Me.txtFactorDietas.AcceptsReturn = True
		Me.txtFactorDietas.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFactorDietas.BackColor = System.Drawing.SystemColors.Window
		Me.txtFactorDietas.CausesValidation = True
		Me.txtFactorDietas.Enabled = True
		Me.txtFactorDietas.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFactorDietas.HideSelection = True
		Me.txtFactorDietas.ReadOnly = False
		Me.txtFactorDietas.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFactorDietas.MultiLine = False
		Me.txtFactorDietas.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFactorDietas.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFactorDietas.TabStop = True
		Me.txtFactorDietas.Visible = True
		Me.txtFactorDietas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFactorDietas.Name = "txtFactorDietas"

		Me.txtImporteDietaCompleta.Size = New System.Drawing.Size(62, 19)
		Me.txtImporteDietaCompleta.Location = New System.Drawing.Point(402, 58)
		Me.txtImporteDietaCompleta.TabIndex = 4
Me.txtImporteDietaCompleta.Tag = "15"
		Me.txtImporteDietaCompleta.Name = "txtImporteDietaCompleta"

		Me.txtPrecioKilometro.Size = New System.Drawing.Size(62, 19)
		Me.txtPrecioKilometro.Location = New System.Drawing.Point(142, 34)
		Me.txtPrecioKilometro.TabIndex = 2
Me.txtPrecioKilometro.Tag = "13"
		Me.txtPrecioKilometro.Name = "txtPrecioKilometro"
		Me.txtMedios.AutoSize = False
		Me.txtMedios.Size = New System.Drawing.Size(62, 19)
		Me.txtMedios.Location = New System.Drawing.Point(142, 202)
		Me.txtMedios.Maxlength = 6
		Me.txtMedios.TabIndex = 10
		Me.txtMedios.Tag = "12"
		Me.txtMedios.AcceptsReturn = True
		Me.txtMedios.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMedios.BackColor = System.Drawing.SystemColors.Window
		Me.txtMedios.CausesValidation = True
		Me.txtMedios.Enabled = True
		Me.txtMedios.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMedios.HideSelection = True
		Me.txtMedios.ReadOnly = False
		Me.txtMedios.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMedios.MultiLine = False
		Me.txtMedios.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMedios.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMedios.TabStop = True
		Me.txtMedios.Visible = True
		Me.txtMedios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMedios.Name = "txtMedios"
		Me.txtHoteles.AutoSize = False
		Me.txtHoteles.Size = New System.Drawing.Size(62, 19)
		Me.txtHoteles.Location = New System.Drawing.Point(142, 178)
		Me.txtHoteles.Maxlength = 6
		Me.txtHoteles.TabIndex = 9
		Me.txtHoteles.Tag = "11"
		Me.txtHoteles.AcceptsReturn = True
		Me.txtHoteles.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtHoteles.BackColor = System.Drawing.SystemColors.Window
		Me.txtHoteles.CausesValidation = True
		Me.txtHoteles.Enabled = True
		Me.txtHoteles.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtHoteles.HideSelection = True
		Me.txtHoteles.ReadOnly = False
		Me.txtHoteles.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtHoteles.MultiLine = False
		Me.txtHoteles.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtHoteles.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtHoteles.TabStop = True
		Me.txtHoteles.Visible = True
		Me.txtHoteles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtHoteles.Name = "txtHoteles"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(62, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(142, 11)
		Me.txtEmpresa.Maxlength = 6
		Me.txtEmpresa.TabIndex = 1
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(259, 19)
		Me.Text1.Location = New System.Drawing.Point(207, 11)
		Me.Text1.TabIndex = 27
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtCuentaKilometraje.AutoSize = False
		Me.txtCuentaKilometraje.Size = New System.Drawing.Size(62, 19)
		Me.txtCuentaKilometraje.Location = New System.Drawing.Point(142, 83)
		Me.txtCuentaKilometraje.Maxlength = 6
		Me.txtCuentaKilometraje.TabIndex = 5
		Me.txtCuentaKilometraje.Tag = "2"
		Me.txtCuentaKilometraje.AcceptsReturn = True
		Me.txtCuentaKilometraje.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCuentaKilometraje.BackColor = System.Drawing.SystemColors.Window
		Me.txtCuentaKilometraje.CausesValidation = True
		Me.txtCuentaKilometraje.Enabled = True
		Me.txtCuentaKilometraje.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCuentaKilometraje.HideSelection = True
		Me.txtCuentaKilometraje.ReadOnly = False
		Me.txtCuentaKilometraje.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCuentaKilometraje.MultiLine = False
		Me.txtCuentaKilometraje.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCuentaKilometraje.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCuentaKilometraje.TabStop = True
		Me.txtCuentaKilometraje.Visible = True
		Me.txtCuentaKilometraje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCuentaKilometraje.Name = "txtCuentaKilometraje"
		Me.txtCuentaDietas.AutoSize = False
		Me.txtCuentaDietas.Size = New System.Drawing.Size(62, 19)
		Me.txtCuentaDietas.Location = New System.Drawing.Point(142, 107)
		Me.txtCuentaDietas.Maxlength = 6
		Me.txtCuentaDietas.TabIndex = 6
		Me.txtCuentaDietas.Tag = "3"
		Me.txtCuentaDietas.AcceptsReturn = True
		Me.txtCuentaDietas.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCuentaDietas.BackColor = System.Drawing.SystemColors.Window
		Me.txtCuentaDietas.CausesValidation = True
		Me.txtCuentaDietas.Enabled = True
		Me.txtCuentaDietas.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCuentaDietas.HideSelection = True
		Me.txtCuentaDietas.ReadOnly = False
		Me.txtCuentaDietas.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCuentaDietas.MultiLine = False
		Me.txtCuentaDietas.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCuentaDietas.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCuentaDietas.TabStop = True
		Me.txtCuentaDietas.Visible = True
		Me.txtCuentaDietas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCuentaDietas.Name = "txtCuentaDietas"
		Me.txtCuentaParkings.AutoSize = False
		Me.txtCuentaParkings.Size = New System.Drawing.Size(62, 19)
		Me.txtCuentaParkings.Location = New System.Drawing.Point(142, 131)
		Me.txtCuentaParkings.Maxlength = 6
		Me.txtCuentaParkings.TabIndex = 7
		Me.txtCuentaParkings.Tag = "4"
		Me.txtCuentaParkings.AcceptsReturn = True
		Me.txtCuentaParkings.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCuentaParkings.BackColor = System.Drawing.SystemColors.Window
		Me.txtCuentaParkings.CausesValidation = True
		Me.txtCuentaParkings.Enabled = True
		Me.txtCuentaParkings.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCuentaParkings.HideSelection = True
		Me.txtCuentaParkings.ReadOnly = False
		Me.txtCuentaParkings.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCuentaParkings.MultiLine = False
		Me.txtCuentaParkings.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCuentaParkings.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCuentaParkings.TabStop = True
		Me.txtCuentaParkings.Visible = True
		Me.txtCuentaParkings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCuentaParkings.Name = "txtCuentaParkings"
		Me.txtCuentaPeajes.AutoSize = False
		Me.txtCuentaPeajes.Size = New System.Drawing.Size(62, 19)
		Me.txtCuentaPeajes.Location = New System.Drawing.Point(142, 155)
		Me.txtCuentaPeajes.Maxlength = 6
		Me.txtCuentaPeajes.TabIndex = 8
		Me.txtCuentaPeajes.Tag = "5"
		Me.txtCuentaPeajes.AcceptsReturn = True
		Me.txtCuentaPeajes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCuentaPeajes.BackColor = System.Drawing.SystemColors.Window
		Me.txtCuentaPeajes.CausesValidation = True
		Me.txtCuentaPeajes.Enabled = True
		Me.txtCuentaPeajes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCuentaPeajes.HideSelection = True
		Me.txtCuentaPeajes.ReadOnly = False
		Me.txtCuentaPeajes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCuentaPeajes.MultiLine = False
		Me.txtCuentaPeajes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCuentaPeajes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCuentaPeajes.TabStop = True
		Me.txtCuentaPeajes.Visible = True
		Me.txtCuentaPeajes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCuentaPeajes.Name = "txtCuentaPeajes"
		Me.txtCuentaOtros.AutoSize = False
		Me.txtCuentaOtros.Size = New System.Drawing.Size(62, 19)
		Me.txtCuentaOtros.Location = New System.Drawing.Point(142, 227)
		Me.txtCuentaOtros.Maxlength = 6
		Me.txtCuentaOtros.TabIndex = 11
		Me.txtCuentaOtros.Tag = "6"
		Me.txtCuentaOtros.AcceptsReturn = True
		Me.txtCuentaOtros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCuentaOtros.BackColor = System.Drawing.SystemColors.Window
		Me.txtCuentaOtros.CausesValidation = True
		Me.txtCuentaOtros.Enabled = True
		Me.txtCuentaOtros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCuentaOtros.HideSelection = True
		Me.txtCuentaOtros.ReadOnly = False
		Me.txtCuentaOtros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCuentaOtros.MultiLine = False
		Me.txtCuentaOtros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCuentaOtros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCuentaOtros.TabStop = True
		Me.txtCuentaOtros.Visible = True
		Me.txtCuentaOtros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCuentaOtros.Name = "txtCuentaOtros"
		Me.txtCuentaCaja.AutoSize = False
		Me.txtCuentaCaja.Size = New System.Drawing.Size(62, 19)
		Me.txtCuentaCaja.Location = New System.Drawing.Point(142, 251)
		Me.txtCuentaCaja.Maxlength = 6
		Me.txtCuentaCaja.TabIndex = 12
		Me.txtCuentaCaja.Tag = "7"
		Me.txtCuentaCaja.AcceptsReturn = True
		Me.txtCuentaCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCuentaCaja.BackColor = System.Drawing.SystemColors.Window
		Me.txtCuentaCaja.CausesValidation = True
		Me.txtCuentaCaja.Enabled = True
		Me.txtCuentaCaja.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCuentaCaja.HideSelection = True
		Me.txtCuentaCaja.ReadOnly = False
		Me.txtCuentaCaja.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCuentaCaja.MultiLine = False
		Me.txtCuentaCaja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCuentaCaja.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCuentaCaja.TabStop = True
		Me.txtCuentaCaja.Visible = True
		Me.txtCuentaCaja.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCuentaCaja.Name = "txtCuentaCaja"
		Me.txtCategoriaOperacio.AutoSize = False
		Me.txtCategoriaOperacio.Size = New System.Drawing.Size(31, 19)
		Me.txtCategoriaOperacio.Location = New System.Drawing.Point(402, 201)
		Me.txtCategoriaOperacio.Maxlength = 3
		Me.txtCategoriaOperacio.TabIndex = 18
		Me.txtCategoriaOperacio.Tag = "8"
		Me.txtCategoriaOperacio.AcceptsReturn = True
		Me.txtCategoriaOperacio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCategoriaOperacio.BackColor = System.Drawing.SystemColors.Window
		Me.txtCategoriaOperacio.CausesValidation = True
		Me.txtCategoriaOperacio.Enabled = True
		Me.txtCategoriaOperacio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCategoriaOperacio.HideSelection = True
		Me.txtCategoriaOperacio.ReadOnly = False
		Me.txtCategoriaOperacio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCategoriaOperacio.MultiLine = False
		Me.txtCategoriaOperacio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCategoriaOperacio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCategoriaOperacio.TabStop = True
		Me.txtCategoriaOperacio.Visible = True
		Me.txtCategoriaOperacio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCategoriaOperacio.Name = "txtCategoriaOperacio"
		Me.txtSerieDocuments.AutoSize = False
		Me.txtSerieDocuments.Size = New System.Drawing.Size(31, 19)
		Me.txtSerieDocuments.Location = New System.Drawing.Point(402, 225)
		Me.txtSerieDocuments.Maxlength = 3
		Me.txtSerieDocuments.TabIndex = 19
		Me.txtSerieDocuments.Tag = "9"
		Me.txtSerieDocuments.AcceptsReturn = True
		Me.txtSerieDocuments.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSerieDocuments.BackColor = System.Drawing.SystemColors.Window
		Me.txtSerieDocuments.CausesValidation = True
		Me.txtSerieDocuments.Enabled = True
		Me.txtSerieDocuments.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSerieDocuments.HideSelection = True
		Me.txtSerieDocuments.ReadOnly = False
		Me.txtSerieDocuments.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSerieDocuments.MultiLine = False
		Me.txtSerieDocuments.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSerieDocuments.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSerieDocuments.TabStop = True
		Me.txtSerieDocuments.Visible = True
		Me.txtSerieDocuments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSerieDocuments.Name = "txtSerieDocuments"
		Me.txtDiarioContable.AutoSize = False
		Me.txtDiarioContable.Size = New System.Drawing.Size(62, 19)
		Me.txtDiarioContable.Location = New System.Drawing.Point(402, 249)
		Me.txtDiarioContable.Maxlength = 6
		Me.txtDiarioContable.TabIndex = 20
		Me.txtDiarioContable.Tag = "10"
		Me.txtDiarioContable.AcceptsReturn = True
		Me.txtDiarioContable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDiarioContable.BackColor = System.Drawing.SystemColors.Window
		Me.txtDiarioContable.CausesValidation = True
		Me.txtDiarioContable.Enabled = True
		Me.txtDiarioContable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDiarioContable.HideSelection = True
		Me.txtDiarioContable.ReadOnly = False
		Me.txtDiarioContable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDiarioContable.MultiLine = False
		Me.txtDiarioContable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDiarioContable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDiarioContable.TabStop = True
		Me.txtDiarioContable.Visible = True
		Me.txtDiarioContable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDiarioContable.Name = "txtDiarioContable"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(314, 288)
		Me.cmdAceptar.TabIndex = 26
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(404, 288)
		Me.cmdGuardar.TabIndex = 37
		Me.cmdGuardar.Name = "cmdGuardar"

		Me.txtImporteMediaDieta.Size = New System.Drawing.Size(62, 19)
		Me.txtImporteMediaDieta.Location = New System.Drawing.Point(142, 58)
		Me.txtImporteMediaDieta.TabIndex = 3
Me.txtImporteMediaDieta.Tag = "14"
		Me.txtImporteMediaDieta.Name = "txtImporteMediaDieta"
		Me.lbl25.Text = "Factor peajes,aparcamientos"
		Me.lbl25.Size = New System.Drawing.Size(161, 15)
		Me.lbl25.Location = New System.Drawing.Point(224, 182)
		Me.lbl25.TabIndex = 43
		Me.lbl25.Tag = "25"
		Me.lbl25.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl25.BackColor = System.Drawing.SystemColors.Control
		Me.lbl25.Enabled = True
		Me.lbl25.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl25.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl25.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl25.UseMnemonic = True
		Me.lbl25.Visible = True
		Me.lbl25.AutoSize = False
		Me.lbl25.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl25.Name = "lbl25"
		Me.lbl24.Text = "Factor desplazamientos IRPF"
		Me.lbl24.Size = New System.Drawing.Size(161, 15)
		Me.lbl24.Location = New System.Drawing.Point(224, 158)
		Me.lbl24.TabIndex = 42
		Me.lbl24.Tag = "24"
		Me.lbl24.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl24.BackColor = System.Drawing.SystemColors.Control
		Me.lbl24.Enabled = True
		Me.lbl24.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl24.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl24.UseMnemonic = True
		Me.lbl24.Visible = True
		Me.lbl24.AutoSize = False
		Me.lbl24.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl24.Name = "lbl24"
		Me.lbl23.Text = "Factor desplazamientos"
		Me.lbl23.Size = New System.Drawing.Size(161, 15)
		Me.lbl23.Location = New System.Drawing.Point(224, 134)
		Me.lbl23.TabIndex = 41
		Me.lbl23.Tag = "23"
		Me.lbl23.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl23.BackColor = System.Drawing.SystemColors.Control
		Me.lbl23.Enabled = True
		Me.lbl23.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl23.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl23.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl23.UseMnemonic = True
		Me.lbl23.Visible = True
		Me.lbl23.AutoSize = False
		Me.lbl23.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl23.Name = "lbl23"
		Me.lbl22.Text = "Factor dietas IRPF"
		Me.lbl22.Size = New System.Drawing.Size(161, 15)
		Me.lbl22.Location = New System.Drawing.Point(224, 110)
		Me.lbl22.TabIndex = 40
		Me.lbl22.Tag = "22"
		Me.lbl22.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl22.BackColor = System.Drawing.SystemColors.Control
		Me.lbl22.Enabled = True
		Me.lbl22.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl22.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl22.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl22.UseMnemonic = True
		Me.lbl22.Visible = True
		Me.lbl22.AutoSize = False
		Me.lbl22.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl22.Name = "lbl22"
		Me.lbl21.Text = "Factor dietas"
		Me.lbl21.Size = New System.Drawing.Size(161, 15)
		Me.lbl21.Location = New System.Drawing.Point(224, 86)
		Me.lbl21.TabIndex = 39
		Me.lbl21.Tag = "21"
		Me.lbl21.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl21.BackColor = System.Drawing.SystemColors.Control
		Me.lbl21.Enabled = True
		Me.lbl21.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl21.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl21.UseMnemonic = True
		Me.lbl21.Visible = True
		Me.lbl21.AutoSize = False
		Me.lbl21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl21.Name = "lbl21"
		Me.Label5.Text = "Importe dieta completa"
		Me.Label5.Size = New System.Drawing.Size(129, 15)
		Me.Label5.Location = New System.Drawing.Point(224, 62)
		Me.Label5.TabIndex = 21
		Me.Label5.Tag = "15"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Importe media dieta"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(10, 62)
		Me.Label4.TabIndex = 22
		Me.Label4.Tag = "14"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "Precio kilometro"
		Me.Label3.Size = New System.Drawing.Size(111, 15)
		Me.Label3.Location = New System.Drawing.Point(10, 38)
		Me.Label3.TabIndex = 23
		Me.Label3.Tag = "13"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Cuenta medios transporte"
		Me.Label2.Size = New System.Drawing.Size(121, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 206)
		Me.Label2.TabIndex = 24
		Me.Label2.Tag = "12"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Cuenta hoteles"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 182)
		Me.Label1.TabIndex = 25
		Me.Label1.Tag = "11"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Cuenta kilometraje"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 87)
		Me.lbl2.TabIndex = 28
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Cuenta dietas"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 111)
		Me.lbl3.TabIndex = 29
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Cuenta parkings"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 135)
		Me.lbl4.TabIndex = 30
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Cuenta peajes"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 159)
		Me.lbl5.TabIndex = 31
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Cuenta otros"
		Me.lbl6.Size = New System.Drawing.Size(123, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 231)
		Me.lbl6.TabIndex = 32
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Cuenta caja"
		Me.lbl7.Size = New System.Drawing.Size(123, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 255)
		Me.lbl7.TabIndex = 33
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Categoria operaci�n"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(224, 205)
		Me.lbl8.TabIndex = 34
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Serie documentos"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(224, 229)
		Me.lbl9.TabIndex = 35
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl10.Text = "Diario contable"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(224, 253)
		Me.lbl10.TabIndex = 36
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 288)
		Me.lblLock.TabIndex = 38
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 282
		Me.Line1.Y2 = 282
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 283
		Me.Line2.Y2 = 283
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.txtImporteMediaDieta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtPrecioKilometro, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtImporteDietaCompleta, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtFactorPeajes)
		Me.Controls.Add(txtFactorDesplazamientosI)
		Me.Controls.Add(txtFactorDesplazamientos)
		Me.Controls.Add(txtFactorDietasIRPF)
		Me.Controls.Add(txtFactorDietas)
		Me.Controls.Add(txtImporteDietaCompleta)
		Me.Controls.Add(txtPrecioKilometro)
		Me.Controls.Add(txtMedios)
		Me.Controls.Add(txtHoteles)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtCuentaKilometraje)
		Me.Controls.Add(txtCuentaDietas)
		Me.Controls.Add(txtCuentaParkings)
		Me.Controls.Add(txtCuentaPeajes)
		Me.Controls.Add(txtCuentaOtros)
		Me.Controls.Add(txtCuentaCaja)
		Me.Controls.Add(txtCategoriaOperacio)
		Me.Controls.Add(txtSerieDocuments)
		Me.Controls.Add(txtDiarioContable)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(txtImporteMediaDieta)
		Me.Controls.Add(lbl25)
		Me.Controls.Add(lbl24)
		Me.Controls.Add(lbl23)
		Me.Controls.Add(lbl22)
		Me.Controls.Add(lbl21)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl10)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
