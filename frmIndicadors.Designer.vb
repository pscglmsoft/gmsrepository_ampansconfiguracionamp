<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadors
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtPieceEI As System.Windows.Forms.TextBox
	Public WithEvents txtPieceContar As System.Windows.Forms.TextBox
	Public WithEvents txtObservacionesIndicador As System.Windows.Forms.TextBox
	Public WithEvents txtXecuteObtencioDescripcio As System.Windows.Forms.TextBox
	Public WithEvents txtNumeroDecimals As System.Windows.Forms.TextBox
	Public WithEvents txtGrupoIndicadorResp As System.Windows.Forms.TextBox
	Public WithEvents txtIndicadorPercentatge As System.Windows.Forms.TextBox
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents GroupBox1 As DataControl.GmsGroupBox
	Public WithEvents txtDescripcio As System.Windows.Forms.TextBox
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtGrupIndicador As System.Windows.Forms.TextBox
	Public WithEvents txtNumAgrupInd As System.Windows.Forms.TextBox
	Public WithEvents cmbTipusIndicador As DataControl.GmsCombo
	Public WithEvents chkDadesDuplicadesEmp As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmbOperadorcalcul As DataControl.GmsCombo
	Public WithEvents cmbObtencioTotal As DataControl.GmsCombo
	Public WithEvents cmbObjectiu As DataControl.GmsCombo
	Public WithEvents chkNoUsarInf As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkDesglocSegDada As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkTituloPrincipal As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents txtFechaBaja As DataControl.GmsData
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents frDadesGenerals As DataControl.GmsGroupBox
	Public WithEvents txtObservacionesParametro As System.Windows.Forms.TextBox
	Public WithEvents chkCambioRequiereAct As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents frIndParam As DataControl.GmsGroupBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lblLock As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadors))
		Me.components = New System.ComponentModel.Container()
		Me.frDadesGenerals = New DataControl.GmsGroupBox
		Me.txtPieceEI = New System.Windows.Forms.TextBox
		Me.txtPieceContar = New System.Windows.Forms.TextBox
		Me.txtObservacionesIndicador = New System.Windows.Forms.TextBox
		Me.txtXecuteObtencioDescripcio = New System.Windows.Forms.TextBox
		Me.txtNumeroDecimals = New System.Windows.Forms.TextBox
		Me.GroupBox1 = New DataControl.GmsGroupBox
		Me.txtGrupoIndicadorResp = New System.Windows.Forms.TextBox
		Me.txtIndicadorPercentatge = New System.Windows.Forms.TextBox
		Me.Label14 = New System.Windows.Forms.Label
		Me.Label13 = New System.Windows.Forms.Label
		Me.txtDescripcio = New System.Windows.Forms.TextBox
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtGrupIndicador = New System.Windows.Forms.TextBox
		Me.txtNumAgrupInd = New System.Windows.Forms.TextBox
		Me.cmbTipusIndicador = New DataControl.GmsCombo
		Me.chkDadesDuplicadesEmp = New AxXtremeSuiteControls.AxCheckBox
		Me.cmbOperadorcalcul = New DataControl.GmsCombo
		Me.cmbObtencioTotal = New DataControl.GmsCombo
		Me.cmbObjectiu = New DataControl.GmsCombo
		Me.chkNoUsarInf = New AxXtremeSuiteControls.AxCheckBox
		Me.chkDesglocSegDada = New AxXtremeSuiteControls.AxCheckBox
		Me.chkTituloPrincipal = New AxXtremeSuiteControls.AxCheckBox
		Me.txtFechaBaja = New DataControl.GmsData
		Me.lbl5 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label12 = New System.Windows.Forms.Label
		Me.frIndParam = New DataControl.GmsGroupBox
		Me.txtObservacionesParametro = New System.Windows.Forms.TextBox
		Me.chkCambioRequiereAct = New AxXtremeSuiteControls.AxCheckBox
		Me.Label15 = New System.Windows.Forms.Label
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lblLock = New System.Windows.Forms.Label
		Me.frDadesGenerals.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.frIndParam.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.chkDadesDuplicadesEmp, System.ComponentModel.ISupportInitialize).BeginInit()



		CType(Me.chkNoUsarInf, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkDesglocSegDada, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkTituloPrincipal, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.frDadesGenerals, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkCambioRequiereAct, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frIndParam, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Indicadors"
		Me.ClientSize = New System.Drawing.Size(758, 534)
		Me.Location = New System.Drawing.Point(322, 277)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORS WAP"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadors"

		Me.frDadesGenerals.Size = New System.Drawing.Size(737, 395)
		Me.frDadesGenerals.Location = New System.Drawing.Point(10, 4)
Me.frDadesGenerals.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameNone
		Me.frDadesGenerals.TabIndex = 19
Me.frDadesGenerals.Text = "GroupBox1"
		Me.frDadesGenerals.Name = "frDadesGenerals"
		Me.txtPieceEI.AutoSize = False
		Me.txtPieceEI.Size = New System.Drawing.Size(41, 19)
		Me.txtPieceEI.Location = New System.Drawing.Point(172, 190)
		Me.txtPieceEI.Maxlength = 3
		Me.txtPieceEI.TabIndex = 36
		Me.txtPieceEI.Tag = "13"
		Me.txtPieceEI.AcceptsReturn = True
		Me.txtPieceEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPieceEI.BackColor = System.Drawing.SystemColors.Window
		Me.txtPieceEI.CausesValidation = True
		Me.txtPieceEI.Enabled = True
		Me.txtPieceEI.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPieceEI.HideSelection = True
		Me.txtPieceEI.ReadOnly = False
		Me.txtPieceEI.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPieceEI.MultiLine = False
		Me.txtPieceEI.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPieceEI.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPieceEI.TabStop = True
		Me.txtPieceEI.Visible = True
		Me.txtPieceEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPieceEI.Name = "txtPieceEI"
		Me.txtPieceContar.AutoSize = False
		Me.txtPieceContar.Size = New System.Drawing.Size(41, 19)
		Me.txtPieceContar.Location = New System.Drawing.Point(172, 214)
		Me.txtPieceContar.Maxlength = 3
		Me.txtPieceContar.TabIndex = 35
		Me.txtPieceContar.Tag = "18"
		Me.txtPieceContar.AcceptsReturn = True
		Me.txtPieceContar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPieceContar.BackColor = System.Drawing.SystemColors.Window
		Me.txtPieceContar.CausesValidation = True
		Me.txtPieceContar.Enabled = True
		Me.txtPieceContar.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPieceContar.HideSelection = True
		Me.txtPieceContar.ReadOnly = False
		Me.txtPieceContar.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPieceContar.MultiLine = False
		Me.txtPieceContar.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPieceContar.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPieceContar.TabStop = True
		Me.txtPieceContar.Visible = True
		Me.txtPieceContar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPieceContar.Name = "txtPieceContar"
		Me.txtObservacionesIndicador.AutoSize = False
		Me.txtObservacionesIndicador.Size = New System.Drawing.Size(538, 59)
		Me.txtObservacionesIndicador.Location = New System.Drawing.Point(172, 238)
		Me.txtObservacionesIndicador.Maxlength = 999
		Me.txtObservacionesIndicador.MultiLine = True
		Me.txtObservacionesIndicador.TabIndex = 13
		Me.txtObservacionesIndicador.Tag = "17"
		Me.txtObservacionesIndicador.AcceptsReturn = True
		Me.txtObservacionesIndicador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesIndicador.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesIndicador.CausesValidation = True
		Me.txtObservacionesIndicador.Enabled = True
		Me.txtObservacionesIndicador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesIndicador.HideSelection = True
		Me.txtObservacionesIndicador.ReadOnly = False
		Me.txtObservacionesIndicador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesIndicador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesIndicador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesIndicador.TabStop = True
		Me.txtObservacionesIndicador.Visible = True
		Me.txtObservacionesIndicador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesIndicador.Name = "txtObservacionesIndicador"
		Me.txtXecuteObtencioDescripcio.AutoSize = False
		Me.txtXecuteObtencioDescripcio.Size = New System.Drawing.Size(554, 69)
		Me.txtXecuteObtencioDescripcio.Location = New System.Drawing.Point(172, 322)
		Me.txtXecuteObtencioDescripcio.Maxlength = 999
		Me.txtXecuteObtencioDescripcio.MultiLine = True
		Me.txtXecuteObtencioDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtXecuteObtencioDescripcio.TabIndex = 14
		Me.txtXecuteObtencioDescripcio.Tag = "52"
		Me.txtXecuteObtencioDescripcio.AcceptsReturn = True
		Me.txtXecuteObtencioDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtXecuteObtencioDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtXecuteObtencioDescripcio.CausesValidation = True
		Me.txtXecuteObtencioDescripcio.Enabled = True
		Me.txtXecuteObtencioDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtXecuteObtencioDescripcio.HideSelection = True
		Me.txtXecuteObtencioDescripcio.ReadOnly = False
		Me.txtXecuteObtencioDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtXecuteObtencioDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtXecuteObtencioDescripcio.TabStop = True
		Me.txtXecuteObtencioDescripcio.Visible = True
		Me.txtXecuteObtencioDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtXecuteObtencioDescripcio.Name = "txtXecuteObtencioDescripcio"
		Me.txtNumeroDecimals.AutoSize = False
		Me.txtNumeroDecimals.BackColor = System.Drawing.Color.FromARGB(64, 0, 0)
		Me.txtNumeroDecimals.Size = New System.Drawing.Size(41, 19)
		Me.txtNumeroDecimals.Location = New System.Drawing.Point(424, 164)
		Me.txtNumeroDecimals.Maxlength = 3
		Me.txtNumeroDecimals.TabIndex = 8
		Me.txtNumeroDecimals.Tag = "22"
		Me.txtNumeroDecimals.AcceptsReturn = True
		Me.txtNumeroDecimals.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumeroDecimals.CausesValidation = True
		Me.txtNumeroDecimals.Enabled = True
		Me.txtNumeroDecimals.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumeroDecimals.HideSelection = True
		Me.txtNumeroDecimals.ReadOnly = False
		Me.txtNumeroDecimals.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumeroDecimals.MultiLine = False
		Me.txtNumeroDecimals.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumeroDecimals.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumeroDecimals.TabStop = True
		Me.txtNumeroDecimals.Visible = True
		Me.txtNumeroDecimals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumeroDecimals.Name = "txtNumeroDecimals"

		Me.GroupBox1.Size = New System.Drawing.Size(241, 69)
		Me.GroupBox1.Location = New System.Drawing.Point(474, 118)
Me.GroupBox1.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder
		Me.GroupBox1.TabIndex = 29
Me.GroupBox1.Text = "Indicador respecto"
		Me.GroupBox1.Name = "GroupBox1"
		Me.txtGrupoIndicadorResp.AutoSize = False
		Me.txtGrupoIndicadorResp.Size = New System.Drawing.Size(41, 19)
		Me.txtGrupoIndicadorResp.Location = New System.Drawing.Point(168, 41)
		Me.txtGrupoIndicadorResp.Maxlength = 3
		Me.txtGrupoIndicadorResp.TabIndex = 10
		Me.txtGrupoIndicadorResp.Tag = "25"
		Me.txtGrupoIndicadorResp.AcceptsReturn = True
		Me.txtGrupoIndicadorResp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGrupoIndicadorResp.BackColor = System.Drawing.SystemColors.Window
		Me.txtGrupoIndicadorResp.CausesValidation = True
		Me.txtGrupoIndicadorResp.Enabled = True
		Me.txtGrupoIndicadorResp.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGrupoIndicadorResp.HideSelection = True
		Me.txtGrupoIndicadorResp.ReadOnly = False
		Me.txtGrupoIndicadorResp.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGrupoIndicadorResp.MultiLine = False
		Me.txtGrupoIndicadorResp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGrupoIndicadorResp.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGrupoIndicadorResp.TabStop = True
		Me.txtGrupoIndicadorResp.Visible = True
		Me.txtGrupoIndicadorResp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGrupoIndicadorResp.Name = "txtGrupoIndicadorResp"
		Me.txtIndicadorPercentatge.AutoSize = False
		Me.txtIndicadorPercentatge.Size = New System.Drawing.Size(41, 19)
		Me.txtIndicadorPercentatge.Location = New System.Drawing.Point(168, 18)
		Me.txtIndicadorPercentatge.Maxlength = 3
		Me.txtIndicadorPercentatge.TabIndex = 9
		Me.txtIndicadorPercentatge.Tag = "24"
		Me.txtIndicadorPercentatge.AcceptsReturn = True
		Me.txtIndicadorPercentatge.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtIndicadorPercentatge.BackColor = System.Drawing.SystemColors.Window
		Me.txtIndicadorPercentatge.CausesValidation = True
		Me.txtIndicadorPercentatge.Enabled = True
		Me.txtIndicadorPercentatge.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtIndicadorPercentatge.HideSelection = True
		Me.txtIndicadorPercentatge.ReadOnly = False
		Me.txtIndicadorPercentatge.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtIndicadorPercentatge.MultiLine = False
		Me.txtIndicadorPercentatge.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtIndicadorPercentatge.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtIndicadorPercentatge.TabStop = True
		Me.txtIndicadorPercentatge.Visible = True
		Me.txtIndicadorPercentatge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtIndicadorPercentatge.Name = "txtIndicadorPercentatge"
		Me.Label14.Text = "Grupo indicador"
		Me.Label14.Size = New System.Drawing.Size(119, 15)
		Me.Label14.Location = New System.Drawing.Point(36, 43)
		Me.Label14.TabIndex = 31
		Me.Label14.Tag = "25"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label14.BackColor = System.Drawing.SystemColors.Control
		Me.Label14.Enabled = True
		Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.UseMnemonic = True
		Me.Label14.Visible = True
		Me.Label14.AutoSize = False
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Name = "Label14"
		Me.Label13.Text = "Indicador"
		Me.Label13.Size = New System.Drawing.Size(119, 15)
		Me.Label13.Location = New System.Drawing.Point(36, 20)
		Me.Label13.TabIndex = 30
		Me.Label13.Tag = "24"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label13.BackColor = System.Drawing.SystemColors.Control
		Me.Label13.Enabled = True
		Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.UseMnemonic = True
		Me.Label13.Visible = True
		Me.Label13.AutoSize = False
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Name = "Label13"
		Me.txtDescripcio.AutoSize = False
		Me.txtDescripcio.Size = New System.Drawing.Size(542, 49)
		Me.txtDescripcio.Location = New System.Drawing.Point(172, 38)
		Me.txtDescripcio.Maxlength = 999
		Me.txtDescripcio.MultiLine = True
		Me.txtDescripcio.TabIndex = 2
		Me.txtDescripcio.Tag = "3"
		Me.txtDescripcio.AcceptsReturn = True
		Me.txtDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcio.CausesValidation = True
		Me.txtDescripcio.Enabled = True
		Me.txtDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcio.HideSelection = True
		Me.txtDescripcio.ReadOnly = False
		Me.txtDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcio.TabStop = True
		Me.txtDescripcio.Visible = True
		Me.txtDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcio.Name = "txtDescripcio"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(49, 19)
		Me.txtCodi.Location = New System.Drawing.Point(602, 8)
		Me.txtCodi.Maxlength = 3
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*2"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(310, 19)
		Me.Text1.Location = New System.Drawing.Point(216, 8)
		Me.Text1.TabIndex = 20
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtGrupIndicador.AutoSize = False
		Me.txtGrupIndicador.Size = New System.Drawing.Size(41, 19)
		Me.txtGrupIndicador.Location = New System.Drawing.Point(172, 8)
		Me.txtGrupIndicador.Maxlength = 3
		Me.txtGrupIndicador.TabIndex = 0
		Me.txtGrupIndicador.Tag = "*1"
		Me.txtGrupIndicador.AcceptsReturn = True
		Me.txtGrupIndicador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGrupIndicador.BackColor = System.Drawing.SystemColors.Window
		Me.txtGrupIndicador.CausesValidation = True
		Me.txtGrupIndicador.Enabled = True
		Me.txtGrupIndicador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGrupIndicador.HideSelection = True
		Me.txtGrupIndicador.ReadOnly = False
		Me.txtGrupIndicador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGrupIndicador.MultiLine = False
		Me.txtGrupIndicador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGrupIndicador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGrupIndicador.TabStop = True
		Me.txtGrupIndicador.Visible = True
		Me.txtGrupIndicador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGrupIndicador.Name = "txtGrupIndicador"
		Me.txtNumAgrupInd.AutoSize = False
		Me.txtNumAgrupInd.Size = New System.Drawing.Size(41, 19)
		Me.txtNumAgrupInd.Location = New System.Drawing.Point(172, 165)
		Me.txtNumAgrupInd.Maxlength = 3
		Me.txtNumAgrupInd.TabIndex = 7
		Me.txtNumAgrupInd.Tag = "11"
		Me.txtNumAgrupInd.AcceptsReturn = True
		Me.txtNumAgrupInd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumAgrupInd.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumAgrupInd.CausesValidation = True
		Me.txtNumAgrupInd.Enabled = True
		Me.txtNumAgrupInd.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumAgrupInd.HideSelection = True
		Me.txtNumAgrupInd.ReadOnly = False
		Me.txtNumAgrupInd.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumAgrupInd.MultiLine = False
		Me.txtNumAgrupInd.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumAgrupInd.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumAgrupInd.TabStop = True
		Me.txtNumAgrupInd.Visible = True
		Me.txtNumAgrupInd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumAgrupInd.Name = "txtNumAgrupInd"

		Me.cmbTipusIndicador.Size = New System.Drawing.Size(211, 19)
		Me.cmbTipusIndicador.Location = New System.Drawing.Point(172, 94)
		Me.cmbTipusIndicador.TabIndex = 3
Me.cmbTipusIndicador.Tag = "7"
		Me.cmbTipusIndicador.Name = "cmbTipusIndicador"
		chkDadesDuplicadesEmp.OcxState = CType(resources.GetObject("chkDadesDuplicadesEmp.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkDadesDuplicadesEmp.Size = New System.Drawing.Size(219, 17)
		Me.chkDadesDuplicadesEmp.Location = New System.Drawing.Point(238, 190)
		Me.chkDadesDuplicadesEmp.TabIndex = 11
Me.chkDadesDuplicadesEmp.Tag = "20"
		Me.chkDadesDuplicadesEmp.Name = "chkDadesDuplicadesEmp"

		Me.cmbOperadorcalcul.Size = New System.Drawing.Size(211, 19)
		Me.cmbOperadorcalcul.Location = New System.Drawing.Point(172, 117)
		Me.cmbOperadorcalcul.TabIndex = 5
Me.cmbOperadorcalcul.Tag = "12"
		Me.cmbOperadorcalcul.Name = "cmbOperadorcalcul"

		Me.cmbObtencioTotal.Size = New System.Drawing.Size(211, 19)
		Me.cmbObtencioTotal.Location = New System.Drawing.Point(172, 141)
		Me.cmbObtencioTotal.TabIndex = 6
Me.cmbObtencioTotal.Tag = "14"
		Me.cmbObtencioTotal.Name = "cmbObtencioTotal"

		Me.cmbObjectiu.Size = New System.Drawing.Size(155, 19)
		Me.cmbObjectiu.Location = New System.Drawing.Point(560, 94)
		Me.cmbObjectiu.TabIndex = 4
Me.cmbObjectiu.Tag = "10"
		Me.cmbObjectiu.Name = "cmbObjectiu"
		chkNoUsarInf.OcxState = CType(resources.GetObject("chkNoUsarInf.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkNoUsarInf.Size = New System.Drawing.Size(219, 17)
		Me.chkNoUsarInf.Location = New System.Drawing.Point(492, 190)
		Me.chkNoUsarInf.TabIndex = 12
Me.chkNoUsarInf.Tag = "27"
		Me.chkNoUsarInf.Name = "chkNoUsarInf"
		chkDesglocSegDada.OcxState = CType(resources.GetObject("chkDesglocSegDada.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkDesglocSegDada.Size = New System.Drawing.Size(175, 19)
		Me.chkDesglocSegDada.Location = New System.Drawing.Point(10, 300)
		Me.chkDesglocSegDada.TabIndex = 37
Me.chkDesglocSegDada.Tag = "19"
		Me.chkDesglocSegDada.Name = "chkDesglocSegDada"
		chkTituloPrincipal.OcxState = CType(resources.GetObject("chkTituloPrincipal.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTituloPrincipal.Size = New System.Drawing.Size(225, 19)
		Me.chkTituloPrincipal.Location = New System.Drawing.Point(234, 300)
		Me.chkTituloPrincipal.TabIndex = 38
Me.chkTituloPrincipal.Tag = "26"
		Me.chkTituloPrincipal.Name = "chkTituloPrincipal"

		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(624, 214)
		Me.txtFechaBaja.TabIndex = 44
Me.txtFechaBaja.Tag = "6"
		Me.txtFechaBaja.Name = "txtFechaBaja"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lbl5.Text = "Fecha de baja"
		Me.lbl5.Size = New System.Drawing.Size(95, 15)
		Me.lbl5.Location = New System.Drawing.Point(520, 218)
		Me.lbl5.TabIndex = 45
		Me.lbl5.Tag = "6"
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.Label3.Text = "Piece dins estructura indicador"
		Me.Label3.Size = New System.Drawing.Size(165, 15)
		Me.Label3.Location = New System.Drawing.Point(10, 194)
		Me.Label3.TabIndex = 40
		Me.Label3.Tag = "13"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label7.Text = "Piece a contar"
		Me.Label7.Size = New System.Drawing.Size(167, 15)
		Me.Label7.Location = New System.Drawing.Point(10, 218)
		Me.Label7.TabIndex = 39
		Me.Label7.Tag = "18"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label6.Text = "Observaciones"
		Me.Label6.Size = New System.Drawing.Size(121, 15)
		Me.Label6.Location = New System.Drawing.Point(10, 240)
		Me.Label6.TabIndex = 34
		Me.Label6.Tag = "17"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.Label5.Text = "Xecute obtenci� descripci� de la dada"
		Me.Label5.Size = New System.Drawing.Size(155, 29)
		Me.Label5.Location = New System.Drawing.Point(10, 322)
		Me.Label5.TabIndex = 33
		Me.Label5.Tag = "52"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label10.Text = "N�mero decimals"
		Me.Label10.Size = New System.Drawing.Size(117, 15)
		Me.Label10.Location = New System.Drawing.Point(342, 166)
		Me.Label10.TabIndex = 32
		Me.Label10.Tag = "22"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.lbl7.Text = "Tipus d'indicador"
		Me.lbl7.Size = New System.Drawing.Size(121, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 96)
		Me.lbl7.TabIndex = 28
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl3.Text = "Descripci�"
		Me.lbl3.Size = New System.Drawing.Size(121, 29)
		Me.lbl3.Location = New System.Drawing.Point(10, 42)
		Me.lbl3.TabIndex = 27
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lbl2.Text = "Codi"
		Me.lbl2.Size = New System.Drawing.Size(49, 15)
		Me.lbl2.Location = New System.Drawing.Point(546, 12)
		Me.lbl2.TabIndex = 26
		Me.lbl2.Tag = "2"
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl1.Text = "Grup d'indicador"
		Me.lbl1.Size = New System.Drawing.Size(121, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 12)
		Me.lbl1.TabIndex = 25
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.Label1.Text = "N� Agrupaci� indicadors"
		Me.Label1.Size = New System.Drawing.Size(119, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 169)
		Me.Label1.TabIndex = 24
		Me.Label1.Tag = "11"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Label4.Text = "Obtenci� total anual"
		Me.Label4.Size = New System.Drawing.Size(121, 15)
		Me.Label4.Location = New System.Drawing.Point(10, 143)
		Me.Label4.TabIndex = 23
		Me.Label4.Tag = "14"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label2.Text = "Operador c�lcul"
		Me.Label2.Size = New System.Drawing.Size(121, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 121)
		Me.Label2.TabIndex = 22
		Me.Label2.Tag = "12"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label12.Text = "Objectiu"
		Me.Label12.Size = New System.Drawing.Size(103, 15)
		Me.Label12.Location = New System.Drawing.Point(444, 96)
		Me.Label12.TabIndex = 21
		Me.Label12.Tag = "10"
		Me.Label12.BackColor = System.Drawing.SystemColors.Control
		Me.Label12.Enabled = True
		Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.UseMnemonic = True
		Me.Label12.Visible = True
		Me.Label12.AutoSize = False
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Name = "Label12"

		Me.frIndParam.Size = New System.Drawing.Size(737, 89)
		Me.frIndParam.Location = New System.Drawing.Point(10, 404)
Me.frIndParam.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder
		Me.frIndParam.TabIndex = 18
Me.frIndParam.Text = "Indicador parametrizable"
		Me.frIndParam.Name = "frIndParam"
		Me.txtObservacionesParametro.AutoSize = False
		Me.txtObservacionesParametro.Size = New System.Drawing.Size(554, 51)
		Me.txtObservacionesParametro.Location = New System.Drawing.Point(172, 14)
		Me.txtObservacionesParametro.Maxlength = 999
		Me.txtObservacionesParametro.MultiLine = True
		Me.txtObservacionesParametro.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservacionesParametro.TabIndex = 41
		Me.txtObservacionesParametro.Tag = "29"
		Me.txtObservacionesParametro.AcceptsReturn = True
		Me.txtObservacionesParametro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesParametro.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesParametro.CausesValidation = True
		Me.txtObservacionesParametro.Enabled = True
		Me.txtObservacionesParametro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesParametro.HideSelection = True
		Me.txtObservacionesParametro.ReadOnly = False
		Me.txtObservacionesParametro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesParametro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesParametro.TabStop = True
		Me.txtObservacionesParametro.Visible = True
		Me.txtObservacionesParametro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesParametro.Name = "txtObservacionesParametro"
		chkCambioRequiereAct.OcxState = CType(resources.GetObject("chkCambioRequiereAct.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkCambioRequiereAct.Size = New System.Drawing.Size(175, 23)
		Me.chkCambioRequiereAct.Location = New System.Drawing.Point(10, 66)
		Me.chkCambioRequiereAct.TabIndex = 43
Me.chkCambioRequiereAct.Tag = "30"
		Me.chkCambioRequiereAct.Name = "chkCambioRequiereAct"
		Me.Label15.Text = "Observaciones par�metro"
		Me.Label15.Size = New System.Drawing.Size(121, 55)
		Me.Label15.Location = New System.Drawing.Point(10, 16)
		Me.Label15.TabIndex = 42
		Me.Label15.Tag = "29"
		Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label15.BackColor = System.Drawing.SystemColors.Control
		Me.Label15.Enabled = True
		Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.UseMnemonic = True
		Me.Label15.Visible = True
		Me.Label15.AutoSize = False
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Name = "Label15"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(577, 503)
		Me.cmdAceptar.TabIndex = 15
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(667, 503)
		Me.cmdGuardar.TabIndex = 16
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(10, 502)
		Me.lblLock.TabIndex = 17
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frIndParam, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkCambioRequiereAct, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frDadesGenerals, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.chkTituloPrincipal, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkDesglocSegDada, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkNoUsarInf, System.ComponentModel.ISupportInitialize).EndInit()



		CType(Me.chkDadesDuplicadesEmp, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(frDadesGenerals)
		Me.Controls.Add(frIndParam)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lblLock)
		Me.frDadesGenerals.Controls.Add(txtPieceEI)
		Me.frDadesGenerals.Controls.Add(txtPieceContar)
		Me.frDadesGenerals.Controls.Add(txtObservacionesIndicador)
		Me.frDadesGenerals.Controls.Add(txtXecuteObtencioDescripcio)
		Me.frDadesGenerals.Controls.Add(txtNumeroDecimals)
		Me.frDadesGenerals.Controls.Add(GroupBox1)
		Me.frDadesGenerals.Controls.Add(txtDescripcio)
		Me.frDadesGenerals.Controls.Add(txtCodi)
		Me.frDadesGenerals.Controls.Add(Text1)
		Me.frDadesGenerals.Controls.Add(txtGrupIndicador)
		Me.frDadesGenerals.Controls.Add(txtNumAgrupInd)
		Me.frDadesGenerals.Controls.Add(cmbTipusIndicador)
		Me.frDadesGenerals.Controls.Add(chkDadesDuplicadesEmp)
		Me.frDadesGenerals.Controls.Add(cmbOperadorcalcul)
		Me.frDadesGenerals.Controls.Add(cmbObtencioTotal)
		Me.frDadesGenerals.Controls.Add(cmbObjectiu)
		Me.frDadesGenerals.Controls.Add(chkNoUsarInf)
		Me.frDadesGenerals.Controls.Add(chkDesglocSegDada)
		Me.frDadesGenerals.Controls.Add(chkTituloPrincipal)
		Me.frDadesGenerals.Controls.Add(txtFechaBaja)
		Me.frDadesGenerals.Controls.Add(lbl5)
		Me.frDadesGenerals.Controls.Add(Label3)
		Me.frDadesGenerals.Controls.Add(Label7)
		Me.frDadesGenerals.Controls.Add(Label6)
		Me.frDadesGenerals.Controls.Add(Label5)
		Me.frDadesGenerals.Controls.Add(Label10)
		Me.frDadesGenerals.Controls.Add(lbl7)
		Me.frDadesGenerals.Controls.Add(lbl3)
		Me.frDadesGenerals.Controls.Add(lbl2)
		Me.frDadesGenerals.Controls.Add(lbl1)
		Me.frDadesGenerals.Controls.Add(Label1)
		Me.frDadesGenerals.Controls.Add(Label4)
		Me.frDadesGenerals.Controls.Add(Label2)
		Me.frDadesGenerals.Controls.Add(Label12)
		Me.GroupBox1.Controls.Add(txtGrupoIndicadorResp)
		Me.GroupBox1.Controls.Add(txtIndicadorPercentatge)
		Me.GroupBox1.Controls.Add(Label14)
		Me.GroupBox1.Controls.Add(Label13)
		Me.frIndParam.Controls.Add(txtObservacionesParametro)
		Me.frIndParam.Controls.Add(chkCambioRequiereAct)
		Me.frIndParam.Controls.Add(Label15)
		Me.frDadesGenerals.ResumeLayout(False)
		Me.GroupBox1.ResumeLayout(False)
		Me.frIndParam.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
