<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSistemasGestion
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtTextoLopd As System.Windows.Forms.TextBox
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents TabControlPage5 As GmsTabControl.GmsTabPage
	Public WithEvents chkNoCerrarContabilidad As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents txtSisGesFina As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresaFinanzas As System.Windows.Forms.TextBox
	Public WithEvents txtNamesFin As System.Windows.Forms.TextBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents TabControlPage4 As GmsTabControl.GmsTabPage
	Public WithEvents grdImpresosGraf As FlexCell.Grid
	Public WithEvents TabControlPage3 As GmsTabControl.GmsTabPage
	Public WithEvents txtNamespaceFinanzas As System.Windows.Forms.TextBox
	Public WithEvents cmbLogoPredeterminado As DataControl.GmsCombo
	Public WithEvents lbl19 As System.Windows.Forms.Label
	Public WithEvents lbl16 As System.Windows.Forms.Label
	Public WithEvents TabControlPage2 As GmsTabControl.GmsTabPage
	Public WithEvents txtUrl As System.Windows.Forms.TextBox
	Public WithEvents txtEmail As System.Windows.Forms.TextBox
	Public WithEvents txtSubDireccion As System.Windows.Forms.TextBox
	Public WithEvents txtFax As System.Windows.Forms.TextBox
	Public WithEvents txtNombreFiscal As System.Windows.Forms.TextBox
	Public WithEvents txtTelefono As System.Windows.Forms.TextBox
	Public WithEvents txtNif As System.Windows.Forms.TextBox
	Public WithEvents txtProvincia As System.Windows.Forms.TextBox
	Public WithEvents txtPoblacion As System.Windows.Forms.TextBox
	Public WithEvents txtDireccion As System.Windows.Forms.TextBox
	Public WithEvents txtNombreEmpresa As System.Windows.Forms.TextBox
	Public WithEvents txtDatosRegistro As System.Windows.Forms.TextBox
	Public WithEvents lbl14 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl12 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents TabControlPage1 As GmsTabControl.GmsTabPage
	Public WithEvents TabControl1 As DataControl.GmsTabControl
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtSistema As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSistemasGestion))
		Me.components = New System.ComponentModel.Container()
		Me.TabControl1 = New DataControl.GmsTabControl
		Me.TabControlPage5 = New GmsTabControl.GmsTabPage
		Me.txtTextoLopd = New System.Windows.Forms.TextBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.TabControlPage4 = New GmsTabControl.GmsTabPage
		Me.chkNoCerrarContabilidad = New AxXtremeSuiteControls.AxCheckBox
		Me.txtSisGesFina = New System.Windows.Forms.TextBox
		Me.txtEmpresaFinanzas = New System.Windows.Forms.TextBox
		Me.txtNamesFin = New System.Windows.Forms.TextBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.TabControlPage3 = New GmsTabControl.GmsTabPage
		Me.grdImpresosGraf = New FlexCell.Grid
		Me.TabControlPage2 = New GmsTabControl.GmsTabPage
		Me.txtNamespaceFinanzas = New System.Windows.Forms.TextBox
		Me.cmbLogoPredeterminado = New DataControl.GmsCombo
		Me.lbl19 = New System.Windows.Forms.Label
		Me.lbl16 = New System.Windows.Forms.Label
		Me.TabControlPage1 = New GmsTabControl.GmsTabPage
		Me.txtUrl = New System.Windows.Forms.TextBox
		Me.txtEmail = New System.Windows.Forms.TextBox
		Me.txtSubDireccion = New System.Windows.Forms.TextBox
		Me.txtFax = New System.Windows.Forms.TextBox
		Me.txtNombreFiscal = New System.Windows.Forms.TextBox
		Me.txtTelefono = New System.Windows.Forms.TextBox
		Me.txtNif = New System.Windows.Forms.TextBox
		Me.txtProvincia = New System.Windows.Forms.TextBox
		Me.txtPoblacion = New System.Windows.Forms.TextBox
		Me.txtDireccion = New System.Windows.Forms.TextBox
		Me.txtNombreEmpresa = New System.Windows.Forms.TextBox
		Me.txtDatosRegistro = New System.Windows.Forms.TextBox
		Me.lbl14 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl12 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtSistema = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.lblLock = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage5.SuspendLayout()
		Me.TabControlPage4.SuspendLayout()
		Me.TabControlPage3.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkNoCerrarContabilidad, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Sistemas de gesti�n"
		Me.ClientSize = New System.Drawing.Size(651, 475)
		Me.Location = New System.Drawing.Point(368, 291)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-SISTEMAS ENT"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSistemasGestion"

		Me.TabControl1.Size = New System.Drawing.Size(631, 377)
		Me.TabControl1.Location = New System.Drawing.Point(10, 60)
		Me.TabControl1.TabIndex = 25
		Me.TabControl1.Name = "TabControl1"

		Me.TabControlPage5.Size = New System.Drawing.Size(627, 353)
		Me.TabControlPage5.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage5.TabIndex = 30
Me.TabControlPage5.Text = "LOPD"
		Me.TabControlPage5.Visible = False
		Me.TabControlPage5.Name = "TabControlPage5"
		Me.txtTextoLopd.AutoSize = False
		Me.txtTextoLopd.Size = New System.Drawing.Size(590, 173)
		Me.txtTextoLopd.Location = New System.Drawing.Point(16, 32)
		Me.txtTextoLopd.Maxlength = 35
		Me.txtTextoLopd.MultiLine = True
		Me.txtTextoLopd.TabIndex = 20
		Me.txtTextoLopd.Tag = "22###########5"
		Me.txtTextoLopd.AcceptsReturn = True
		Me.txtTextoLopd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTextoLopd.BackColor = System.Drawing.SystemColors.Window
		Me.txtTextoLopd.CausesValidation = True
		Me.txtTextoLopd.Enabled = True
		Me.txtTextoLopd.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTextoLopd.HideSelection = True
		Me.txtTextoLopd.ReadOnly = False
		Me.txtTextoLopd.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTextoLopd.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTextoLopd.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTextoLopd.TabStop = True
		Me.txtTextoLopd.Visible = True
		Me.txtTextoLopd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTextoLopd.Name = "txtTextoLopd"
		Me.Label4.Text = "Texto Ley de Protecci�n de Datos en documentos"
		Me.Label4.Size = New System.Drawing.Size(267, 15)
		Me.Label4.Location = New System.Drawing.Point(18, 12)
		Me.Label4.TabIndex = 43
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"

		Me.TabControlPage4.Size = New System.Drawing.Size(627, 353)
		Me.TabControlPage4.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage4.TabIndex = 29
Me.TabControlPage4.Text = "Finanzas"
		Me.TabControlPage4.Visible = False
		Me.TabControlPage4.Name = "TabControlPage4"
		chkNoCerrarContabilidad.OcxState = CType(resources.GetObject("chkNoCerrarContabilidad.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkNoCerrarContabilidad.Size = New System.Drawing.Size(191, 15)
		Me.chkNoCerrarContabilidad.Location = New System.Drawing.Point(32, 164)
		Me.chkNoCerrarContabilidad.TabIndex = 19
Me.chkNoCerrarContabilidad.Tag = "20###########4"
		Me.chkNoCerrarContabilidad.Name = "chkNoCerrarContabilidad"
		Me.txtSisGesFina.AutoSize = False
		Me.txtSisGesFina.Size = New System.Drawing.Size(30, 19)
		Me.txtSisGesFina.Location = New System.Drawing.Point(210, 76)
		Me.txtSisGesFina.Maxlength = 12
		Me.txtSisGesFina.TabIndex = 18
		Me.txtSisGesFina.Tag = "18###########4"
		Me.txtSisGesFina.AcceptsReturn = True
		Me.txtSisGesFina.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSisGesFina.BackColor = System.Drawing.SystemColors.Window
		Me.txtSisGesFina.CausesValidation = True
		Me.txtSisGesFina.Enabled = True
		Me.txtSisGesFina.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSisGesFina.HideSelection = True
		Me.txtSisGesFina.ReadOnly = False
		Me.txtSisGesFina.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSisGesFina.MultiLine = False
		Me.txtSisGesFina.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSisGesFina.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSisGesFina.TabStop = True
		Me.txtSisGesFina.Visible = True
		Me.txtSisGesFina.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSisGesFina.Name = "txtSisGesFina"
		Me.txtEmpresaFinanzas.AutoSize = False
		Me.txtEmpresaFinanzas.Size = New System.Drawing.Size(98, 19)
		Me.txtEmpresaFinanzas.Location = New System.Drawing.Point(210, 52)
		Me.txtEmpresaFinanzas.Maxlength = 12
		Me.txtEmpresaFinanzas.TabIndex = 17
		Me.txtEmpresaFinanzas.Tag = "17###########4"
		Me.txtEmpresaFinanzas.AcceptsReturn = True
		Me.txtEmpresaFinanzas.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresaFinanzas.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresaFinanzas.CausesValidation = True
		Me.txtEmpresaFinanzas.Enabled = True
		Me.txtEmpresaFinanzas.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresaFinanzas.HideSelection = True
		Me.txtEmpresaFinanzas.ReadOnly = False
		Me.txtEmpresaFinanzas.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresaFinanzas.MultiLine = False
		Me.txtEmpresaFinanzas.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresaFinanzas.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresaFinanzas.TabStop = True
		Me.txtEmpresaFinanzas.Visible = True
		Me.txtEmpresaFinanzas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresaFinanzas.Name = "txtEmpresaFinanzas"
		Me.txtNamesFin.AutoSize = False
		Me.txtNamesFin.Size = New System.Drawing.Size(98, 19)
		Me.txtNamesFin.Location = New System.Drawing.Point(210, 28)
		Me.txtNamesFin.Maxlength = 12
		Me.txtNamesFin.TabIndex = 16
		Me.txtNamesFin.Tag = "16###########4"
		Me.txtNamesFin.AcceptsReturn = True
		Me.txtNamesFin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNamesFin.BackColor = System.Drawing.SystemColors.Window
		Me.txtNamesFin.CausesValidation = True
		Me.txtNamesFin.Enabled = True
		Me.txtNamesFin.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNamesFin.HideSelection = True
		Me.txtNamesFin.ReadOnly = False
		Me.txtNamesFin.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNamesFin.MultiLine = False
		Me.txtNamesFin.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNamesFin.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNamesFin.TabStop = True
		Me.txtNamesFin.Visible = True
		Me.txtNamesFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNamesFin.Name = "txtNamesFin"
		Me.Label2.Text = "Sistema gesti�n"
		Me.Label2.Size = New System.Drawing.Size(163, 15)
		Me.Label2.Location = New System.Drawing.Point(32, 78)
		Me.Label2.TabIndex = 42
		Me.Label2.Tag = "18"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Empresa finanzas predeterminada"
		Me.Label1.Size = New System.Drawing.Size(163, 15)
		Me.Label1.Location = New System.Drawing.Point(32, 54)
		Me.Label1.TabIndex = 41
		Me.Label1.Tag = "17"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Label6.Text = "Namespace datos finanzas"
		Me.Label6.Size = New System.Drawing.Size(163, 15)
		Me.Label6.Location = New System.Drawing.Point(32, 30)
		Me.Label6.TabIndex = 40
		Me.Label6.Tag = "16"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"

		Me.TabControlPage3.Size = New System.Drawing.Size(627, 353)
		Me.TabControlPage3.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage3.TabIndex = 28
Me.TabControlPage3.Text = "Impresos gr�ficos"
		Me.TabControlPage3.Visible = False
		Me.TabControlPage3.Name = "TabControlPage3"

		Me.grdImpresosGraf.Size = New System.Drawing.Size(587, 309)
		Me.grdImpresosGraf.Location = New System.Drawing.Point(22, 22)
Me.grdImpresosGraf.BorderStyle = FlexCell.BorderStyleEnum.FixedSingle
		Me.grdImpresosGraf.TabIndex = 15
		Me.grdImpresosGraf.Name = "grdImpresosGraf"

		Me.TabControlPage2.Size = New System.Drawing.Size(627, 353)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 27
Me.TabControlPage2.Text = "Par�metros"
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"
		Me.txtNamespaceFinanzas.AutoSize = False
		Me.txtNamespaceFinanzas.Size = New System.Drawing.Size(205, 19)
		Me.txtNamespaceFinanzas.Location = New System.Drawing.Point(159, 34)
		Me.txtNamespaceFinanzas.Maxlength = 15
		Me.txtNamespaceFinanzas.TabIndex = 14
		Me.txtNamespaceFinanzas.Tag = "15###########2"
		Me.txtNamespaceFinanzas.AcceptsReturn = True
		Me.txtNamespaceFinanzas.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNamespaceFinanzas.BackColor = System.Drawing.SystemColors.Window
		Me.txtNamespaceFinanzas.CausesValidation = True
		Me.txtNamespaceFinanzas.Enabled = True
		Me.txtNamespaceFinanzas.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNamespaceFinanzas.HideSelection = True
		Me.txtNamespaceFinanzas.ReadOnly = False
		Me.txtNamespaceFinanzas.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNamespaceFinanzas.MultiLine = False
		Me.txtNamespaceFinanzas.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNamespaceFinanzas.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNamespaceFinanzas.TabStop = True
		Me.txtNamespaceFinanzas.Visible = True
		Me.txtNamespaceFinanzas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNamespaceFinanzas.Name = "txtNamespaceFinanzas"

		Me.cmbLogoPredeterminado.Size = New System.Drawing.Size(205, 19)
		Me.cmbLogoPredeterminado.Location = New System.Drawing.Point(159, 59)
		Me.cmbLogoPredeterminado.TabIndex = 50
Me.cmbLogoPredeterminado.Tag = "19###########2"
		Me.cmbLogoPredeterminado.Name = "cmbLogoPredeterminado"
		Me.lbl19.Text = "Logo predeterminado"
		Me.lbl19.Size = New System.Drawing.Size(119, 15)
		Me.lbl19.Location = New System.Drawing.Point(36, 63)
		Me.lbl19.TabIndex = 49
		Me.lbl19.Tag = "19"
		Me.lbl19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl19.BackColor = System.Drawing.SystemColors.Control
		Me.lbl19.Enabled = True
		Me.lbl19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl19.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl19.UseMnemonic = True
		Me.lbl19.Visible = True
		Me.lbl19.AutoSize = False
		Me.lbl19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl19.Name = "lbl19"
		Me.lbl16.Text = "Namespace alternativo"
		Me.lbl16.Size = New System.Drawing.Size(119, 15)
		Me.lbl16.Location = New System.Drawing.Point(36, 38)
		Me.lbl16.TabIndex = 39
		Me.lbl16.Tag = "15"
		Me.lbl16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl16.BackColor = System.Drawing.SystemColors.Control
		Me.lbl16.Enabled = True
		Me.lbl16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl16.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl16.UseMnemonic = True
		Me.lbl16.Visible = True
		Me.lbl16.AutoSize = False
		Me.lbl16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl16.Name = "lbl16"

		Me.TabControlPage1.Size = New System.Drawing.Size(627, 353)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 26
Me.TabControlPage1.Text = "Datos generales"
		Me.TabControlPage1.Name = "TabControlPage1"
		Me.txtUrl.AutoSize = False
		Me.txtUrl.Size = New System.Drawing.Size(259, 19)
		Me.txtUrl.Location = New System.Drawing.Point(166, 268)
		Me.txtUrl.Maxlength = 25
		Me.txtUrl.TabIndex = 12
		Me.txtUrl.Tag = "14"
		Me.txtUrl.AcceptsReturn = True
		Me.txtUrl.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUrl.BackColor = System.Drawing.SystemColors.Window
		Me.txtUrl.CausesValidation = True
		Me.txtUrl.Enabled = True
		Me.txtUrl.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUrl.HideSelection = True
		Me.txtUrl.ReadOnly = False
		Me.txtUrl.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUrl.MultiLine = False
		Me.txtUrl.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUrl.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUrl.TabStop = True
		Me.txtUrl.Visible = True
		Me.txtUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUrl.Name = "txtUrl"
		Me.txtEmail.AutoSize = False
		Me.txtEmail.Size = New System.Drawing.Size(259, 19)
		Me.txtEmail.Location = New System.Drawing.Point(166, 243)
		Me.txtEmail.Maxlength = 25
		Me.txtEmail.TabIndex = 11
		Me.txtEmail.Tag = "11"
		Me.txtEmail.AcceptsReturn = True
		Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmail.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmail.CausesValidation = True
		Me.txtEmail.Enabled = True
		Me.txtEmail.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmail.HideSelection = True
		Me.txtEmail.ReadOnly = False
		Me.txtEmail.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmail.MultiLine = False
		Me.txtEmail.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmail.TabStop = True
		Me.txtEmail.Visible = True
		Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmail.Name = "txtEmail"
		Me.txtSubDireccion.AutoSize = False
		Me.txtSubDireccion.Size = New System.Drawing.Size(412, 19)
		Me.txtSubDireccion.Location = New System.Drawing.Point(166, 93)
		Me.txtSubDireccion.Maxlength = 40
		Me.txtSubDireccion.TabIndex = 5
		Me.txtSubDireccion.Tag = "12"
		Me.txtSubDireccion.AcceptsReturn = True
		Me.txtSubDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubDireccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubDireccion.CausesValidation = True
		Me.txtSubDireccion.Enabled = True
		Me.txtSubDireccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubDireccion.HideSelection = True
		Me.txtSubDireccion.ReadOnly = False
		Me.txtSubDireccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubDireccion.MultiLine = False
		Me.txtSubDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubDireccion.TabStop = True
		Me.txtSubDireccion.Visible = True
		Me.txtSubDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubDireccion.Name = "txtSubDireccion"
		Me.txtFax.AutoSize = False
		Me.txtFax.Size = New System.Drawing.Size(109, 19)
		Me.txtFax.Location = New System.Drawing.Point(166, 218)
		Me.txtFax.Maxlength = 14
		Me.txtFax.TabIndex = 10
		Me.txtFax.Tag = "10"
		Me.txtFax.AcceptsReturn = True
		Me.txtFax.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFax.BackColor = System.Drawing.SystemColors.Window
		Me.txtFax.CausesValidation = True
		Me.txtFax.Enabled = True
		Me.txtFax.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFax.HideSelection = True
		Me.txtFax.ReadOnly = False
		Me.txtFax.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFax.MultiLine = False
		Me.txtFax.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFax.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFax.TabStop = True
		Me.txtFax.Visible = True
		Me.txtFax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFax.Name = "txtFax"
		Me.txtNombreFiscal.AutoSize = False
		Me.txtNombreFiscal.Size = New System.Drawing.Size(411, 19)
		Me.txtNombreFiscal.Location = New System.Drawing.Point(166, 43)
		Me.txtNombreFiscal.Maxlength = 40
		Me.txtNombreFiscal.TabIndex = 3
		Me.txtNombreFiscal.Tag = "9###########1"
		Me.txtNombreFiscal.AcceptsReturn = True
		Me.txtNombreFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreFiscal.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreFiscal.CausesValidation = True
		Me.txtNombreFiscal.Enabled = True
		Me.txtNombreFiscal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreFiscal.HideSelection = True
		Me.txtNombreFiscal.ReadOnly = False
		Me.txtNombreFiscal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreFiscal.MultiLine = False
		Me.txtNombreFiscal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreFiscal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreFiscal.TabStop = True
		Me.txtNombreFiscal.Visible = True
		Me.txtNombreFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreFiscal.Name = "txtNombreFiscal"
		Me.txtTelefono.AutoSize = False
		Me.txtTelefono.Size = New System.Drawing.Size(109, 19)
		Me.txtTelefono.Location = New System.Drawing.Point(166, 193)
		Me.txtTelefono.Maxlength = 14
		Me.txtTelefono.TabIndex = 9
		Me.txtTelefono.Tag = "8###########1"
		Me.txtTelefono.AcceptsReturn = True
		Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefono.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefono.CausesValidation = True
		Me.txtTelefono.Enabled = True
		Me.txtTelefono.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefono.HideSelection = True
		Me.txtTelefono.ReadOnly = False
		Me.txtTelefono.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefono.MultiLine = False
		Me.txtTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefono.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefono.TabStop = True
		Me.txtTelefono.Visible = True
		Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefono.Name = "txtTelefono"
		Me.txtNif.AutoSize = False
		Me.txtNif.Size = New System.Drawing.Size(109, 19)
		Me.txtNif.Location = New System.Drawing.Point(166, 168)
		Me.txtNif.Maxlength = 10
		Me.txtNif.TabIndex = 8
		Me.txtNif.Tag = "7###########1"
		Me.txtNif.AcceptsReturn = True
		Me.txtNif.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNif.BackColor = System.Drawing.SystemColors.Window
		Me.txtNif.CausesValidation = True
		Me.txtNif.Enabled = True
		Me.txtNif.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNif.HideSelection = True
		Me.txtNif.ReadOnly = False
		Me.txtNif.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNif.MultiLine = False
		Me.txtNif.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNif.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNif.TabStop = True
		Me.txtNif.Visible = True
		Me.txtNif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNif.Name = "txtNif"
		Me.txtProvincia.AutoSize = False
		Me.txtProvincia.Size = New System.Drawing.Size(109, 19)
		Me.txtProvincia.Location = New System.Drawing.Point(166, 143)
		Me.txtProvincia.Maxlength = 20
		Me.txtProvincia.TabIndex = 7
		Me.txtProvincia.Tag = "6###########1"
		Me.txtProvincia.AcceptsReturn = True
		Me.txtProvincia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProvincia.BackColor = System.Drawing.SystemColors.Window
		Me.txtProvincia.CausesValidation = True
		Me.txtProvincia.Enabled = True
		Me.txtProvincia.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProvincia.HideSelection = True
		Me.txtProvincia.ReadOnly = False
		Me.txtProvincia.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProvincia.MultiLine = False
		Me.txtProvincia.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProvincia.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProvincia.TabStop = True
		Me.txtProvincia.Visible = True
		Me.txtProvincia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtProvincia.Name = "txtProvincia"
		Me.txtPoblacion.AutoSize = False
		Me.txtPoblacion.Size = New System.Drawing.Size(310, 19)
		Me.txtPoblacion.Location = New System.Drawing.Point(166, 118)
		Me.txtPoblacion.Maxlength = 30
		Me.txtPoblacion.TabIndex = 6
		Me.txtPoblacion.Tag = "5###########1"
		Me.txtPoblacion.AcceptsReturn = True
		Me.txtPoblacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPoblacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtPoblacion.CausesValidation = True
		Me.txtPoblacion.Enabled = True
		Me.txtPoblacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPoblacion.HideSelection = True
		Me.txtPoblacion.ReadOnly = False
		Me.txtPoblacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPoblacion.MultiLine = False
		Me.txtPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPoblacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPoblacion.TabStop = True
		Me.txtPoblacion.Visible = True
		Me.txtPoblacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPoblacion.Name = "txtPoblacion"
		Me.txtDireccion.AutoSize = False
		Me.txtDireccion.Size = New System.Drawing.Size(412, 19)
		Me.txtDireccion.Location = New System.Drawing.Point(166, 68)
		Me.txtDireccion.Maxlength = 40
		Me.txtDireccion.TabIndex = 4
		Me.txtDireccion.Tag = "4###########1"
		Me.txtDireccion.AcceptsReturn = True
		Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDireccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDireccion.CausesValidation = True
		Me.txtDireccion.Enabled = True
		Me.txtDireccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDireccion.HideSelection = True
		Me.txtDireccion.ReadOnly = False
		Me.txtDireccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDireccion.MultiLine = False
		Me.txtDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDireccion.TabStop = True
		Me.txtDireccion.Visible = True
		Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDireccion.Name = "txtDireccion"
		Me.txtNombreEmpresa.AutoSize = False
		Me.txtNombreEmpresa.Size = New System.Drawing.Size(411, 19)
		Me.txtNombreEmpresa.Location = New System.Drawing.Point(166, 18)
		Me.txtNombreEmpresa.Maxlength = 50
		Me.txtNombreEmpresa.TabIndex = 2
		Me.txtNombreEmpresa.Tag = "3###########1"
		Me.txtNombreEmpresa.AcceptsReturn = True
		Me.txtNombreEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreEmpresa.CausesValidation = True
		Me.txtNombreEmpresa.Enabled = True
		Me.txtNombreEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreEmpresa.HideSelection = True
		Me.txtNombreEmpresa.ReadOnly = False
		Me.txtNombreEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreEmpresa.MultiLine = False
		Me.txtNombreEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreEmpresa.TabStop = True
		Me.txtNombreEmpresa.Visible = True
		Me.txtNombreEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreEmpresa.Name = "txtNombreEmpresa"
		Me.txtDatosRegistro.AutoSize = False
		Me.txtDatosRegistro.Size = New System.Drawing.Size(412, 45)
		Me.txtDatosRegistro.Location = New System.Drawing.Point(166, 293)
		Me.txtDatosRegistro.Maxlength = 35
		Me.txtDatosRegistro.MultiLine = True
		Me.txtDatosRegistro.TabIndex = 13
		Me.txtDatosRegistro.Tag = "13###########1"
		Me.txtDatosRegistro.AcceptsReturn = True
		Me.txtDatosRegistro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDatosRegistro.BackColor = System.Drawing.SystemColors.Window
		Me.txtDatosRegistro.CausesValidation = True
		Me.txtDatosRegistro.Enabled = True
		Me.txtDatosRegistro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDatosRegistro.HideSelection = True
		Me.txtDatosRegistro.ReadOnly = False
		Me.txtDatosRegistro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDatosRegistro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDatosRegistro.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDatosRegistro.TabStop = True
		Me.txtDatosRegistro.Visible = True
		Me.txtDatosRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDatosRegistro.Name = "txtDatosRegistro"
		Me.lbl14.Text = "Direcci�n web"
		Me.lbl14.Size = New System.Drawing.Size(129, 15)
		Me.lbl14.Location = New System.Drawing.Point(26, 268)
		Me.lbl14.TabIndex = 47
		Me.lbl14.Tag = "14"
		Me.lbl14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl14.BackColor = System.Drawing.SystemColors.Control
		Me.lbl14.Enabled = True
		Me.lbl14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl14.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl14.UseMnemonic = True
		Me.lbl14.Visible = True
		Me.lbl14.AutoSize = False
		Me.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl14.Name = "lbl14"
		Me.lbl11.Text = "Email"
		Me.lbl11.Size = New System.Drawing.Size(129, 15)
		Me.lbl11.Location = New System.Drawing.Point(26, 243)
		Me.lbl11.TabIndex = 46
		Me.lbl11.Tag = "11"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl12.Text = "Sub-direcci�n"
		Me.lbl12.Size = New System.Drawing.Size(129, 15)
		Me.lbl12.Location = New System.Drawing.Point(26, 96)
		Me.lbl12.TabIndex = 45
		Me.lbl12.Tag = "12"
		Me.lbl12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl12.BackColor = System.Drawing.SystemColors.Control
		Me.lbl12.Enabled = True
		Me.lbl12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl12.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl12.UseMnemonic = True
		Me.lbl12.Visible = True
		Me.lbl12.AutoSize = False
		Me.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl12.Name = "lbl12"
		Me.lbl10.Text = "Fax"
		Me.lbl10.Size = New System.Drawing.Size(129, 15)
		Me.lbl10.Location = New System.Drawing.Point(26, 219)
		Me.lbl10.TabIndex = 44
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl9.Text = "Nombre fiscal"
		Me.lbl9.Size = New System.Drawing.Size(129, 15)
		Me.lbl9.Location = New System.Drawing.Point(26, 47)
		Me.lbl9.TabIndex = 38
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl8.Text = "Telefono"
		Me.lbl8.Size = New System.Drawing.Size(129, 15)
		Me.lbl8.Location = New System.Drawing.Point(26, 194)
		Me.lbl8.TabIndex = 37
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl7.Text = "NIF"
		Me.lbl7.Size = New System.Drawing.Size(129, 15)
		Me.lbl7.Location = New System.Drawing.Point(26, 170)
		Me.lbl7.TabIndex = 36
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Provincia"
		Me.lbl6.Size = New System.Drawing.Size(129, 15)
		Me.lbl6.Location = New System.Drawing.Point(26, 145)
		Me.lbl6.TabIndex = 35
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl5.Text = "Poblaci�n"
		Me.lbl5.Size = New System.Drawing.Size(129, 15)
		Me.lbl5.Location = New System.Drawing.Point(26, 120)
		Me.lbl5.TabIndex = 34
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl4.Text = "Direcci�n"
		Me.lbl4.Size = New System.Drawing.Size(129, 15)
		Me.lbl4.Location = New System.Drawing.Point(26, 71)
		Me.lbl4.TabIndex = 33
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl3.Text = "Nombre comercial"
		Me.lbl3.Size = New System.Drawing.Size(129, 15)
		Me.lbl3.Location = New System.Drawing.Point(27, 22)
		Me.lbl3.TabIndex = 32
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.Label3.Text = "Datos registro mercantil"
		Me.Label3.Size = New System.Drawing.Size(129, 15)
		Me.Label3.Location = New System.Drawing.Point(26, 293)
		Me.Label3.TabIndex = 31
		Me.Label3.Tag = "13"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(43, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(112, 11)
		Me.txtEmpresa.Maxlength = 4
		Me.txtEmpresa.TabIndex = 0
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(294, 19)
		Me.Text1.Location = New System.Drawing.Point(158, 11)
		Me.Text1.TabIndex = 21
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtSistema.AutoSize = False
		Me.txtSistema.Size = New System.Drawing.Size(22, 19)
		Me.txtSistema.Location = New System.Drawing.Point(112, 35)
		Me.txtSistema.Maxlength = 1
		Me.txtSistema.TabIndex = 1
		Me.txtSistema.Tag = "*2"
		Me.txtSistema.AcceptsReturn = True
		Me.txtSistema.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSistema.BackColor = System.Drawing.SystemColors.Window
		Me.txtSistema.CausesValidation = True
		Me.txtSistema.Enabled = True
		Me.txtSistema.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSistema.HideSelection = True
		Me.txtSistema.ReadOnly = False
		Me.txtSistema.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSistema.MultiLine = False
		Me.txtSistema.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSistema.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSistema.TabStop = True
		Me.txtSistema.Visible = True
		Me.txtSistema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSistema.Name = "txtSistema"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(554, 442)
		Me.cmdAceptar.TabIndex = 23
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.lblLock.Text = "Registro bloqueado por otra sesion de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(10, 444)
		Me.lblLock.TabIndex = 24
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(99, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 13)
		Me.lbl1.TabIndex = 48
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Sistema de gesti�n"
		Me.lbl2.Size = New System.Drawing.Size(99, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 37)
		Me.lbl2.TabIndex = 22
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkNoCerrarContabilidad, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtSistema)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(lblLock)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControl1.Controls.Add(TabControlPage3)
		Me.TabControl1.Controls.Add(TabControlPage4)
		Me.TabControl1.Controls.Add(TabControlPage5)
		Me.TabControlPage5.Controls.Add(txtTextoLopd)
		Me.TabControlPage5.Controls.Add(Label4)
		Me.TabControlPage4.Controls.Add(chkNoCerrarContabilidad)
		Me.TabControlPage4.Controls.Add(txtSisGesFina)
		Me.TabControlPage4.Controls.Add(txtEmpresaFinanzas)
		Me.TabControlPage4.Controls.Add(txtNamesFin)
		Me.TabControlPage4.Controls.Add(Label2)
		Me.TabControlPage4.Controls.Add(Label1)
		Me.TabControlPage4.Controls.Add(Label6)
		Me.TabControlPage3.Controls.Add(grdImpresosGraf)
		Me.TabControlPage2.Controls.Add(txtNamespaceFinanzas)
		Me.TabControlPage2.Controls.Add(cmbLogoPredeterminado)
		Me.TabControlPage2.Controls.Add(lbl19)
		Me.TabControlPage2.Controls.Add(lbl16)
		Me.TabControlPage1.Controls.Add(txtUrl)
		Me.TabControlPage1.Controls.Add(txtEmail)
		Me.TabControlPage1.Controls.Add(txtSubDireccion)
		Me.TabControlPage1.Controls.Add(txtFax)
		Me.TabControlPage1.Controls.Add(txtNombreFiscal)
		Me.TabControlPage1.Controls.Add(txtTelefono)
		Me.TabControlPage1.Controls.Add(txtNif)
		Me.TabControlPage1.Controls.Add(txtProvincia)
		Me.TabControlPage1.Controls.Add(txtPoblacion)
		Me.TabControlPage1.Controls.Add(txtDireccion)
		Me.TabControlPage1.Controls.Add(txtNombreEmpresa)
		Me.TabControlPage1.Controls.Add(txtDatosRegistro)
		Me.TabControlPage1.Controls.Add(lbl14)
		Me.TabControlPage1.Controls.Add(lbl11)
		Me.TabControlPage1.Controls.Add(lbl12)
		Me.TabControlPage1.Controls.Add(lbl10)
		Me.TabControlPage1.Controls.Add(lbl9)
		Me.TabControlPage1.Controls.Add(lbl8)
		Me.TabControlPage1.Controls.Add(lbl7)
		Me.TabControlPage1.Controls.Add(lbl6)
		Me.TabControlPage1.Controls.Add(lbl5)
		Me.TabControlPage1.Controls.Add(lbl4)
		Me.TabControlPage1.Controls.Add(lbl3)
		Me.TabControlPage1.Controls.Add(Label3)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage5.ResumeLayout(False)
		Me.TabControlPage4.ResumeLayout(False)
		Me.TabControlPage3.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
