Option Strict Off
Option Explicit On
Friend Class frmNivellsEstructuraSeguretat
	Inherits FormParent
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		GetAllNodes(trvEstructura).Clear()
	End Sub
	
	Private Sub chkVeureTot_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVeureTot.ClickEvent
		If chkVeureTot.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			'UPGRADE_WARNING: El l�mite inferior de la colecci�n trvEstructura.Nodes cambi� de 1 a 0. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			GetAllNodes(trvEstructura).Item(1).Checked = False
			'UPGRADE_WARNING: El l�mite inferior de la colecci�n trvEstructura.Nodes cambi� de 1 a 0. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			MarcaNode(GetAllNodes(trvEstructura).Item(1))
			trvEstructura.Enabled = False
		Else
			trvEstructura.Enabled = True
		End If
	End Sub
	
	Private Sub cmdCerrar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCerrar.ClickEvent
		Me.Unload()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If Permis(Me, e_Permisos.Execucions) = False Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtUsuari.Text
		MCache.P2 = chkVeureTot
		MCache.P3 = GetCheckedNodes(trvEstructura, 4)
		CacheXecute("D GSEG^ENT.G.INDSEG")
	End Sub
	
	Private Sub frmNivellsEstructuraSeguretat_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmNivellsEstructuraSeguretat_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmNivellsEstructuraSeguretat_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		IniciForm(Me)
		Text2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
	End Sub
	
	Private Sub trvEstructura_AfterCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.TreeViewEventArgs) Handles trvEstructura.AfterCheck
		Dim Node As System.Windows.Forms.TreeNode = eventArgs.Node
		MarcaNode(Node)
	End Sub
	
	Private Sub txtUsuari_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.DoubleClick
		ConsultaTaula(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtUsuari_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuari.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		MontaArbre()
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub MontaArbre()
		Dim Reg As String
		If txtUsuari.Text = "" Then Exit Sub
		Reg = CarregaTreeView(trvEstructura, "CTS^ENT.G.INDSEG", txtUsuari.Text, True)
		txtUsuari.Enabled = False
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		chkVeureTot.Value = Val(Piece(Reg, S, 1))
	End Sub
End Class
