<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParametresRecursosHumans
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtCodigoPoblacion As System.Windows.Forms.TextBox
	Public WithEvents txtXecuteVerificacion As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtCodigoEmpresaNom As System.Windows.Forms.TextBox
	Public WithEvents chkTrabajarVinculado As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmbProgramaNomina As DataControl.GmsCombo
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label30 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents GroupBox1 As DataControl.GmsGroupBox
	Public WithEvents txtSubvencions As System.Windows.Forms.TextBox
	Public WithEvents txtFirmes As System.Windows.Forms.TextBox
	Public WithEvents txtPathEntitats As System.Windows.Forms.TextBox
	Public WithEvents txtPathPublicitat As System.Windows.Forms.TextBox
	Public WithEvents txtPathFitxa As System.Windows.Forms.TextBox
	Public WithEvents TxtPathcomunicats As System.Windows.Forms.TextBox
	Public WithEvents txtPathRiscos As System.Windows.Forms.TextBox
	Public WithEvents txtPathEnquestes As System.Windows.Forms.TextBox
	Public WithEvents txtPathCursos2 As System.Windows.Forms.TextBox
	Public WithEvents txtPathCurriculums As System.Windows.Forms.TextBox
	Public WithEvents txtPathComunicacions As System.Windows.Forms.TextBox
	Public WithEvents txtPathCursos As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtPathFicheros As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Command1 As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdCursos As AxXtremeSuiteControls.AxPushButton
	Public WithEvents CmdComunicacions As AxXtremeSuiteControls.AxPushButton
	Public WithEvents CmbCurriculums As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbCursos2 As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbEnquestes As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbRiscos As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbComunicats As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbFitxa As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdEntitats As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdPublicitat As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdFirmes As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdSubvencions As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Line3 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParametresRecursosHumans))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.GroupBox1 = New DataControl.GmsGroupBox
		Me.txtCodigoPoblacion = New System.Windows.Forms.TextBox
		Me.txtXecuteVerificacion = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtCodigoEmpresaNom = New System.Windows.Forms.TextBox
		Me.chkTrabajarVinculado = New AxXtremeSuiteControls.AxCheckBox
		Me.cmbProgramaNomina = New DataControl.GmsCombo
		Me.Label14 = New System.Windows.Forms.Label
		Me.Label15 = New System.Windows.Forms.Label
		Me.Label30 = New System.Windows.Forms.Label
		Me.Label13 = New System.Windows.Forms.Label
		Me.txtSubvencions = New System.Windows.Forms.TextBox
		Me.txtFirmes = New System.Windows.Forms.TextBox
		Me.txtPathEntitats = New System.Windows.Forms.TextBox
		Me.txtPathPublicitat = New System.Windows.Forms.TextBox
		Me.txtPathFitxa = New System.Windows.Forms.TextBox
		Me.TxtPathcomunicats = New System.Windows.Forms.TextBox
		Me.txtPathRiscos = New System.Windows.Forms.TextBox
		Me.txtPathEnquestes = New System.Windows.Forms.TextBox
		Me.txtPathCursos2 = New System.Windows.Forms.TextBox
		Me.txtPathCurriculums = New System.Windows.Forms.TextBox
		Me.txtPathComunicacions = New System.Windows.Forms.TextBox
		Me.txtPathCursos = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtPathFicheros = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Command1 = New AxXtremeSuiteControls.AxPushButton
		Me.cmdCursos = New AxXtremeSuiteControls.AxPushButton
		Me.CmdComunicacions = New AxXtremeSuiteControls.AxPushButton
		Me.CmbCurriculums = New AxXtremeSuiteControls.AxPushButton
		Me.cmbCursos2 = New AxXtremeSuiteControls.AxPushButton
		Me.cmbEnquestes = New AxXtremeSuiteControls.AxPushButton
		Me.cmbRiscos = New AxXtremeSuiteControls.AxPushButton
		Me.cmbComunicats = New AxXtremeSuiteControls.AxPushButton
		Me.cmbFitxa = New AxXtremeSuiteControls.AxPushButton
		Me.cmdEntitats = New AxXtremeSuiteControls.AxPushButton
		Me.cmdPublicitat = New AxXtremeSuiteControls.AxPushButton
		Me.cmdFirmes = New AxXtremeSuiteControls.AxPushButton
		Me.cmdSubvencions = New AxXtremeSuiteControls.AxPushButton
		Me.Label12 = New System.Windows.Forms.Label
		Me.Label11 = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Line3 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkTrabajarVinculado, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Command1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCursos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.CmdComunicacions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.CmbCurriculums, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbCursos2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEnquestes, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbRiscos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbComunicats, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbFitxa, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdEntitats, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdPublicitat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdFirmes, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdSubvencions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Par�metros recursos humanos"
		Me.ClientSize = New System.Drawing.Size(610, 539)
		Me.Location = New System.Drawing.Point(474, 264)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-RRHH"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParametresRecursosHumans"

		Me.GroupBox1.Size = New System.Drawing.Size(583, 137)
		Me.GroupBox1.Location = New System.Drawing.Point(6, 36)
Me.GroupBox1.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder
		Me.GroupBox1.TabIndex = 50
Me.GroupBox1.Text = "Vinculaci�n con programa de n�mina"
		Me.GroupBox1.Name = "GroupBox1"
		Me.txtCodigoPoblacion.AutoSize = False
		Me.txtCodigoPoblacion.Size = New System.Drawing.Size(52, 19)
		Me.txtCodigoPoblacion.Location = New System.Drawing.Point(202, 98)
		Me.txtCodigoPoblacion.Maxlength = 5
		Me.txtCodigoPoblacion.TabIndex = 5
		Me.txtCodigoPoblacion.Tag = "19"
		Me.txtCodigoPoblacion.AcceptsReturn = True
		Me.txtCodigoPoblacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoPoblacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoPoblacion.CausesValidation = True
		Me.txtCodigoPoblacion.Enabled = True
		Me.txtCodigoPoblacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoPoblacion.HideSelection = True
		Me.txtCodigoPoblacion.ReadOnly = False
		Me.txtCodigoPoblacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoPoblacion.MultiLine = False
		Me.txtCodigoPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoPoblacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoPoblacion.TabStop = True
		Me.txtCodigoPoblacion.Visible = True
		Me.txtCodigoPoblacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoPoblacion.Name = "txtCodigoPoblacion"
		Me.txtXecuteVerificacion.AutoSize = False
		Me.txtXecuteVerificacion.Size = New System.Drawing.Size(369, 19)
		Me.txtXecuteVerificacion.Location = New System.Drawing.Point(202, 72)
		Me.txtXecuteVerificacion.Maxlength = 50
		Me.txtXecuteVerificacion.TabIndex = 4
		Me.txtXecuteVerificacion.Tag = "18"
		Me.txtXecuteVerificacion.AcceptsReturn = True
		Me.txtXecuteVerificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtXecuteVerificacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtXecuteVerificacion.CausesValidation = True
		Me.txtXecuteVerificacion.Enabled = True
		Me.txtXecuteVerificacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtXecuteVerificacion.HideSelection = True
		Me.txtXecuteVerificacion.ReadOnly = False
		Me.txtXecuteVerificacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtXecuteVerificacion.MultiLine = False
		Me.txtXecuteVerificacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtXecuteVerificacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtXecuteVerificacion.TabStop = True
		Me.txtXecuteVerificacion.Visible = True
		Me.txtXecuteVerificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtXecuteVerificacion.Name = "txtXecuteVerificacion"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(314, 19)
		Me.Text4.Location = New System.Drawing.Point(257, 98)
		Me.Text4.TabIndex = 53
		Me.Text4.Tag = "^19"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtCodigoEmpresaNom.AutoSize = False
		Me.txtCodigoEmpresaNom.Size = New System.Drawing.Size(93, 19)
		Me.txtCodigoEmpresaNom.Location = New System.Drawing.Point(478, 48)
		Me.txtCodigoEmpresaNom.Maxlength = 50
		Me.txtCodigoEmpresaNom.TabIndex = 3
		Me.txtCodigoEmpresaNom.Tag = "17"
		Me.txtCodigoEmpresaNom.AcceptsReturn = True
		Me.txtCodigoEmpresaNom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoEmpresaNom.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoEmpresaNom.CausesValidation = True
		Me.txtCodigoEmpresaNom.Enabled = True
		Me.txtCodigoEmpresaNom.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoEmpresaNom.HideSelection = True
		Me.txtCodigoEmpresaNom.ReadOnly = False
		Me.txtCodigoEmpresaNom.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoEmpresaNom.MultiLine = False
		Me.txtCodigoEmpresaNom.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoEmpresaNom.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoEmpresaNom.TabStop = True
		Me.txtCodigoEmpresaNom.Visible = True
		Me.txtCodigoEmpresaNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoEmpresaNom.Name = "txtCodigoEmpresaNom"
		chkTrabajarVinculado.OcxState = CType(resources.GetObject("chkTrabajarVinculado.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTrabajarVinculado.Size = New System.Drawing.Size(197, 19)
		Me.chkTrabajarVinculado.Location = New System.Drawing.Point(18, 24)
		Me.chkTrabajarVinculado.TabIndex = 1
Me.chkTrabajarVinculado.Tag = "15"
		Me.chkTrabajarVinculado.Name = "chkTrabajarVinculado"

		Me.cmbProgramaNomina.Size = New System.Drawing.Size(119, 19)
		Me.cmbProgramaNomina.Location = New System.Drawing.Point(202, 48)
		Me.cmbProgramaNomina.TabIndex = 2
Me.cmbProgramaNomina.Tag = "16"
		Me.cmbProgramaNomina.Name = "cmbProgramaNomina"
		Me.Label14.Text = "Xecute verificacion importar registro"
		Me.Label14.Size = New System.Drawing.Size(177, 15)
		Me.Label14.Location = New System.Drawing.Point(18, 76)
		Me.Label14.TabIndex = 55
		Me.Label14.Tag = "18"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label14.BackColor = System.Drawing.SystemColors.Control
		Me.Label14.Enabled = True
		Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.UseMnemonic = True
		Me.Label14.Visible = True
		Me.Label14.AutoSize = False
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Name = "Label14"
		Me.Label15.Text = "C�digo poblaci�n desconocida"
		Me.Label15.Size = New System.Drawing.Size(177, 15)
		Me.Label15.Location = New System.Drawing.Point(18, 102)
		Me.Label15.TabIndex = 54
		Me.Label15.Tag = "19"
		Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label15.BackColor = System.Drawing.SystemColors.Control
		Me.Label15.Enabled = True
		Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.UseMnemonic = True
		Me.Label15.Visible = True
		Me.Label15.AutoSize = False
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Name = "Label15"
		Me.Label30.Text = "Programa n�mina"
		Me.Label30.Size = New System.Drawing.Size(90, 15)
		Me.Label30.Location = New System.Drawing.Point(18, 50)
		Me.Label30.TabIndex = 52
		Me.Label30.Tag = "16"
		Me.Label30.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label30.BackColor = System.Drawing.SystemColors.Control
		Me.Label30.Enabled = True
		Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label30.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label30.UseMnemonic = True
		Me.Label30.Visible = True
		Me.Label30.AutoSize = False
		Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label30.Name = "Label30"
		Me.Label13.Text = "C�digo empresa n�mina"
		Me.Label13.Size = New System.Drawing.Size(119, 15)
		Me.Label13.Location = New System.Drawing.Point(356, 52)
		Me.Label13.TabIndex = 51
		Me.Label13.Tag = "17"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label13.BackColor = System.Drawing.SystemColors.Control
		Me.Label13.Enabled = True
		Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.UseMnemonic = True
		Me.Label13.Visible = True
		Me.Label13.AutoSize = False
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Name = "Label13"
		Me.txtSubvencions.AutoSize = False
		Me.txtSubvencions.Size = New System.Drawing.Size(369, 19)
		Me.txtSubvencions.Location = New System.Drawing.Point(192, 469)
		Me.txtSubvencions.Maxlength = 50
		Me.txtSubvencions.TabIndex = 18
		Me.txtSubvencions.Tag = "14"
		Me.txtSubvencions.AcceptsReturn = True
		Me.txtSubvencions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubvencions.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubvencions.CausesValidation = True
		Me.txtSubvencions.Enabled = True
		Me.txtSubvencions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubvencions.HideSelection = True
		Me.txtSubvencions.ReadOnly = False
		Me.txtSubvencions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubvencions.MultiLine = False
		Me.txtSubvencions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubvencions.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubvencions.TabStop = True
		Me.txtSubvencions.Visible = True
		Me.txtSubvencions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubvencions.Name = "txtSubvencions"
		Me.txtFirmes.AutoSize = False
		Me.txtFirmes.Size = New System.Drawing.Size(369, 19)
		Me.txtFirmes.Location = New System.Drawing.Point(192, 445)
		Me.txtFirmes.Maxlength = 50
		Me.txtFirmes.TabIndex = 17
		Me.txtFirmes.Tag = "13"
		Me.txtFirmes.AcceptsReturn = True
		Me.txtFirmes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFirmes.BackColor = System.Drawing.SystemColors.Window
		Me.txtFirmes.CausesValidation = True
		Me.txtFirmes.Enabled = True
		Me.txtFirmes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFirmes.HideSelection = True
		Me.txtFirmes.ReadOnly = False
		Me.txtFirmes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFirmes.MultiLine = False
		Me.txtFirmes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFirmes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFirmes.TabStop = True
		Me.txtFirmes.Visible = True
		Me.txtFirmes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFirmes.Name = "txtFirmes"
		Me.txtPathEntitats.AutoSize = False
		Me.txtPathEntitats.Size = New System.Drawing.Size(369, 19)
		Me.txtPathEntitats.Location = New System.Drawing.Point(192, 397)
		Me.txtPathEntitats.Maxlength = 50
		Me.txtPathEntitats.TabIndex = 15
		Me.txtPathEntitats.Tag = "11"
		Me.txtPathEntitats.AcceptsReturn = True
		Me.txtPathEntitats.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathEntitats.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathEntitats.CausesValidation = True
		Me.txtPathEntitats.Enabled = True
		Me.txtPathEntitats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathEntitats.HideSelection = True
		Me.txtPathEntitats.ReadOnly = False
		Me.txtPathEntitats.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathEntitats.MultiLine = False
		Me.txtPathEntitats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathEntitats.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathEntitats.TabStop = True
		Me.txtPathEntitats.Visible = True
		Me.txtPathEntitats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathEntitats.Name = "txtPathEntitats"
		Me.txtPathPublicitat.AutoSize = False
		Me.txtPathPublicitat.Size = New System.Drawing.Size(369, 19)
		Me.txtPathPublicitat.Location = New System.Drawing.Point(192, 421)
		Me.txtPathPublicitat.Maxlength = 50
		Me.txtPathPublicitat.TabIndex = 16
		Me.txtPathPublicitat.Tag = "12"
		Me.txtPathPublicitat.AcceptsReturn = True
		Me.txtPathPublicitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathPublicitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathPublicitat.CausesValidation = True
		Me.txtPathPublicitat.Enabled = True
		Me.txtPathPublicitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathPublicitat.HideSelection = True
		Me.txtPathPublicitat.ReadOnly = False
		Me.txtPathPublicitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathPublicitat.MultiLine = False
		Me.txtPathPublicitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathPublicitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathPublicitat.TabStop = True
		Me.txtPathPublicitat.Visible = True
		Me.txtPathPublicitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathPublicitat.Name = "txtPathPublicitat"
		Me.txtPathFitxa.AutoSize = False
		Me.txtPathFitxa.Size = New System.Drawing.Size(369, 19)
		Me.txtPathFitxa.Location = New System.Drawing.Point(192, 373)
		Me.txtPathFitxa.Maxlength = 50
		Me.txtPathFitxa.TabIndex = 14
		Me.txtPathFitxa.Tag = "10"
		Me.txtPathFitxa.AcceptsReturn = True
		Me.txtPathFitxa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathFitxa.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathFitxa.CausesValidation = True
		Me.txtPathFitxa.Enabled = True
		Me.txtPathFitxa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathFitxa.HideSelection = True
		Me.txtPathFitxa.ReadOnly = False
		Me.txtPathFitxa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathFitxa.MultiLine = False
		Me.txtPathFitxa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathFitxa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathFitxa.TabStop = True
		Me.txtPathFitxa.Visible = True
		Me.txtPathFitxa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathFitxa.Name = "txtPathFitxa"
		Me.TxtPathcomunicats.AutoSize = False
		Me.TxtPathcomunicats.Size = New System.Drawing.Size(369, 19)
		Me.TxtPathcomunicats.Location = New System.Drawing.Point(192, 349)
		Me.TxtPathcomunicats.Maxlength = 50
		Me.TxtPathcomunicats.TabIndex = 13
		Me.TxtPathcomunicats.Tag = "9"
		Me.TxtPathcomunicats.AcceptsReturn = True
		Me.TxtPathcomunicats.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.TxtPathcomunicats.BackColor = System.Drawing.SystemColors.Window
		Me.TxtPathcomunicats.CausesValidation = True
		Me.TxtPathcomunicats.Enabled = True
		Me.TxtPathcomunicats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.TxtPathcomunicats.HideSelection = True
		Me.TxtPathcomunicats.ReadOnly = False
		Me.TxtPathcomunicats.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.TxtPathcomunicats.MultiLine = False
		Me.TxtPathcomunicats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.TxtPathcomunicats.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.TxtPathcomunicats.TabStop = True
		Me.TxtPathcomunicats.Visible = True
		Me.TxtPathcomunicats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.TxtPathcomunicats.Name = "TxtPathcomunicats"
		Me.txtPathRiscos.AutoSize = False
		Me.txtPathRiscos.Size = New System.Drawing.Size(369, 19)
		Me.txtPathRiscos.Location = New System.Drawing.Point(192, 325)
		Me.txtPathRiscos.Maxlength = 50
		Me.txtPathRiscos.TabIndex = 12
		Me.txtPathRiscos.Tag = "8"
		Me.txtPathRiscos.AcceptsReturn = True
		Me.txtPathRiscos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathRiscos.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathRiscos.CausesValidation = True
		Me.txtPathRiscos.Enabled = True
		Me.txtPathRiscos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathRiscos.HideSelection = True
		Me.txtPathRiscos.ReadOnly = False
		Me.txtPathRiscos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathRiscos.MultiLine = False
		Me.txtPathRiscos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathRiscos.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathRiscos.TabStop = True
		Me.txtPathRiscos.Visible = True
		Me.txtPathRiscos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathRiscos.Name = "txtPathRiscos"
		Me.txtPathEnquestes.AutoSize = False
		Me.txtPathEnquestes.Size = New System.Drawing.Size(369, 19)
		Me.txtPathEnquestes.Location = New System.Drawing.Point(192, 301)
		Me.txtPathEnquestes.Maxlength = 50
		Me.txtPathEnquestes.TabIndex = 11
		Me.txtPathEnquestes.Tag = "7"
		Me.txtPathEnquestes.AcceptsReturn = True
		Me.txtPathEnquestes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathEnquestes.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathEnquestes.CausesValidation = True
		Me.txtPathEnquestes.Enabled = True
		Me.txtPathEnquestes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathEnquestes.HideSelection = True
		Me.txtPathEnquestes.ReadOnly = False
		Me.txtPathEnquestes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathEnquestes.MultiLine = False
		Me.txtPathEnquestes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathEnquestes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathEnquestes.TabStop = True
		Me.txtPathEnquestes.Visible = True
		Me.txtPathEnquestes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathEnquestes.Name = "txtPathEnquestes"
		Me.txtPathCursos2.AutoSize = False
		Me.txtPathCursos2.Size = New System.Drawing.Size(369, 19)
		Me.txtPathCursos2.Location = New System.Drawing.Point(192, 277)
		Me.txtPathCursos2.Maxlength = 50
		Me.txtPathCursos2.TabIndex = 10
		Me.txtPathCursos2.Tag = "6"
		Me.txtPathCursos2.AcceptsReturn = True
		Me.txtPathCursos2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathCursos2.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathCursos2.CausesValidation = True
		Me.txtPathCursos2.Enabled = True
		Me.txtPathCursos2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathCursos2.HideSelection = True
		Me.txtPathCursos2.ReadOnly = False
		Me.txtPathCursos2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathCursos2.MultiLine = False
		Me.txtPathCursos2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathCursos2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathCursos2.TabStop = True
		Me.txtPathCursos2.Visible = True
		Me.txtPathCursos2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathCursos2.Name = "txtPathCursos2"
		Me.txtPathCurriculums.AutoSize = False
		Me.txtPathCurriculums.Size = New System.Drawing.Size(369, 19)
		Me.txtPathCurriculums.Location = New System.Drawing.Point(192, 253)
		Me.txtPathCurriculums.Maxlength = 50
		Me.txtPathCurriculums.TabIndex = 9
		Me.txtPathCurriculums.Tag = "5"
		Me.txtPathCurriculums.AcceptsReturn = True
		Me.txtPathCurriculums.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathCurriculums.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathCurriculums.CausesValidation = True
		Me.txtPathCurriculums.Enabled = True
		Me.txtPathCurriculums.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathCurriculums.HideSelection = True
		Me.txtPathCurriculums.ReadOnly = False
		Me.txtPathCurriculums.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathCurriculums.MultiLine = False
		Me.txtPathCurriculums.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathCurriculums.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathCurriculums.TabStop = True
		Me.txtPathCurriculums.Visible = True
		Me.txtPathCurriculums.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathCurriculums.Name = "txtPathCurriculums"
		Me.txtPathComunicacions.AutoSize = False
		Me.txtPathComunicacions.Size = New System.Drawing.Size(369, 19)
		Me.txtPathComunicacions.Location = New System.Drawing.Point(192, 229)
		Me.txtPathComunicacions.Maxlength = 50
		Me.txtPathComunicacions.TabIndex = 8
		Me.txtPathComunicacions.Tag = "4"
		Me.txtPathComunicacions.AcceptsReturn = True
		Me.txtPathComunicacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathComunicacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathComunicacions.CausesValidation = True
		Me.txtPathComunicacions.Enabled = True
		Me.txtPathComunicacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathComunicacions.HideSelection = True
		Me.txtPathComunicacions.ReadOnly = False
		Me.txtPathComunicacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathComunicacions.MultiLine = False
		Me.txtPathComunicacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathComunicacions.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathComunicacions.TabStop = True
		Me.txtPathComunicacions.Visible = True
		Me.txtPathComunicacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathComunicacions.Name = "txtPathComunicacions"
		Me.txtPathCursos.AutoSize = False
		Me.txtPathCursos.Size = New System.Drawing.Size(369, 19)
		Me.txtPathCursos.Location = New System.Drawing.Point(192, 205)
		Me.txtPathCursos.Maxlength = 50
		Me.txtPathCursos.TabIndex = 7
		Me.txtPathCursos.Tag = "3"
		Me.txtPathCursos.AcceptsReturn = True
		Me.txtPathCursos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathCursos.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathCursos.CausesValidation = True
		Me.txtPathCursos.Enabled = True
		Me.txtPathCursos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathCursos.HideSelection = True
		Me.txtPathCursos.ReadOnly = False
		Me.txtPathCursos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathCursos.MultiLine = False
		Me.txtPathCursos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathCursos.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathCursos.TabStop = True
		Me.txtPathCursos.Visible = True
		Me.txtPathCursos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathCursos.Name = "txtPathCursos"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(52, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(192, 11)
		Me.txtEmpresa.Maxlength = 5
		Me.txtEmpresa.TabIndex = 0
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(274, 19)
		Me.Text1.Location = New System.Drawing.Point(247, 11)
		Me.Text1.TabIndex = 21
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtPathFicheros.AutoSize = False
		Me.txtPathFicheros.Size = New System.Drawing.Size(369, 19)
		Me.txtPathFicheros.Location = New System.Drawing.Point(192, 181)
		Me.txtPathFicheros.Maxlength = 50
		Me.txtPathFicheros.TabIndex = 6
		Me.txtPathFicheros.Tag = "2"
		Me.txtPathFicheros.AcceptsReturn = True
		Me.txtPathFicheros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathFicheros.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathFicheros.CausesValidation = True
		Me.txtPathFicheros.Enabled = True
		Me.txtPathFicheros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathFicheros.HideSelection = True
		Me.txtPathFicheros.ReadOnly = False
		Me.txtPathFicheros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathFicheros.MultiLine = False
		Me.txtPathFicheros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathFicheros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathFicheros.TabStop = True
		Me.txtPathFicheros.Visible = True
		Me.txtPathFicheros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathFicheros.Name = "txtPathFicheros"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(416, 505)
		Me.cmdAceptar.TabIndex = 20
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(506, 505)
		Me.cmdGuardar.TabIndex = 23
		Me.cmdGuardar.Name = "cmdGuardar"
		Command1.OcxState = CType(resources.GetObject("Command1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.Command1.Size = New System.Drawing.Size(23, 23)
		Me.Command1.Location = New System.Drawing.Point(566, 180)
		Me.Command1.TabIndex = 25
Me.Command1.Tag = "FolderSearch"
		Me.Command1.Name = "Command1"
		cmdCursos.OcxState = CType(resources.GetObject("cmdCursos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCursos.Size = New System.Drawing.Size(23, 23)
		Me.cmdCursos.Location = New System.Drawing.Point(566, 204)
		Me.cmdCursos.TabIndex = 26
Me.cmdCursos.Tag = "FolderSearch"
		Me.cmdCursos.Name = "cmdCursos"
		CmdComunicacions.OcxState = CType(resources.GetObject("CmdComunicacions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CmdComunicacions.Size = New System.Drawing.Size(23, 23)
		Me.CmdComunicacions.Location = New System.Drawing.Point(566, 228)
		Me.CmdComunicacions.TabIndex = 28
Me.CmdComunicacions.Tag = "FolderSearch"
		Me.CmdComunicacions.Name = "CmdComunicacions"
		CmbCurriculums.OcxState = CType(resources.GetObject("CmbCurriculums.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CmbCurriculums.Size = New System.Drawing.Size(23, 23)
		Me.CmbCurriculums.Location = New System.Drawing.Point(566, 252)
		Me.CmbCurriculums.TabIndex = 30
Me.CmbCurriculums.Tag = "FolderSearch"
		Me.CmbCurriculums.Name = "CmbCurriculums"
		cmbCursos2.OcxState = CType(resources.GetObject("cmbCursos2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbCursos2.Size = New System.Drawing.Size(23, 23)
		Me.cmbCursos2.Location = New System.Drawing.Point(566, 276)
		Me.cmbCursos2.TabIndex = 32
Me.cmbCursos2.Tag = "FolderSearch"
		Me.cmbCursos2.Name = "cmbCursos2"
		cmbEnquestes.OcxState = CType(resources.GetObject("cmbEnquestes.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEnquestes.Size = New System.Drawing.Size(23, 23)
		Me.cmbEnquestes.Location = New System.Drawing.Point(566, 300)
		Me.cmbEnquestes.TabIndex = 34
Me.cmbEnquestes.Tag = "FolderSearch"
		Me.cmbEnquestes.Name = "cmbEnquestes"
		cmbRiscos.OcxState = CType(resources.GetObject("cmbRiscos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbRiscos.Size = New System.Drawing.Size(23, 23)
		Me.cmbRiscos.Location = New System.Drawing.Point(566, 324)
		Me.cmbRiscos.TabIndex = 36
Me.cmbRiscos.Tag = "FolderSearch"
		Me.cmbRiscos.Name = "cmbRiscos"
		cmbComunicats.OcxState = CType(resources.GetObject("cmbComunicats.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbComunicats.Size = New System.Drawing.Size(23, 23)
		Me.cmbComunicats.Location = New System.Drawing.Point(566, 348)
		Me.cmbComunicats.TabIndex = 38
Me.cmbComunicats.Tag = "FolderSearch"
		Me.cmbComunicats.Name = "cmbComunicats"
		cmbFitxa.OcxState = CType(resources.GetObject("cmbFitxa.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbFitxa.Size = New System.Drawing.Size(23, 23)
		Me.cmbFitxa.Location = New System.Drawing.Point(566, 372)
		Me.cmbFitxa.TabIndex = 40
Me.cmbFitxa.Tag = "FolderSearch"
		Me.cmbFitxa.Name = "cmbFitxa"
		cmdEntitats.OcxState = CType(resources.GetObject("cmdEntitats.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdEntitats.Size = New System.Drawing.Size(23, 23)
		Me.cmdEntitats.Location = New System.Drawing.Point(566, 396)
		Me.cmdEntitats.TabIndex = 42
Me.cmdEntitats.Tag = "FolderSearch"
		Me.cmdEntitats.Name = "cmdEntitats"
		cmdPublicitat.OcxState = CType(resources.GetObject("cmdPublicitat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdPublicitat.Size = New System.Drawing.Size(23, 23)
		Me.cmdPublicitat.Location = New System.Drawing.Point(566, 420)
		Me.cmdPublicitat.TabIndex = 43
Me.cmdPublicitat.Tag = "FolderSearch"
		Me.cmdPublicitat.Name = "cmdPublicitat"
		cmdFirmes.OcxState = CType(resources.GetObject("cmdFirmes.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdFirmes.Size = New System.Drawing.Size(23, 23)
		Me.cmdFirmes.Location = New System.Drawing.Point(566, 444)
		Me.cmdFirmes.TabIndex = 46
Me.cmdFirmes.Tag = "FolderSearch"
		Me.cmdFirmes.Name = "cmdFirmes"
		cmdSubvencions.OcxState = CType(resources.GetObject("cmdSubvencions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdSubvencions.Size = New System.Drawing.Size(23, 23)
		Me.cmdSubvencions.Location = New System.Drawing.Point(566, 468)
		Me.cmdSubvencions.TabIndex = 48
Me.cmdSubvencions.Tag = "FolderSearch"
		Me.cmdSubvencions.Name = "cmdSubvencions"
		Me.Label12.Text = "Path Subvencions"
		Me.Label12.Size = New System.Drawing.Size(177, 15)
		Me.Label12.Location = New System.Drawing.Point(10, 473)
		Me.Label12.TabIndex = 49
		Me.Label12.Tag = "14"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label12.BackColor = System.Drawing.SystemColors.Control
		Me.Label12.Enabled = True
		Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.UseMnemonic = True
		Me.Label12.Visible = True
		Me.Label12.AutoSize = False
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Name = "Label12"
		Me.Label11.Text = "Path signatures digitals"
		Me.Label11.Size = New System.Drawing.Size(177, 15)
		Me.Label11.Location = New System.Drawing.Point(10, 449)
		Me.Label11.TabIndex = 47
		Me.Label11.Tag = "13"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label11.BackColor = System.Drawing.SystemColors.Control
		Me.Label11.Enabled = True
		Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.UseMnemonic = True
		Me.Label11.Visible = True
		Me.Label11.AutoSize = False
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.Name = "Label11"
		Me.Label10.Text = "Path entitats visitades"
		Me.Label10.Size = New System.Drawing.Size(177, 15)
		Me.Label10.Location = New System.Drawing.Point(10, 401)
		Me.Label10.TabIndex = 45
		Me.Label10.Tag = "11"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.Label9.Text = "Path publicitat"
		Me.Label9.Size = New System.Drawing.Size(177, 15)
		Me.Label9.Location = New System.Drawing.Point(10, 425)
		Me.Label9.TabIndex = 44
		Me.Label9.Tag = "12"
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Line3.BorderColor = System.Drawing.SystemColors.Window
		Me.Line3.X1 = 14
		Me.Line3.X2 = 73
		Me.Line3.Y1 = 499
		Me.Line3.Y2 = 499
		Me.Line3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line3.BorderWidth = 1
		Me.Line3.Visible = True
		Me.Line3.Name = "Line3"
		Me.Label8.Text = "Path fitxa seguretat"
		Me.Label8.Size = New System.Drawing.Size(177, 15)
		Me.Label8.Location = New System.Drawing.Point(10, 377)
		Me.Label8.TabIndex = 41
		Me.Label8.Tag = "10"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label7.Text = "Path comunicats"
		Me.Label7.Size = New System.Drawing.Size(177, 15)
		Me.Label7.Location = New System.Drawing.Point(10, 353)
		Me.Label7.TabIndex = 39
		Me.Label7.Tag = "9"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label6.Text = "Path riscos"
		Me.Label6.Size = New System.Drawing.Size(177, 15)
		Me.Label6.Location = New System.Drawing.Point(10, 329)
		Me.Label6.TabIndex = 37
		Me.Label6.Tag = "8"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.Label5.Text = "Path enquestes"
		Me.Label5.Size = New System.Drawing.Size(177, 15)
		Me.Label5.Location = New System.Drawing.Point(10, 305)
		Me.Label5.TabIndex = 35
		Me.Label5.Tag = "7"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Path cursos"
		Me.Label4.Size = New System.Drawing.Size(177, 15)
		Me.Label4.Location = New System.Drawing.Point(10, 281)
		Me.Label4.TabIndex = 33
		Me.Label4.Tag = "6"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "Path curr�culums"
		Me.Label3.Size = New System.Drawing.Size(177, 15)
		Me.Label3.Location = New System.Drawing.Point(10, 257)
		Me.Label3.TabIndex = 31
		Me.Label3.Tag = "5"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Path comunicacions"
		Me.Label2.Size = New System.Drawing.Size(177, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 233)
		Me.Label2.TabIndex = 29
		Me.Label2.Tag = "4"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Path documentaci�n cursos"
		Me.Label1.Size = New System.Drawing.Size(177, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 209)
		Me.Label1.TabIndex = 27
		Me.Label1.Tag = "3"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 19
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Path ficheros fotografias personal"
		Me.lbl2.Size = New System.Drawing.Size(177, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 185)
		Me.lbl2.TabIndex = 22
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 506)
		Me.lblLock.TabIndex = 24
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 498
		Me.Line1.Y2 = 498
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		CType(Me.cmdSubvencions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdFirmes, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdPublicitat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdEntitats, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbFitxa, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbComunicats, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbRiscos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEnquestes, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbCursos2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.CmbCurriculums, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.CmdComunicacions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdCursos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Command1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.chkTrabajarVinculado, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(GroupBox1)
		Me.Controls.Add(txtSubvencions)
		Me.Controls.Add(txtFirmes)
		Me.Controls.Add(txtPathEntitats)
		Me.Controls.Add(txtPathPublicitat)
		Me.Controls.Add(txtPathFitxa)
		Me.Controls.Add(TxtPathcomunicats)
		Me.Controls.Add(txtPathRiscos)
		Me.Controls.Add(txtPathEnquestes)
		Me.Controls.Add(txtPathCursos2)
		Me.Controls.Add(txtPathCurriculums)
		Me.Controls.Add(txtPathComunicacions)
		Me.Controls.Add(txtPathCursos)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtPathFicheros)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Command1)
		Me.Controls.Add(cmdCursos)
		Me.Controls.Add(CmdComunicacions)
		Me.Controls.Add(CmbCurriculums)
		Me.Controls.Add(cmbCursos2)
		Me.Controls.Add(cmbEnquestes)
		Me.Controls.Add(cmbRiscos)
		Me.Controls.Add(cmbComunicats)
		Me.Controls.Add(cmbFitxa)
		Me.Controls.Add(cmdEntitats)
		Me.Controls.Add(cmdPublicitat)
		Me.Controls.Add(cmdFirmes)
		Me.Controls.Add(cmdSubvencions)
		Me.Controls.Add(Label12)
		Me.Controls.Add(Label11)
		Me.Controls.Add(Label10)
		Me.Controls.Add(Label9)
		Me.ShapeContainer1.Shapes.Add(Line3)
		Me.Controls.Add(Label8)
		Me.Controls.Add(Label7)
		Me.Controls.Add(Label6)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.Controls.Add(ShapeContainer1)
		Me.GroupBox1.Controls.Add(txtCodigoPoblacion)
		Me.GroupBox1.Controls.Add(txtXecuteVerificacion)
		Me.GroupBox1.Controls.Add(Text4)
		Me.GroupBox1.Controls.Add(txtCodigoEmpresaNom)
		Me.GroupBox1.Controls.Add(chkTrabajarVinculado)
		Me.GroupBox1.Controls.Add(cmbProgramaNomina)
		Me.GroupBox1.Controls.Add(Label14)
		Me.GroupBox1.Controls.Add(Label15)
		Me.GroupBox1.Controls.Add(Label30)
		Me.GroupBox1.Controls.Add(Label13)
		Me.GroupBox1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
