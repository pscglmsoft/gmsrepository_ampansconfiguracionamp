Option Strict Off
Option Explicit On
Friend Class frmIndicadors
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub chkDadesDuplicadesEmp_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDadesDuplicadesEmp.GotFocus
		XGotFocus(Me, chkDadesDuplicadesEmp)
	End Sub
	
	Private Sub chkDadesDuplicadesEmp_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDadesDuplicadesEmp.LostFocus
		XLostFocus(Me, chkDadesDuplicadesEmp)
	End Sub
	Private Sub chkNoUsarInf_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNoUsarInf.GotFocus
		XGotFocus(Me, chkNoUsarInf)
	End Sub
	
	Private Sub chkNoUsarInf_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNoUsarInf.LostFocus
		XLostFocus(Me, chkNoUsarInf)
	End Sub
	
	Private Sub chkDesglocSegDada_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDesglocSegDada.GotFocus
		XGotFocus(Me, chkDesglocSegDada)
	End Sub
	
	
	Private Sub chkDesglocSegDada_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDesglocSegDada.LostFocus
		XLostFocus(Me, chkDesglocSegDada)
	End Sub
	
	Private Sub chkTituloPrincipal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTituloPrincipal.GotFocus
		XGotFocus(Me, chkTituloPrincipal)
	End Sub
	
	
	Private Sub chkTituloPrincipal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTituloPrincipal.LostFocus
		XLostFocus(Me, chkTituloPrincipal)
	End Sub
	
	
	Private Sub cmbObjectiu_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbObjectiu.GotFocus
		XGotFocus(Me, cmbObjectiu)
	End Sub
	
	Private Sub cmbObjectiu_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbObjectiu.LostFocus
		XLostFocus(Me, cmbObjectiu)
	End Sub
	
	Private Sub cmbObtencioTotal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbObtencioTotal.GotFocus
		XGotFocus(Me, cmbObtencioTotal)
	End Sub
	
	Private Sub cmbObtencioTotal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbObtencioTotal.LostFocus
		XLostFocus(Me, cmbObtencioTotal)
	End Sub
	
	Private Sub cmbOperadorcalcul_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOperadorcalcul.GotFocus
		XGotFocus(Me, cmbOperadorcalcul)
	End Sub
	Private Sub cmbOperadorcalcul_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOperadorcalcul.LostFocus
		XLostFocus(Me, cmbOperadorcalcul)
	End Sub
	
	
	Private Sub frmIndicadors_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadors_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadors_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmIndicadors_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: Control Name no se pudo resolver porque est� dentro del espacio de nombres gen�rico ActiveControl. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
		If Me.ActiveControl.Name = "txtXecuteObtencioDescripcio" Then
			ControlKey(Me, KeyAscii,  , True)
		Else
			ControlKey(Me, KeyAscii)
		End If
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
	Private Sub txtGrupIndicador_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupIndicador.GotFocus
		XGotFocus(Me, txtGrupIndicador)
	End Sub
	
	Private Sub txtGrupIndicador_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupIndicador.DoubleClick
		ConsultaTaula(Me, txtGrupIndicador)
	End Sub
	
	Private Sub txtGrupIndicador_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupIndicador.LostFocus
		If txtGrupIndicador.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtGrupIndicador)
	End Sub
	
	Private Sub txtGrupIndicador_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtGrupIndicador.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtGrupoIndicadorResp_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupoIndicadorResp.GotFocus
		XGotFocus(Me, txtGrupIndicador)
	End Sub
	
	Private Sub txtGrupoIndicadorResp_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupoIndicadorResp.LostFocus
		XLostFocus(Me, txtGrupIndicador)
	End Sub
	
	Private Sub txtIndicadorPercentatge_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIndicadorPercentatge.GotFocus
		XGotFocus(Me, txtIndicadorPercentatge)
	End Sub
	
	Private Sub txtIndicadorPercentatge_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIndicadorPercentatge.LostFocus
		XLostFocus(Me, txtIndicadorPercentatge)
	End Sub
	
	Private Sub txtNumAgrupInd_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumAgrupInd.GotFocus
		XGotFocus(Me, txtNumAgrupInd)
	End Sub
	Private Sub txtNumAgrupInd_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumAgrupInd.LostFocus
		XLostFocus(Me, txtNumAgrupInd)
	End Sub
	Private Sub cmbTipusIndicador_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusIndicador.GotFocus
		XGotFocus(Me, cmbTipusIndicador)
	End Sub
	
	Private Sub cmbTipusIndicador_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusIndicador.LostFocus
		XLostFocus(Me, cmbTipusIndicador)
	End Sub
	
	Private Sub txtObservacionesIndicador_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesIndicador.GotFocus
		XGotFocus(Me, txtObservacionesIndicador)
	End Sub
	
	Private Sub txtObservacionesIndicador_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesIndicador.LostFocus
		XLostFocus(Me, txtObservacionesIndicador)
	End Sub
	
	Private Sub txtObservacionesParametro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesParametro.GotFocus
		XGotFocus(Me, txtObservacionesParametro)
	End Sub
	
	Private Sub txtObservacionesParametro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesParametro.LostFocus
		XLostFocus(Me, txtObservacionesParametro)
	End Sub
	
	Private Sub txtObservacionesParametro_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacionesParametro.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtPieceContar_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPieceContar.GotFocus
		XGotFocus(Me, txtPieceContar)
	End Sub
	
	Private Sub txtPieceContar_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPieceContar.LostFocus
		XLostFocus(Me, txtPieceContar)
	End Sub
	
	Private Sub txtPieceContar_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPieceContar.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtXecuteObtencioDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXecuteObtencioDescripcio.GotFocus
		XGotFocus(Me, txtXecuteObtencioDescripcio)
	End Sub
	
	Private Sub txtXecuteObtencioDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXecuteObtencioDescripcio.LostFocus
		XLostFocus(Me, txtXecuteObtencioDescripcio)
	End Sub
	
	Private Sub txtXecuteObtencioDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtXecuteObtencioDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtXecuteObtencioDescripcio)
		eventArgs.Cancel = Cancel
	End Sub
End Class
