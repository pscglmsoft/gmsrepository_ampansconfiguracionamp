Option Strict Off
Option Explicit On
Friend Class frmEnvioCorreos
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmbModulo_DropDownClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbModulo.DropDownClosed
		carregaCorreus()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmEnvioCorreos.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmEnvioCorreos_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		carregaCorreus()
	End Sub
	
	Private Sub frmEnvioCorreos_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmEnvioCorreos_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CarregaCombo(Me, "cmbModulo",  ,  ,  , "S WL=$$FiltraModulEnt^ENT.G.AUX0001($P(%REGC,S,1))")
	End Sub
	
	Private Sub carregaCorreus()
		If cmbModulo.Columns(1).Value = "" Then Exit Sub
		CarregaFGrid(grdCorreus, "GridCorreus^ENT.G.AUX0001", (cmbModulo.Columns(1).Value))
	End Sub
	
	Private Sub grdCorreus_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdCorreus.DoubleClick
		ObreFormulari(frmDefinicionCorreos, "frmDefinicionCorreos", cmbModulo.Columns(1).Value & S & grdCorreus.Cell(grdCorreus.ActiveCell.Row, 1).Text, Me.Name)
	End Sub
End Class
