<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEnvioCorreos
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents cmbModulo As DataControl.GmsCombo
	Public WithEvents grdCorreus As FlexCell.Grid
	Public WithEvents lbl1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEnvioCorreos))
		Me.components = New System.ComponentModel.Container()
		Me.cmbModulo = New DataControl.GmsCombo
		Me.grdCorreus = New FlexCell.Grid
		Me.lbl1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True


		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Env�o de correos"
		Me.ClientSize = New System.Drawing.Size(515, 314)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmEnvioCorreos"

		Me.cmbModulo.Size = New System.Drawing.Size(145, 19)
		Me.cmbModulo.Location = New System.Drawing.Point(123, 9)
		Me.cmbModulo.TabIndex = 0
Me.cmbModulo.Tag = "######1###^%GMSPro(GMODUL,""P"",*)#1"
		Me.cmbModulo.Name = "cmbModulo"

		Me.grdCorreus.Size = New System.Drawing.Size(493, 219)
		Me.grdCorreus.Location = New System.Drawing.Point(9, 45)
Me.grdCorreus.BorderStyle = FlexCell.BorderStyleEnum.FixedSingle
		Me.grdCorreus.TabIndex = 2
Me.grdCorreus.Tag = "ENT-IMP#8"
		Me.grdCorreus.Name = "grdCorreus"
		Me.lbl1.Text = "Modulo"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(9, 13)
		Me.lbl1.TabIndex = 1
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"


		Me.Controls.Add(cmbModulo)
		Me.Controls.Add(grdCorreus)
		Me.Controls.Add(lbl1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
