<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAvisoCorreos
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtCentroEmision As System.Windows.Forms.TextBox
	Public WithEvents grdListaD As FlexCell.Grid
	Public WithEvents TabControlPage2 As GmsTabControl.GmsTabPage
	Public WithEvents tvUsuaris As System.Windows.Forms.TreeView
	Public WithEvents TvGrupos As System.Windows.Forms.TreeView
	Public WithEvents TvCentros As System.Windows.Forms.TreeView
	Public WithEvents TabControlPage1 As GmsTabControl.GmsTabPage
	Public WithEvents TabCorreus As DataControl.GmsTabControl
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents cmbModulo As DataControl.GmsCombo
	Public WithEvents chkMarcado As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbCodigoMail As DataControl.GmsCombo
	Public WithEvents ImageList3 As System.Windows.Forms.ImageList
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAvisoCorreos))
		Me.components = New System.ComponentModel.Container()
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtCentroEmision = New System.Windows.Forms.TextBox
		Me.TabCorreus = New DataControl.GmsTabControl
		Me.TabControlPage2 = New GmsTabControl.GmsTabPage
		Me.grdListaD = New FlexCell.Grid
		Me.TabControlPage1 = New GmsTabControl.GmsTabPage
		Me.tvUsuaris = New System.Windows.Forms.TreeView
		Me.TvGrupos = New System.Windows.Forms.TreeView
		Me.TvCentros = New System.Windows.Forms.TreeView
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.cmbModulo = New DataControl.GmsCombo
		Me.chkMarcado = New AxXtremeSuiteControls.AxCheckBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.cmbCodigoMail = New DataControl.GmsCombo
		Me.ImageList3 = New System.Windows.Forms.ImageList
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.TabCorreus.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True

		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabCorreus, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.chkMarcado, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()

		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Aviso correos"
		Me.ClientSize = New System.Drawing.Size(812, 610)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "G-CORREOS_AVISO"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmAvisoCorreos"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(288, 19)
		Me.Text3.Location = New System.Drawing.Point(189, 108)
		Me.Text3.TabIndex = 16
		Me.Text3.Tag = "^5"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtCentroEmision.AutoSize = False
		Me.txtCentroEmision.Size = New System.Drawing.Size(62, 19)
		Me.txtCentroEmision.Location = New System.Drawing.Point(124, 108)
		Me.txtCentroEmision.Maxlength = 6
		Me.txtCentroEmision.TabIndex = 4
		Me.txtCentroEmision.Tag = "5"
		Me.txtCentroEmision.AcceptsReturn = True
		Me.txtCentroEmision.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentroEmision.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentroEmision.CausesValidation = True
		Me.txtCentroEmision.Enabled = True
		Me.txtCentroEmision.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentroEmision.HideSelection = True
		Me.txtCentroEmision.ReadOnly = False
		Me.txtCentroEmision.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentroEmision.MultiLine = False
		Me.txtCentroEmision.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentroEmision.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentroEmision.TabStop = True
		Me.txtCentroEmision.Visible = True
		Me.txtCentroEmision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentroEmision.Name = "txtCentroEmision"

		Me.TabCorreus.Size = New System.Drawing.Size(803, 431)
		Me.TabCorreus.Location = New System.Drawing.Point(6, 136)
		Me.TabCorreus.TabIndex = 12
		Me.TabCorreus.Name = "TabCorreus"

		Me.TabControlPage2.Size = New System.Drawing.Size(799, 407)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 14
Me.TabControlPage2.Text = "Lista distribuci�n"
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"

		Me.grdListaD.Size = New System.Drawing.Size(792, 398)
		Me.grdListaD.Location = New System.Drawing.Point(2, 4)
Me.grdListaD.BorderStyle = FlexCell.BorderStyleEnum.FixedSingle
		Me.grdListaD.TabIndex = 15
Me.grdListaD.Tag = "ENT-IMP#12"
		Me.grdListaD.Name = "grdListaD"

		Me.TabControlPage1.Size = New System.Drawing.Size(799, 407)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 13
Me.TabControlPage1.Text = "Selecci�n destinatarios"
		Me.TabControlPage1.Name = "TabControlPage1"
		Me.tvUsuaris.CausesValidation = True
		Me.tvUsuaris.Size = New System.Drawing.Size(261, 397)
		Me.tvUsuaris.Location = New System.Drawing.Point(4, 4)
		Me.tvUsuaris.TabIndex = 18
		Me.tvUsuaris.HideSelection = False
		Me.tvUsuaris.Indent = 2
		Me.tvUsuaris.LabelEdit = False
		Me.tvUsuaris.Sorted = True
		Me.tvUsuaris.CheckBoxes = True
		Me.tvUsuaris.ImageList = ImageList3
		Me.tvUsuaris.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.tvUsuaris.Name = "tvUsuaris"
		Me.TvGrupos.CausesValidation = True
		Me.TvGrupos.Size = New System.Drawing.Size(261, 397)
		Me.TvGrupos.Location = New System.Drawing.Point(268, 4)
		Me.TvGrupos.TabIndex = 19
		Me.TvGrupos.HideSelection = False
		Me.TvGrupos.Indent = 2
		Me.TvGrupos.LabelEdit = False
		Me.TvGrupos.Sorted = True
		Me.TvGrupos.CheckBoxes = True
		Me.TvGrupos.ImageList = ImageList3
		Me.TvGrupos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.TvGrupos.Name = "TvGrupos"
		Me.TvCentros.CausesValidation = True
		Me.TvCentros.Size = New System.Drawing.Size(261, 397)
		Me.TvCentros.Location = New System.Drawing.Point(532, 4)
		Me.TvCentros.TabIndex = 20
		Me.TvCentros.HideSelection = False
		Me.TvCentros.Indent = 2
		Me.TvCentros.LabelEdit = False
		Me.TvCentros.Sorted = True
		Me.TvCentros.CheckBoxes = True
		Me.TvCentros.ImageList = ImageList3
		Me.TvCentros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.TvCentros.Name = "TvCentros"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(62, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(124, 61)
		Me.txtEmpresa.Maxlength = 6
		Me.txtEmpresa.TabIndex = 2
		Me.txtEmpresa.Tag = "*3"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(288, 19)
		Me.Text1.Location = New System.Drawing.Point(190, 61)
		Me.Text1.TabIndex = 7
		Me.Text1.Tag = "^3"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"

		Me.cmbModulo.Size = New System.Drawing.Size(145, 19)
		Me.cmbModulo.Location = New System.Drawing.Point(124, 11)
		Me.cmbModulo.TabIndex = 0
Me.cmbModulo.Tag = "*1"
		Me.cmbModulo.Name = "cmbModulo"
		chkMarcado.OcxState = CType(resources.GetObject("chkMarcado.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkMarcado.Size = New System.Drawing.Size(127, 19)
		Me.chkMarcado.Location = New System.Drawing.Point(10, 85)
		Me.chkMarcado.TabIndex = 3
Me.chkMarcado.Tag = "4"
		Me.chkMarcado.Name = "chkMarcado"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(631, 581)
		Me.cmdAceptar.TabIndex = 5
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(721, 581)
		Me.cmdGuardar.TabIndex = 10
		Me.cmdGuardar.Name = "cmdGuardar"

		Me.cmbCodigoMail.Size = New System.Drawing.Size(352, 19)
		Me.cmbCodigoMail.Location = New System.Drawing.Point(124, 36)
		Me.cmbCodigoMail.TabIndex = 1
Me.cmbCodigoMail.Tag = "*2"
		Me.cmbCodigoMail.Name = "cmbCodigoMail"
		Me.ImageList3.ImageSize = New System.Drawing.Size(17, 15)
		Me.ImageList3.TransparentColor = System.Drawing.Color.FromARGB(192, 192, 192)
Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList3.Images.SetKeyName(0, "")
		Me.ImageList3.Images.SetKeyName(1, "")
		Me.ImageList3.Images.SetKeyName(2, "")
		Me.ImageList3.Images.SetKeyName(3, "")
		Me.ImageList3.Images.SetKeyName(4, "")
		Me.ImageList3.Images.SetKeyName(5, "")
		Me.ImageList3.Images.SetKeyName(6, "")
		Me.ImageList3.Images.SetKeyName(7, "")
		Me.ImageList3.Images.SetKeyName(8, "")
		Me.ImageList3.Images.SetKeyName(9, "")
		Me.ImageList3.Images.SetKeyName(10, "")
		Me.ImageList3.Images.SetKeyName(11, "")
		Me.ImageList3.Images.SetKeyName(12, "")
		Me.ImageList3.Images.SetKeyName(13, "")
		Me.ImageList3.Images.SetKeyName(14, "")
		Me.ImageList3.Images.SetKeyName(15, "")
		Me.ImageList3.Images.SetKeyName(16, "")
		Me.ImageList3.Images.SetKeyName(17, "")
		Me.ImageList3.Images.SetKeyName(18, "")
		Me.ImageList3.Images.SetKeyName(19, "")
		Me.ImageList3.Images.SetKeyName(20, "")
		Me.ImageList3.Images.SetKeyName(21, "")
		Me.ImageList3.Images.SetKeyName(22, "")
		Me.ImageList3.Images.SetKeyName(23, "")
		Me.ImageList3.Images.SetKeyName(24, "")
		Me.ImageList3.Images.SetKeyName(25, "")
		Me.ImageList3.Images.SetKeyName(26, "")
		Me.ImageList3.Images.SetKeyName(27, "")
		Me.ImageList3.Images.SetKeyName(28, "User")
		Me.ImageList3.Images.SetKeyName(29, "ToDoList")
		Me.ImageList3.Images.SetKeyName(30, "")
		Me.ImageList3.Images.SetKeyName(31, "")
		Me.Label1.Text = "Centro emisi�n"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 110)
		Me.Label1.TabIndex = 17
		Me.Label1.Tag = "5"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 63)
		Me.lbl1.TabIndex = 6
		Me.lbl1.Tag = "3"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Modulo"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 15)
		Me.lbl2.TabIndex = 8
		Me.lbl2.Tag = "1"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "C�digo mail"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 39)
		Me.lbl3.TabIndex = 9
		Me.lbl3.Tag = "2"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 580)
		Me.lblLock.TabIndex = 11
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"

		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkMarcado, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.TabCorreus, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()

		Me.Controls.Add(Text3)
		Me.Controls.Add(txtCentroEmision)
		Me.Controls.Add(TabCorreus)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(cmbModulo)
		Me.Controls.Add(chkMarcado)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(cmbCodigoMail)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lblLock)
		Me.TabCorreus.Controls.Add(TabControlPage2)
		Me.TabCorreus.Controls.Add(TabControlPage1)
		Me.TabControlPage2.Controls.Add(grdListaD)
		Me.TabControlPage1.Controls.Add(tvUsuaris)
		Me.TabControlPage1.Controls.Add(TvGrupos)
		Me.TabControlPage1.Controls.Add(TvCentros)
		Me.TabCorreus.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
