Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmDefinicionCorreos
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic FlagExtern As Boolean
	''OKPublic RegistreExtern As String
	
Overrides 	Sub fGetReg()
		CarregaEmpreses()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	
	
	Private Sub cmbCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCodigo.GotFocus
		XGotFocus(Me, cmbCodigo)
	End Sub
	
	Private Sub cmbCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCodigo.LostFocus
		If cmbCodigo.Columns(1).Value <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, cmbCodigo)
		CarregaEmpreses()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmDefinicionCorreos.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmDefinicionCorreos_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		SituaCombo(cmbModulo, (frmEnvioCorreos.cmbModulo.Columns(1).Value))
		SituaCombo(cmbCodigo, (frmEnvioCorreos.grdCorreus.Cell(frmEnvioCorreos.grdCorreus.ActiveCell.Row, 1).Text))
		CarregaEmpreses()
	End Sub
	
	Private Sub frmDefinicionCorreos_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		'Inici
		Me.Unload()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmDefinicionCorreos_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmbModulo.Enabled = False
		cmbCodigo.Enabled = False
	End Sub
	
	Private Sub frmDefinicionCorreos_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmDefinicionCorreos_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cmbModulo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbModulo.GotFocus
		XGotFocus(Me, cmbModulo)
	End Sub
	
	Private Sub cmbModulo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbModulo.LostFocus
		If cmbModulo.Columns(1).Value <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, cmbModulo)
	End Sub
	
	
	Private Sub grdEmpreses_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdEmpreses.DoubleClick
		'If grdEmpreses.MouseRow < 1 Then Exit Sub
		'If grdEmpreses.MouseCol = 4 Then Exit Sub
		'If grdEmpreses.Cell(grdEmpreses.ActiveCell.Row, 4).Text <> 1 Then Exit Sub
		GravaRegistre(Me)
		With frmAvisoCorreos
			.Empresa = grdEmpreses.Cell(grdEmpreses.ActiveCell.Row, 1).Text
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto cmbModulo.Columns().Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.XModul = cmbModulo.Columns(1).Value
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto cmbCodigo.Columns().Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.cMail = cmbCodigo.Columns(1).Value
		End With
		ObreFormulari(frmAvisoCorreos, "frmAvisoCorreos", cmbModulo.Columns(1).Value & S & cmbCodigo.Columns(1).Value & S & grdEmpreses.Cell(grdEmpreses.ActiveCell.Row, 1).Text, "frmEnvioCorreos")
	End Sub
	
	
	Private Sub txtXecuteMail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXecuteMail.GotFocus
		XGotFocus(Me, txtXecuteMail)
	End Sub
	
	Private Sub txtXecuteMail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXecuteMail.LostFocus
		XLostFocus(Me, txtXecuteMail)
	End Sub
	
	Private Sub txtXecuteMail_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtXecuteMail.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaEmpreses()
		If cmbModulo.Columns(1).Value = "" Then Exit Sub
		If cmbCodigo.Columns(1).Value = "" Then Exit Sub
		CarregaFGrid(grdEmpreses, "GridEmpreses^ENT.G.AUX0001", cmbModulo.Columns(1).Value & S & cmbCodigo.Columns(1).Value)
	End Sub
	
Overrides 	Sub DeleteReg()
		DelReg(Me)
		Me.Unload()
	End Sub
End Class
