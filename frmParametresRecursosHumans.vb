Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParametresRecursosHumans
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub chkTrabajarVinculado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTrabajarVinculado.GotFocus
		XGotFocus(Me, chkTrabajarVinculado)
	End Sub
	
	Private Sub chkTrabajarVinculado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTrabajarVinculado.LostFocus
		XLostFocus(Me, chkTrabajarVinculado)
	End Sub
	
	Private Sub chkTrabajarVinculado_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTrabajarVinculado.ClickEvent
		If chkTrabajarVinculado.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			cmbProgramaNomina.Enabled = True
			txtCodigoEmpresaNom.Enabled = True
			txtXecuteVerificacion.Enabled = True
			txtCodigoPoblacion.Enabled = True
		Else
			cmbProgramaNomina.Value = ""
			txtCodigoEmpresaNom.Text = ""
			txtXecuteVerificacion.Text = ""
			txtCodigoPoblacion.Text = ""
			cmbProgramaNomina.Enabled = False
			txtCodigoEmpresaNom.Enabled = False
			txtCodigoPoblacion.Enabled = False
			txtXecuteVerificacion.Enabled = False
		End If
	End Sub
	
	Private Sub cmbProgramaNomina_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbProgramaNomina.GotFocus
		XGotFocus(Me, cmbProgramaNomina)
	End Sub
	
	
	Private Sub cmbProgramaNomina_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbProgramaNomina.LostFocus
		XLostFocus(Me, cmbProgramaNomina)
	End Sub
	
	Private Sub cmdSubvencions_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSubvencions.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtSubvencions.Text, "Buscar carpeta", "Camino de ficheros de subvenciones")
		If a <> "" Then
			txtSubvencions.Text = a
		End If
	End Sub
	
	Private Sub cmbComunicats_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbComunicats.Click
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino de ficheros de comunicats")
		If a <> "" Then
			TxtPathcomunicats.Text = a
		End If
	End Sub
	
	Private Sub CmbCurriculums_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmbCurriculums.Click
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino de ficheros de curr�culums")
		If a <> "" Then
			txtPathCurriculums.Text = a
		End If
	End Sub
	
	Private Sub cmbCursos2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCursos2.Click
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino de ficheros de cursos")
		If a <> "" Then
			txtPathCursos2.Text = a
		End If
	End Sub
	
	Private Sub cmbEnquestes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEnquestes.Click
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino de ficheros de encuestas")
		If a <> "" Then
			txtPathEnquestes.Text = a
		End If
	End Sub
	
	Private Sub cmdFirmes_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFirmes.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtFirmes.Text, "Buscar carpeta", "Camino de ficheros de firmas digitales")
		If a <> "" Then txtFirmes.Text = a
	End Sub
	
	Private Sub cmbFitxa_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFitxa.Click
		Dim a As String
		a = ArbreDirectoris(txtPathFitxa.Text, "Buscar carpeta", "Camino de ficheros de fichas de seguridad")
		If a <> "" Then
			txtPathFitxa.Text = a
		End If
	End Sub
	
	Private Sub cmbRiscos_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbRiscos.Click
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino de ficheros de riscos")
		If a <> "" Then
			txtPathRiscos.Text = a
		End If
	End Sub
	
	Private Sub CmdComunicacions_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdComunicacions.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino de ficheros de comunicaciones")
		If a <> "" Then
			txtPathComunicacions.Text = a
		End If
	End Sub
	
	Private Sub cmdCursos_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCursos.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino documentaci�n cursos personal")
		If a <> "" Then
			txtPathCursos.Text = a
		End If
	End Sub
	
	Private Sub cmdEntitats_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEntitats.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathEntitats.Text, "Buscar carpeta", "Camino de ficheros de entidades visitadas")
		If a <> "" Then
			txtPathEntitats.Text = a
		End If
	End Sub
	
	Private Sub cmdPublicitat_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPublicitat.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathPublicitat.Text, "Buscar carpeta", "Camino de ficheros de publicidad")
		If a <> "" Then
			txtPathPublicitat.Text = a
		End If
	End Sub
	
	Private Sub Command1_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command1.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathFicheros.Text, "Buscar carpeta", "Camino de ficheros fotos personal")
		If a <> "" Then
			txtPathFicheros.Text = a
		End If
	End Sub
	
	Private Sub frmParametresRecursosHumans_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		
		If chkTrabajarVinculado.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			If cmbProgramaNomina.Columns(1).Value = "" Then
				xMsgBox("Falta informar el programa de n�mina", MsgBoxStyle.Information, Me.Text)
				cmbProgramaNomina.Focus()
				Exit Sub
			ElseIf txtCodigoEmpresaNom.Text = "" Then 
				xMsgBox("Falta informar el codigo empresa de n�mina", MsgBoxStyle.Information, Me.Text)
				txtCodigoEmpresaNom.Focus()
				Exit Sub
			End If
		End If
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		ResetForm(Me)
	End Sub
	
	Private Sub frmParametresRecursosHumans_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmbProgramaNomina.Enabled = False
		txtCodigoEmpresaNom.Enabled = False
	End Sub
	
	Private Sub frmParametresRecursosHumans_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParametresRecursosHumans_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	
	Private Sub txtCodigoPoblacion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPoblacion.DoubleClick
		ConsultaTaula(Me, txtCodigoPoblacion)
	End Sub
	
	Private Sub txtCodigoPoblacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPoblacion.GotFocus
		XGotFocus(Me, txtCodigoPoblacion)
	End Sub
	
	Private Sub txtCodigoPoblacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPoblacion.LostFocus
		XLostFocus(Me, txtCodigoPoblacion)
	End Sub
	
	Private Sub txtCodigoPoblacion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigoPoblacion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtPathComunicacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathComunicacions.GotFocus
		XGotFocus(Me, txtPathComunicacions)
	End Sub
	
	Private Sub txtPathComunicacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathComunicacions.LostFocus
		XLostFocus(Me, txtPathComunicacions)
	End Sub
	
	Private Sub TxtPathcomunicats_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TxtPathcomunicats.GotFocus
		XGotFocus(Me, TxtPathcomunicats)
	End Sub
	
	Private Sub TxtPathcomunicats_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TxtPathcomunicats.LostFocus
		XLostFocus(Me, TxtPathcomunicats)
	End Sub
	
	Private Sub TxtPathcomunicats_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles TxtPathcomunicats.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPathCurriculums_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathCurriculums.GotFocus
		XGotFocus(Me, txtPathCurriculums)
	End Sub
	
	Private Sub txtPathCurriculums_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathCurriculums.LostFocus
		XLostFocus(Me, txtPathCurriculums)
	End Sub
	
	Private Sub txtPathEnquestes_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathEnquestes.GotFocus
		XGotFocus(Me, txtPathEnquestes)
	End Sub
	
	Private Sub txtPathEnquestes_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathEnquestes.LostFocus
		XLostFocus(Me, txtPathEnquestes)
	End Sub
	
	Private Sub txtPathFicheros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFicheros.GotFocus
		XGotFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathFicheros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFicheros.LostFocus
		Call XLostFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathCursos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathCursos.GotFocus
		XGotFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathCursos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathCursos.LostFocus
		Call XLostFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathFitxa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFitxa.GotFocus
		XGotFocus(Me, txtPathFitxa)
	End Sub
	
	Private Sub txtPathFitxa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFitxa.LostFocus
		XLostFocus(Me, txtPathFitxa)
	End Sub
	
	Private Sub txtPathRiscos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathRiscos.GotFocus
		XGotFocus(Me, txtPathRiscos)
	End Sub
	
	Private Sub txtPathRiscos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathRiscos.LostFocus
		XLostFocus(Me, txtPathRiscos)
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If chkTrabajarVinculado.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			If cmbProgramaNomina.Columns(1).Value = "" Then
				xMsgBox("Falta informar el programa de n�mina", MsgBoxStyle.Information, Me.Text)
				cmbProgramaNomina.Focus()
				Exit Sub
			ElseIf txtCodigoEmpresaNom.Text = "" Then 
				xMsgBox("Falta informar el codigo empresa de n�mina", MsgBoxStyle.Information, Me.Text)
				txtCodigoEmpresaNom.Focus()
				Exit Sub
			End If
		End If
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	
	
	Private Sub txtXecuteVerificacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXecuteVerificacion.GotFocus
		XGotFocus(Me, txtXecuteVerificacion)
	End Sub
	
	Private Sub txtXecuteVerificacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXecuteVerificacion.LostFocus
		XLostFocus(Me, txtXecuteVerificacion)
	End Sub
End Class
