<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FrmLlistatMails
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents cmbModulo As DataControl.GmsCombo
	Public WithEvents cmbCodigoMail As DataControl.GmsCombo
	Public WithEvents chkVerExcluidos As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents GroupBox1 As DataControl.GmsGroupBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdCancelar As AxXtremeSuiteControls.AxPushButton
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmLlistatMails))
		Me.components = New System.ComponentModel.Container()
		Me.GroupBox1 = New DataControl.GmsGroupBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.cmbModulo = New DataControl.GmsCombo
		Me.cmbCodigoMail = New DataControl.GmsCombo
		Me.chkVerExcluidos = New AxXtremeSuiteControls.AxCheckBox
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdCancelar = New AxXtremeSuiteControls.AxPushButton
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True


		CType(Me.chkVerExcluidos, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Listado e-mails"
		Me.ClientSize = New System.Drawing.Size(495, 166)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "FrmLlistatMails"

		Me.GroupBox1.Size = New System.Drawing.Size(479, 113)
		Me.GroupBox1.Location = New System.Drawing.Point(6, 8)
Me.GroupBox1.BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder
		Me.GroupBox1.TabIndex = 5
		Me.GroupBox1.Name = "GroupBox1"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(272, 19)
		Me.Text1.Location = New System.Drawing.Point(189, 62)
		Me.Text1.TabIndex = 7
		Me.Text1.Tag = "^3"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(62, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(124, 62)
		Me.txtEmpresa.Maxlength = 6
		Me.txtEmpresa.TabIndex = 2
		Me.txtEmpresa.Tag = "3####G-EMPRESAS#1#1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"

		Me.cmbModulo.Size = New System.Drawing.Size(145, 19)
		Me.cmbModulo.Location = New System.Drawing.Point(124, 12)
		Me.cmbModulo.TabIndex = 0
Me.cmbModulo.Tag = "######1###^%GMSPro(GMODUL,""P"",*)#1"
		Me.cmbModulo.Name = "cmbModulo"

		Me.cmbCodigoMail.Size = New System.Drawing.Size(338, 19)
		Me.cmbCodigoMail.Location = New System.Drawing.Point(124, 37)
		Me.cmbCodigoMail.TabIndex = 1
Me.cmbCodigoMail.Tag = "######1###^WCOMBO(MOD1,100,*)#1"
		Me.cmbCodigoMail.Name = "cmbCodigoMail"
		chkVerExcluidos.OcxState = CType(resources.GetObject("chkVerExcluidos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkVerExcluidos.Size = New System.Drawing.Size(169, 23)
		Me.chkVerExcluidos.Location = New System.Drawing.Point(10, 84)
		Me.chkVerExcluidos.TabIndex = 3
Me.chkVerExcluidos.Tag = "30"
		Me.chkVerExcluidos.Name = "chkVerExcluidos"
		Me.lbl3.Text = "C�digo mail"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 40)
		Me.lbl3.TabIndex = 10
		Me.lbl3.Tag = "2"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl2.Text = "Modulo"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 16)
		Me.lbl2.TabIndex = 9
		Me.lbl2.Tag = "1"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 64)
		Me.lbl1.TabIndex = 8
		Me.lbl1.Tag = "3"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(316, 130)
		Me.cmdAceptar.TabIndex = 4
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdCancelar.OcxState = CType(resources.GetObject("cmdCancelar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCancelar.Size = New System.Drawing.Size(83, 27)
		Me.cmdCancelar.Location = New System.Drawing.Point(404, 130)
		Me.cmdCancelar.TabIndex = 6
		Me.cmdCancelar.Name = "cmdCancelar"
		CType(Me.cmdCancelar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkVerExcluidos, System.ComponentModel.ISupportInitialize).EndInit()


		Me.Controls.Add(GroupBox1)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdCancelar)
		Me.GroupBox1.Controls.Add(Text1)
		Me.GroupBox1.Controls.Add(txtEmpresa)
		Me.GroupBox1.Controls.Add(cmbModulo)
		Me.GroupBox1.Controls.Add(cmbCodigoMail)
		Me.GroupBox1.Controls.Add(chkVerExcluidos)
		Me.GroupBox1.Controls.Add(lbl3)
		Me.GroupBox1.Controls.Add(lbl2)
		Me.GroupBox1.Controls.Add(lbl1)
		Me.GroupBox1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
