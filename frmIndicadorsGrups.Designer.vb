<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadorsGrups
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtNumeroParam As DataControl.GmsImports
	Public WithEvents _txtParametro_4 As System.Windows.Forms.TextBox
	Public WithEvents _txtParametro_3 As System.Windows.Forms.TextBox
	Public WithEvents _txtParametro_2 As System.Windows.Forms.TextBox
	Public WithEvents _txtParametro_1 As System.Windows.Forms.TextBox
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcio As System.Windows.Forms.TextBox
	Public WithEvents txtRutina As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmbModul As DataControl.GmsCombo
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents _Label_4 As System.Windows.Forms.Label
	Public WithEvents _Label_3 As System.Windows.Forms.Label
	Public WithEvents _Label_2 As System.Windows.Forms.Label
	Public WithEvents _Label_1 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Label As GmsLabelArray
	Public WithEvents txtParametro As GmsTextBoxArray
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadorsGrups))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtNumeroParam = New DataControl.GmsImports
		Me._txtParametro_4 = New System.Windows.Forms.TextBox
		Me._txtParametro_3 = New System.Windows.Forms.TextBox
		Me._txtParametro_2 = New System.Windows.Forms.TextBox
		Me._txtParametro_1 = New System.Windows.Forms.TextBox
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtDescripcio = New System.Windows.Forms.TextBox
		Me.txtRutina = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.cmbModul = New DataControl.GmsCombo
		Me.Label6 = New System.Windows.Forms.Label
		Me._Label_4 = New System.Windows.Forms.Label
		Me._Label_3 = New System.Windows.Forms.Label
		Me._Label_2 = New System.Windows.Forms.Label
		Me._Label_1 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Label = New GmsLabelArray()
		Me.txtParametro = New GmsTextBoxArray()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtNumeroParam, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtParametro, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Grup d'indicadors"
		Me.ClientSize = New System.Drawing.Size(449, 271)
		Me.Location = New System.Drawing.Point(313, 310)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORS GRUPS WAP"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadorsGrups"

		Me.txtNumeroParam.Size = New System.Drawing.Size(61, 19)
		Me.txtNumeroParam.Location = New System.Drawing.Point(124, 108)
		Me.txtNumeroParam.TabIndex = 7
Me.txtNumeroParam.Tag = "9"
		Me.txtNumeroParam.Name = "txtNumeroParam"
		Me._txtParametro_4.AutoSize = False
		Me._txtParametro_4.Size = New System.Drawing.Size(311, 19)
		Me._txtParametro_4.Location = New System.Drawing.Point(124, 202)
		Me._txtParametro_4.Maxlength = 40
		Me._txtParametro_4.TabIndex = 11
		Me._txtParametro_4.Tag = "8"
		Me._txtParametro_4.AcceptsReturn = True
		Me._txtParametro_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._txtParametro_4.BackColor = System.Drawing.SystemColors.Window
		Me._txtParametro_4.CausesValidation = True
		Me._txtParametro_4.Enabled = True
		Me._txtParametro_4.ForeColor = System.Drawing.SystemColors.WindowText
		Me._txtParametro_4.HideSelection = True
		Me._txtParametro_4.ReadOnly = False
		Me._txtParametro_4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._txtParametro_4.MultiLine = False
		Me._txtParametro_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._txtParametro_4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._txtParametro_4.TabStop = True
		Me._txtParametro_4.Visible = True
		Me._txtParametro_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me._txtParametro_4.Name = "_txtParametro_4"
		Me._txtParametro_3.AutoSize = False
		Me._txtParametro_3.Size = New System.Drawing.Size(311, 19)
		Me._txtParametro_3.Location = New System.Drawing.Point(124, 178)
		Me._txtParametro_3.Maxlength = 40
		Me._txtParametro_3.TabIndex = 10
		Me._txtParametro_3.Tag = "7"
		Me._txtParametro_3.AcceptsReturn = True
		Me._txtParametro_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._txtParametro_3.BackColor = System.Drawing.SystemColors.Window
		Me._txtParametro_3.CausesValidation = True
		Me._txtParametro_3.Enabled = True
		Me._txtParametro_3.ForeColor = System.Drawing.SystemColors.WindowText
		Me._txtParametro_3.HideSelection = True
		Me._txtParametro_3.ReadOnly = False
		Me._txtParametro_3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._txtParametro_3.MultiLine = False
		Me._txtParametro_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._txtParametro_3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._txtParametro_3.TabStop = True
		Me._txtParametro_3.Visible = True
		Me._txtParametro_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me._txtParametro_3.Name = "_txtParametro_3"
		Me._txtParametro_2.AutoSize = False
		Me._txtParametro_2.Size = New System.Drawing.Size(311, 19)
		Me._txtParametro_2.Location = New System.Drawing.Point(124, 154)
		Me._txtParametro_2.Maxlength = 40
		Me._txtParametro_2.TabIndex = 9
		Me._txtParametro_2.Tag = "6"
		Me._txtParametro_2.AcceptsReturn = True
		Me._txtParametro_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._txtParametro_2.BackColor = System.Drawing.SystemColors.Window
		Me._txtParametro_2.CausesValidation = True
		Me._txtParametro_2.Enabled = True
		Me._txtParametro_2.ForeColor = System.Drawing.SystemColors.WindowText
		Me._txtParametro_2.HideSelection = True
		Me._txtParametro_2.ReadOnly = False
		Me._txtParametro_2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._txtParametro_2.MultiLine = False
		Me._txtParametro_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._txtParametro_2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._txtParametro_2.TabStop = True
		Me._txtParametro_2.Visible = True
		Me._txtParametro_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me._txtParametro_2.Name = "_txtParametro_2"
		Me._txtParametro_1.AutoSize = False
		Me._txtParametro_1.Size = New System.Drawing.Size(311, 19)
		Me._txtParametro_1.Location = New System.Drawing.Point(124, 130)
		Me._txtParametro_1.Maxlength = 40
		Me._txtParametro_1.TabIndex = 8
		Me._txtParametro_1.Tag = "5"
		Me._txtParametro_1.AcceptsReturn = True
		Me._txtParametro_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._txtParametro_1.BackColor = System.Drawing.SystemColors.Window
		Me._txtParametro_1.CausesValidation = True
		Me._txtParametro_1.Enabled = True
		Me._txtParametro_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._txtParametro_1.HideSelection = True
		Me._txtParametro_1.ReadOnly = False
		Me._txtParametro_1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._txtParametro_1.MultiLine = False
		Me._txtParametro_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._txtParametro_1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._txtParametro_1.TabStop = True
		Me._txtParametro_1.Visible = True
		Me._txtParametro_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me._txtParametro_1.Name = "_txtParametro_1"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(31, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 11)
		Me.txtCodi.Maxlength = 3
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtDescripcio.AutoSize = False
		Me.txtDescripcio.Size = New System.Drawing.Size(310, 19)
		Me.txtDescripcio.Location = New System.Drawing.Point(124, 35)
		Me.txtDescripcio.Maxlength = 30
		Me.txtDescripcio.TabIndex = 3
		Me.txtDescripcio.Tag = "2"
		Me.txtDescripcio.AcceptsReturn = True
		Me.txtDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcio.CausesValidation = True
		Me.txtDescripcio.Enabled = True
		Me.txtDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcio.HideSelection = True
		Me.txtDescripcio.ReadOnly = False
		Me.txtDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcio.MultiLine = False
		Me.txtDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcio.TabStop = True
		Me.txtDescripcio.Visible = True
		Me.txtDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcio.Name = "txtDescripcio"
		Me.txtRutina.AutoSize = False
		Me.txtRutina.Size = New System.Drawing.Size(181, 19)
		Me.txtRutina.Location = New System.Drawing.Point(124, 85)
		Me.txtRutina.Maxlength = 40
		Me.txtRutina.TabIndex = 6
		Me.txtRutina.Tag = "3"
		Me.txtRutina.AcceptsReturn = True
		Me.txtRutina.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtRutina.BackColor = System.Drawing.SystemColors.Window
		Me.txtRutina.CausesValidation = True
		Me.txtRutina.Enabled = True
		Me.txtRutina.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtRutina.HideSelection = True
		Me.txtRutina.ReadOnly = False
		Me.txtRutina.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtRutina.MultiLine = False
		Me.txtRutina.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtRutina.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtRutina.TabStop = True
		Me.txtRutina.Visible = True
		Me.txtRutina.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtRutina.Name = "txtRutina"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(265, 237)
		Me.cmdAceptar.TabIndex = 12
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(355, 237)
		Me.cmdGuardar.TabIndex = 13
		Me.cmdGuardar.Name = "cmdGuardar"

		Me.cmbModul.Size = New System.Drawing.Size(145, 19)
		Me.cmbModul.Location = New System.Drawing.Point(124, 60)
		Me.cmbModul.TabIndex = 4
Me.cmbModul.Tag = "4"
		Me.cmbModul.Name = "cmbModul"
		Me.Label6.Text = "N�mero de par�metros"
		Me.Label6.Size = New System.Drawing.Size(111, 15)
		Me.Label6.Location = New System.Drawing.Point(10, 110)
		Me.Label6.TabIndex = 20
		Me.Label6.Tag = "9"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me._Label_4.Text = "Par�metro 4"
		Me._Label_4.Size = New System.Drawing.Size(111, 15)
		Me._Label_4.Location = New System.Drawing.Point(10, 206)
		Me._Label_4.TabIndex = 19
		Me._Label_4.Tag = "8"
		Me._Label_4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label_4.BackColor = System.Drawing.SystemColors.Control
		Me._Label_4.Enabled = True
		Me._Label_4.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label_4.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label_4.UseMnemonic = True
		Me._Label_4.Visible = True
		Me._Label_4.AutoSize = False
		Me._Label_4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label_4.Name = "_Label_4"
		Me._Label_3.Text = "Par�metro 3"
		Me._Label_3.Size = New System.Drawing.Size(111, 15)
		Me._Label_3.Location = New System.Drawing.Point(10, 182)
		Me._Label_3.TabIndex = 18
		Me._Label_3.Tag = "7"
		Me._Label_3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label_3.BackColor = System.Drawing.SystemColors.Control
		Me._Label_3.Enabled = True
		Me._Label_3.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label_3.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label_3.UseMnemonic = True
		Me._Label_3.Visible = True
		Me._Label_3.AutoSize = False
		Me._Label_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label_3.Name = "_Label_3"
		Me._Label_2.Text = "Par�metro 2"
		Me._Label_2.Size = New System.Drawing.Size(111, 15)
		Me._Label_2.Location = New System.Drawing.Point(10, 158)
		Me._Label_2.TabIndex = 17
		Me._Label_2.Tag = "6"
		Me._Label_2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label_2.BackColor = System.Drawing.SystemColors.Control
		Me._Label_2.Enabled = True
		Me._Label_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label_2.UseMnemonic = True
		Me._Label_2.Visible = True
		Me._Label_2.AutoSize = False
		Me._Label_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label_2.Name = "_Label_2"
		Me._Label_1.Text = "Par�metro 1"
		Me._Label_1.Size = New System.Drawing.Size(111, 15)
		Me._Label_1.Location = New System.Drawing.Point(10, 134)
		Me._Label_1.TabIndex = 16
		Me._Label_1.Tag = "5"
		Me._Label_1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label_1.BackColor = System.Drawing.SystemColors.Control
		Me._Label_1.Enabled = True
		Me._Label_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label_1.UseMnemonic = True
		Me._Label_1.Visible = True
		Me._Label_1.AutoSize = False
		Me._Label_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label_1.Name = "_Label_1"
		Me.Label1.Text = "M�dul"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 64)
		Me.Label1.TabIndex = 15
		Me.Label1.Tag = "4"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 39)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Rutina"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 89)
		Me.lbl3.TabIndex = 5
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 236)
		Me.lblLock.TabIndex = 14
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 230
		Me.Line1.Y2 = 230
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 231
		Me.Line2.Y2 = 231
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"








		CType(Me.txtParametro, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtNumeroParam, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtNumeroParam)
		Me.Controls.Add(_txtParametro_4)
		Me.Controls.Add(_txtParametro_3)
		Me.Controls.Add(_txtParametro_2)
		Me.Controls.Add(_txtParametro_1)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtDescripcio)
		Me.Controls.Add(txtRutina)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(cmbModul)
		Me.Controls.Add(Label6)
		Me.Controls.Add(_Label_4)
		Me.Controls.Add(_Label_3)
		Me.Controls.Add(_Label_2)
		Me.Controls.Add(_Label_1)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
