<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadorsCentros
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtArea As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtActivitat As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtCentro As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcio As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadorsCentros))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtArea = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtActivitat = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtCentro = New System.Windows.Forms.TextBox
		Me.txtDescripcio = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Indicadores de centros por actividad y area"
		Me.ClientSize = New System.Drawing.Size(460, 157)
		Me.Location = New System.Drawing.Point(510, 170)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORS AREES/ACT/CE"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadorsCentros"
		Me.txtArea.AutoSize = False
		Me.txtArea.Size = New System.Drawing.Size(31, 19)
		Me.txtArea.Location = New System.Drawing.Point(124, 11)
		Me.txtArea.Maxlength = 3
		Me.txtArea.TabIndex = 1
		Me.txtArea.Tag = "*1"
		Me.txtArea.AcceptsReturn = True
		Me.txtArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtArea.BackColor = System.Drawing.SystemColors.Window
		Me.txtArea.CausesValidation = True
		Me.txtArea.Enabled = True
		Me.txtArea.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtArea.HideSelection = True
		Me.txtArea.ReadOnly = False
		Me.txtArea.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtArea.MultiLine = False
		Me.txtArea.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtArea.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtArea.TabStop = True
		Me.txtArea.Visible = True
		Me.txtArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtArea.Name = "txtArea"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(259, 19)
		Me.Text1.Location = New System.Drawing.Point(159, 11)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtActivitat.AutoSize = False
		Me.txtActivitat.Size = New System.Drawing.Size(31, 19)
		Me.txtActivitat.Location = New System.Drawing.Point(124, 35)
		Me.txtActivitat.Maxlength = 3
		Me.txtActivitat.TabIndex = 4
		Me.txtActivitat.Tag = "*2"
		Me.txtActivitat.AcceptsReturn = True
		Me.txtActivitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtActivitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtActivitat.CausesValidation = True
		Me.txtActivitat.Enabled = True
		Me.txtActivitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtActivitat.HideSelection = True
		Me.txtActivitat.ReadOnly = False
		Me.txtActivitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtActivitat.MultiLine = False
		Me.txtActivitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtActivitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtActivitat.TabStop = True
		Me.txtActivitat.Visible = True
		Me.txtActivitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtActivitat.Name = "txtActivitat"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(259, 19)
		Me.Text2.Location = New System.Drawing.Point(159, 35)
		Me.Text2.TabIndex = 5
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtCentro.AutoSize = False
		Me.txtCentro.Size = New System.Drawing.Size(31, 19)
		Me.txtCentro.Location = New System.Drawing.Point(124, 59)
		Me.txtCentro.Maxlength = 3
		Me.txtCentro.TabIndex = 7
		Me.txtCentro.Tag = "*3"
		Me.txtCentro.AcceptsReturn = True
		Me.txtCentro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentro.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentro.CausesValidation = True
		Me.txtCentro.Enabled = True
		Me.txtCentro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentro.HideSelection = True
		Me.txtCentro.ReadOnly = False
		Me.txtCentro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentro.MultiLine = False
		Me.txtCentro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentro.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentro.TabStop = True
		Me.txtCentro.Visible = True
		Me.txtCentro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentro.Name = "txtCentro"
		Me.txtDescripcio.AutoSize = False
		Me.txtDescripcio.Size = New System.Drawing.Size(310, 19)
		Me.txtDescripcio.Location = New System.Drawing.Point(124, 83)
		Me.txtDescripcio.Maxlength = 30
		Me.txtDescripcio.TabIndex = 9
		Me.txtDescripcio.Tag = "4"
		Me.txtDescripcio.AcceptsReturn = True
		Me.txtDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcio.CausesValidation = True
		Me.txtDescripcio.Enabled = True
		Me.txtDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcio.HideSelection = True
		Me.txtDescripcio.ReadOnly = False
		Me.txtDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcio.MultiLine = False
		Me.txtDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcio.TabStop = True
		Me.txtDescripcio.Visible = True
		Me.txtDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcio.Name = "txtDescripcio"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(259, 121)
		Me.cmdAceptar.TabIndex = 10
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(349, 121)
		Me.cmdGuardar.TabIndex = 11
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Area"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 15)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Activitat"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 39)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Centro"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 63)
		Me.lbl3.TabIndex = 6
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Descripci�"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 87)
		Me.lbl4.TabIndex = 8
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 120)
		Me.lblLock.TabIndex = 12
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 114
		Me.Line1.Y2 = 114
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 115
		Me.Line2.Y2 = 115
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtArea)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtActivitat)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtCentro)
		Me.Controls.Add(txtDescripcio)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
