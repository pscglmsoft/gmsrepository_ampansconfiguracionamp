Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParametresUsuaris
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdCursos_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCursos.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathCursos.Text, "Buscar carpeta", "Camino documentaci�n cursos personal")
		If a <> "" Then
			txtPathCursos.Text = a
		End If
	End Sub
	
	Private Sub Command1_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command1.ClickEvent
		Dim a As String
		a = ArbreDirectoris(txtPathFicheros.Text, "Buscar carpeta", "Camino de ficheros fotos personal")
		If a <> "" Then
			txtPathFicheros.Text = a
		End If
	End Sub
	
	'UPGRADE_WARNING: Form evento frmParametresUsuaris.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmParametresUsuaris_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		XGotFocusForm(Me)
	End Sub
	
	Private Sub frmParametresUsuaris_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		ResetForm(Me)
	End Sub
	
	Private Sub frmParametresUsuaris_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmParametresUsuaris_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParametresUsuaris_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Label2_Click()
		
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
		mySendKeys()
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtPathFicheros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFicheros.GotFocus
		XGotFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathFicheros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathFicheros.LostFocus
		Call XLostFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathCursos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathCursos.GotFocus
		XGotFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub txtPathCursos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPathCursos.LostFocus
		Call XLostFocus(Me, txtPathFicheros)
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
End Class
