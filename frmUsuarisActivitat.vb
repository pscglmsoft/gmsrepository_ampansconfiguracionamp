Option Strict Off
Option Explicit On
Friend Class frmUsuarisActivitat
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	Private Sub DefinirGrid()
		IniciFGrid(grdActivitats)
		
		With grdActivitats
			.Cols = 5
			.Rows = 1
			.FixedRowColStyle = FlexCell.FixedRowColStyleEnum.Flat
			.AllowUserReorderColumn = False
			.AllowUserResizing = True
			.AllowUserSort = True
			.FixedRows = 1
			.FixedRowColStyle = FlexCell.FixedRowColStyleEnum.Flat
			.ExtendLastCol = True
			
			.Cell(0, 1).Text = "" '
			.Cell(0, 2).Text = "Codi" '
			.Cell(0, 3).Text = "Activitat"
			.Cell(0, 4).Text = "Perm�s"
			
			.Column(2).Alignment = AlignmentEnum.LeftCenter
			.Column(3).Alignment = AlignmentEnum.LeftCenter
			.Column(4).Alignment = AlignmentEnum.CenterCenter
			
			.Column(2).CellType = FlexCell.CellTypeEnum.TextBox
			.Column(3).CellType = FlexCell.CellTypeEnum.TextBox
			.Column(4).CellType = FlexCell.CellTypeEnum.CheckBox
			
			.Column(2).Mask = MaskEnum.Numeric
			.Column(2).FormatString = "##"
			.Column(2).DecimalLength = 0
			
			.Column(0).Width = 0
			.Column(1).Width = 0
			.Column(2).Width = 70
			.Column(3).Width = 357
			.Column(4).Width = 50
			
		End With
		
		
		FGAplicaIdioma(grdActivitats)
	End Sub
	Private Sub grdActivitats_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdActivitats.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdActivitats", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Marca totes",  ,  , XPIcon("Checked"),  ,  ,  ,  , "M")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Desmarca totes",  ,  , XPIcon("UnChecked"),  ,  ,  ,  , "D")
			End With
		End With
		XpExecutaMenu(Me, "grdActivitats", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub grdActivitats_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdActivitats.Click
		If grdActivitats.MouseRow < 1 Then Exit Sub
		MarcaRowFG(grdActivitats)
	End Sub
	
	Private Sub grdActivitats_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdActivitats.DoubleClick
		If grdActivitats.MouseRow < 1 Then Exit Sub
		If grdActivitats.ActiveCell.Row < 1 Then Exit Sub
		If grdActivitats.Cell(grdActivitats.ActiveCell.Row, 4).Text = "1" Then
			grdActivitats.Cell(grdActivitats.ActiveCell.Row, 4).Text = "0"
		Else
			grdActivitats.Cell(grdActivitats.ActiveCell.Row, 4).Text = "1"
		End If
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim a As String
		Dim I As Short
		
		Select Case NomMenu
			Case "grdActivitats"
				Select Case KeyMenu
					Case "M"
						For I = 1 To grdActivitats.Rows - 1
							grdActivitats.Cell(I, 4).Text = CStr(1)
						Next I
					Case "D"
						For I = 1 To grdActivitats.Rows - 1
							grdActivitats.Cell(I, 4).Text = CStr(0)
						Next I
				End Select
		End Select
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If Guardar() = False Then Exit Sub
		Inici()
	End Sub
	
	Private Sub frmUsuarisActivitat_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuarisActivitat_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmUsuarisActivitat_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		Text2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
		DefinirGrid()
	End Sub
	
	Public Overrides Sub Inici()
		'grdCentres.RemoveAll
		txtEmpresa.Enabled = True
		txtUsuari.Enabled = True
		ResetForm(Me)
	End Sub
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		If txtEmpresa.Text <> "" And txtUsuari.Text <> "" Then
			txtEmpresa.Enabled = False
			txtUsuari.Enabled = False
			CarregaActivitats()
		End If
	End Sub
	Private Sub txtUsuari_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.DoubleClick
		ConsultaTaula(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtUsuari_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.LostFocus
		DisplayDescripcio(Me, txtUsuari)
		If txtUsuari.Text <> "" And txtEmpresa.Text <> "" Then
			txtUsuari.Enabled = False
			txtEmpresa.Enabled = False
			CarregaActivitats()
		End If
	End Sub
	
	Private Sub CarregaActivitats()
		Dim I As Short
		CarregaFGrid(grdActivitats, "ACT2^ESEGUR", txtUsuari.Text & S & txtEmpresa.Text, CStr(2))
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If Guardar() = False Then
			Exit Sub
		End If
	End Sub
	
	Private Function Guardar() As Boolean
		Dim I As Short
		Dim Nod As String
		Guardar = False
		If txtUsuari.Text <> "" Then
			MCache.PLIST = ""
			MCache.PDELIM = Chr(13)
			Nod = ""
			For I = 1 To grdActivitats.Rows - 1
				If Nod <> "" Then Nod = Nod & Chr(13)
				Nod = Nod & grdActivitats.Cell(I, 2).Text & S & grdActivitats.Cell(I, 4).Text
			Next I
			MCache.PLIST = Nod
			MCache.P1 = txtUsuari.Text
			MCache.P2 = txtEmpresa.Text
			CacheXecute("D GACT2^ESEGUR")
		End If
		Guardar = True
	End Function
End Class
