Option Strict Off
Option Explicit On
Friend Class MDI
	Inherits FormParent
	Dim mbMoving As Boolean
	Const sglSplitLimit As Short = 1000
	Public LastNode As System.Windows.Forms.TreeNode
	
	Public Sub ExecutaMenu(ByRef NomMenu As String)
		frmMenus.Enabled = False
		Select Case NomMenu
			' (CONTADORS) Contadores
			Case "frmContadorsAplicacions" : frmContadorsAplicacions.Show() : frmContadorsAplicacions.Activate()
				' (CORREOS) Correos
			Case "frmEnvioCorreos" : frmEnvioCorreos.Show() : frmEnvioCorreos.Activate()
			Case "FrmLlistatMails" : FrmLlistatMails.Show() : FrmLlistatMails.Activate()
				' (DEPURACIONS) Depuraciones
				' (EMPRESAS) Empresas
			Case "frmEmpresasGeneral" : frmEmpresasGeneral.Show() : frmEmpresasGeneral.Activate()
			Case "frmEmpresas" : frmEmpresas.Show() : frmEmpresas.Activate()
			Case "frmSistemasGestion" : frmSistemasGestion.Show() : frmSistemasGestion.Activate()
				' (ERP) Par�metros CET
			Case "frmParametresERP" : frmParametresERP.Show() : frmParametresERP.Activate()
			Case "frmParImputCEE" : frmParImputCEE.Show() : frmParImputCEE.Activate()
				' (FINANCES) Enlla� amb finances
				' (GD) Assignar classes documents
			Case "frmGDClasificacioDocsArbre" : frmGDClasificacioDocsArbre.Show() : frmGDClasificacioDocsArbre.Activate()
				' (GENERALES) Par�metros generales
			Case "frmParametrosSistema" : frmParametrosSistema.Show() : frmParametrosSistema.Activate()
			Case "frmParametrosAplicacion" : frmParametrosAplicacion.Show() : frmParametrosAplicacion.Activate()
			Case "frmImatgesEmpresas" : frmImatgesEmpresas.Show() : frmImatgesEmpresas.Activate()
			Case "frmImatgesFondos" : frmImatgesFondos.Show() : frmImatgesFondos.Activate()
			Case "frmAplicacionsAuditoria" : frmAplicacionsAuditoria.Show() : frmAplicacionsAuditoria.Activate()
				' (GMS) GMS
			Case "frmCarregaDadesComuns" : frmCarregaDadesComuns.Show() : frmCarregaDadesComuns.Activate()
			Case "frmCargaValoresDefecto" : frmCargaValoresDefecto.Show() : frmCargaValoresDefecto.Activate()
				' (IMPRESORAS) Impresoras
			Case "frmModelosImp" : frmModelosImp.Show() : frmModelosImp.Activate()
			Case "frmImpresoresSis" : frmImpresoresSis.Show() : frmImpresoresSis.Activate()
				' (IND) Indicadores
			Case "frmIndicadorsGrups" : frmIndicadorsGrups.Show() : frmIndicadorsGrups.Activate()
			Case "frmIndicadors" : frmIndicadors.Show() : frmIndicadors.Activate()
				' (INDICADORS) Indicadors
			Case "frmIndicadoresParametrizacionGrupos" : frmIndicadoresParametrizacionGrupos.Show() : frmIndicadoresParametrizacionGrupos.Activate()
			Case "frmIndicadoresParametrosVis" : frmIndicadoresParametrosVis.Show() : frmIndicadoresParametrosVis.Activate()
			Case "frmIndicadoresParametrizacion" : frmIndicadoresParametrizacion.Show() : frmIndicadoresParametrizacion.Activate()
				' (INDICADORS ARBRE) Arbol estructura
			Case "frmIndicadorsArees" : frmIndicadorsArees.Show() : frmIndicadorsArees.Activate()
			Case "frmIndicadorsDeActivitats" : frmIndicadorsDeActivitats.Show() : frmIndicadorsDeActivitats.Activate()
			Case "frmIndicadorsCentros" : frmIndicadorsCentros.Show() : frmIndicadorsCentros.Activate()
			Case "frmNivellsEstructura" : frmNivellsEstructura.Show() : frmNivellsEstructura.Activate()
			Case "frmNivellsEstructuraSeguretat" : frmNivellsEstructuraSeguretat.Show() : frmNivellsEstructuraSeguretat.Activate()
				' (MARK) Par�metros relaciones p�blicas
			Case "frmGeneracioXecs" : frmGeneracioXecs.Show() : frmGeneracioXecs.Activate()
			Case "frmParametrosSocios" : frmParametrosSocios.Show() : frmParametrosSocios.Activate()
				' (MENU) Configuraci�n
				' (PARAM PERSONAS) Par�metros personas
			Case "frmParametresUsuaris" : frmParametresUsuaris.Show() : frmParametresUsuaris.Activate()
			Case "frmParametrosActivitades" : frmParametrosActivitades.Show() : frmParametrosActivitades.Activate()
				' (PARAMETROS) Par�metros
				' (REGENERACIONS) Reordenaciones
			Case "frmRegeneracionsApl" : frmRegeneracionsApl.Show() : frmRegeneracionsApl.Activate()
				' (RH) Recursos humanos
			Case "frmParametresRecursosHumans" : frmParametresRecursosHumans.Show() : frmParametresRecursosHumans.Activate()
			Case "frmParametrosDietas" : frmParametrosDietas.Show() : frmParametrosDietas.Activate()
				' (TABLASGEN) Tablas generales
				' (USUARIOS) Usuarios
			Case "frmUsuarios" : frmUsuarios.Show() : frmUsuarios.Activate()
			Case "frmUsuarisPerColaboradors" : frmUsuarisPerColaboradors.Show() : frmUsuarisPerColaboradors.Activate()
			Case "frmGruposUsuarios" : frmGruposUsuarios.Show() : frmGruposUsuarios.Activate()
			Case "frmMenusGrups" : frmMenusGrups.Show() : frmMenusGrups.Activate()
			Case "frmUsuarisActivitat" : frmUsuarisActivitat.Show() : frmUsuarisActivitat.Activate()
			Case "frmUsuarisCentre" : frmUsuarisCentre.Show() : frmUsuarisCentre.Activate()
			Case "frmParamtresVaris" : frmParamtresVaris.Show() : frmParamtresVaris.Activate()
				' (USUARIOS ENTI) Usuarios
			Case "frmPermisosEntitatsUsuaris" : frmPermisosEntitatsUsuaris.Show() : frmPermisosEntitatsUsuaris.Activate()
			Case "frmPermisosEntitatsUsuarisGrups" : frmPermisosEntitatsUsuarisGrups.Show() : frmPermisosEntitatsUsuarisGrups.Activate()
				' (USUDOCU) Documentaci�n usuarios
			Case "frmPermisosDocumentacioUsuari" : frmPermisosDocumentacioUsuari.Show() : frmPermisosDocumentacioUsuari.Activate()
			Case "frmPermisosDocumentacioModul" : frmPermisosDocumentacioModul.Show() : frmPermisosDocumentacioModul.Activate()
		End Select
		frmMenus.Enabled = True
	End Sub
	
	Private Sub CommandBars_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsEvents_ExecuteEvent) Handles CommandBars.Execute
		ExecutaBotonsToolBarAmpans(CStr(Control.ID), Me.ActiveMDIChild)
	End Sub
	
	'UPGRADE_WARNING: Form evento MDI.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub MDI_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		frmSplash.Unload()
		Cursor = System.Windows.Forms.Cursors.Default
	End Sub
	
	Private Sub MDI_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		Modul = "CONFIGURADOR"
		'Modul = "ENT-IMP"
		'GrupModuls = "ENT"
		GrupModuls = "AMP" '"ENT"
		NoTeEmpresa = True
		
		Splash()
		MCache = CacheCub
		'Set Ocache = New CacheObject.Factory
		Wsock = New Net.Sockets.Socket(Net.Sockets.AddressFamily.InterNetwork, Net.Sockets.SocketType.Stream, Net.Sockets.ProtocolType.Tcp)
		PopMenuXp = New gmsPopUpCode.gmsPopUp
		
		Estil.Versio = e_Versio.Estil2007
		CarregaEstil()
		CarregaVariablesRegistre()
		If Conecta(NoTeEmpresa) = False Then
			Me.Unload()
			Exit Sub
		End If
		AmpliaToolbar()
		'CacheXecute "D INI^IINICI"
		
	End Sub
	
	'UPGRADE_ISSUE: El evento Form MDIForm.MouseDown no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub MDI_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles MyBase.MouseDown
		If Button = MouseButtons.Right Then MDIPopupVentana()
	End Sub
	
	'UPGRADE_ISSUE: El evento Form MDIForm.MouseMove no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub MDI_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles MyBase.MouseMove
		TempsActual = Date.Now.TimeOfDay.TotalSeconds
	End Sub
	
	'UPGRADE_WARNING: El evento MDI.Resize se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub MDI_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
		MDIResize()
	End Sub
	
	Private Sub MDI_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		DescarregaFormularis()
Application.Exit() : End    ''MIGRAT
	End Sub
	
	Private Sub TimerDesconexio_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TimerDesconexio.Tick
		ControlDesconexio()
	End Sub
	
	Private Sub TimerFireWall_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TimerFireWall.Tick
		TimeFireWallControl()
	End Sub
	
	Private Sub CacheCub_OnError(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CacheCub.OnError
		ErrorCache()
	End Sub
End Class
