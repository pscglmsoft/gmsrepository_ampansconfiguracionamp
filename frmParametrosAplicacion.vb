Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmParametrosAplicacion
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub chkPermitirNIFRepetidos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermitirNIFRepetidos.GotFocus
		XGotFocus(Me, chkPermitirNIFRepetidos)
	End Sub
	
	Private Sub chkPermitirNIFRepetidos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermitirNIFRepetidos.LostFocus
		XLostFocus(Me, chkPermitirNIFRepetidos)
	End Sub
	
	Private Sub chkVerificarNifs_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVerificarNifs.GotFocus
		XGotFocus(Me, chkVerificarNifs)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmParametrosAplicacion.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmParametrosAplicacion_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		ABM = GetReg(Me)
	End Sub
	
	Private Sub frmParametrosAplicacion_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Me.Unload()
	End Sub
	
	Private Sub frmParametrosAplicacion_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		txtConstant1.Text = CStr(1)
	End Sub
	
	Private Sub frmParametrosAplicacion_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Public Overrides Sub DeleteReg()
		xMsgBox("No es posible eliminar este par�metro de implantaci�n", MsgBoxStyle.Information, Me.Text)
	End Sub
	
	Private Sub frmParametrosAplicacion_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodigoPais_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPais.GotFocus
		XGotFocus(Me, txtCodigoPais)
	End Sub
	
	Private Sub txtCodigoPais_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPais.LostFocus
		Call XLostFocus(Me, txtCodigoPais)
	End Sub
	
	Private Sub txtConstant1_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtConstant1.GotFocus
		XGotFocus(Me, txtConstant1)
		If txtConstant1.Text <> "" Then
			mySendKeys("{tab}")
		End If
	End Sub
	
	Private Sub txtConstant1_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtConstant1.DoubleClick
		ConsultaTaula(Me, txtConstant1)
	End Sub
	
	Private Sub txtConstant1_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtConstant1.LostFocus
		ABM = GetReg(Me)
		Call XLostFocus(Me, txtConstant1)
	End Sub
	
	Private Sub txtIVAServeis_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIVAServeis.DoubleClick
		ConsultaTaula(Me, txtIVAServeis)
	End Sub
	
	Private Sub txtIVAServeis_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIVAServeis.GotFocus
		XGotFocus(Me, txtIVAServeis)
	End Sub
	
	Private Sub txtIVAServeis_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIVAServeis.LostFocus
		Call XLostFocus(Me, txtIVAServeis)
	End Sub
	
	Private Sub txtRetencion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRetencion.GotFocus
		XGotFocus(Me, txtRetencion)
	End Sub
	
	Private Sub txtRetencion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRetencion.LostFocus
		Call XLostFocus(Me, txtRetencion)
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
End Class
