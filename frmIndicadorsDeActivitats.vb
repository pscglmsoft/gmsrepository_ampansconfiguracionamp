Option Strict Off
Option Explicit On
Friend Class frmIndicadorsDeActivitats
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	Public Area As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub FGetReg()
		DisplayCentres()
	End Sub
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		MouReg(Me, Accio,  , Area)
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then Me.Unload()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmIndicadorsDeActivitats.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmIndicadorsDeActivitats_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		DisplayCentres()
	End Sub
	
	Private Sub frmIndicadorsDeActivitats_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		Area = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadorsDeActivitats_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		txtArea.Text = Area
	End Sub
	
	Private Sub frmIndicadorsDeActivitats_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmIndicadorsDeActivitats_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub grdCentres_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdCentres.DoubleClick
		If grdCentres.ActiveCell.Row < 1 Then Exit Sub
		With frmIndicadorsCentros
			.Area = txtArea.Text
			.Activitat = txtActivitat.Text
			.FlagExtern = True
			.txtCentro.Text = grdCentres.Cell(grdCentres.ActiveCell.Row, 1).Text
			.Show()
			.ABM = GetReg(frmIndicadorsCentros)
		End With
	End Sub
	
	Private Sub grdCentres_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdCentres.MouseUp
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdCentres", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Modificar",  , grdCentres.ActiveCell.Row > 0, XPIcon("Edit"),  ,  ,  ,  , "Mod")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou",  , txtActivitat.Text <> "", XPIcon("New"),  ,  ,  ,  , "New")
			End With
		End With
		XpExecutaMenu(Me, "grdCentres")
	End Sub
	
	Private Sub txtArea_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArea.GotFocus
		XGotFocus(Me, txtArea)
	End Sub
	
	Private Sub txtArea_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArea.DoubleClick
		ConsultaTaula(Me, txtArea)
	End Sub
	
	Private Sub txtArea_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArea.LostFocus
		If txtArea.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtArea)
	End Sub
	
	Private Sub txtArea_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtArea.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtActivitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActivitat.GotFocus
		XGotFocus(Me, txtActivitat)
	End Sub
	
	Private Sub txtActivitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActivitat.LostFocus
		If txtActivitat.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtActivitat)
	End Sub
	
	Private Sub txtActivitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtActivitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub DisplayCentres()
		CarregaFGrid(grdCentres, "CGAAC^ENT.G.INDIAUX", txtArea.Text & S & txtActivitat.Text)
	End Sub
	
	'UPGRADE_NOTE: Menu se actualiz� a Menu_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub ResMenu(ByRef Menu As String, ByRef Key As String)
		Select Case Menu
			Case "grdCentres"
				Select Case Key
					Case "Mod"
grdCentres_DoubleClick(grdCentres, New System.EventArgs())
					Case "New"
						With frmIndicadorsCentros
							.Area = txtArea.Text
							.Activitat = txtActivitat.Text
							.FlagExtern = True
							.Show()
							.txtArea.Enabled = False
							.txtActivitat.Enabled = False
						End With
				End Select
		End Select
	End Sub
End Class
