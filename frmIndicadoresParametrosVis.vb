Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmIndicadoresParametrosVis
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub chkRequiereRecalculo_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRequiereRecalculo.ClickEvent
		If txtGrupoIndicadores.Text <> "" And txtIndicador.Text <> "" Then
			If chkRequiereRecalculo.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
				lblRecalcul.Text = "Una modificación en los parámetros de este indicador requiere recálculo"
			Else
				lblRecalcul.Text = "Una modificación en los parámetros de este indicador NO requiere recálculo"
			End If
		End If
	End Sub
	
	Private Sub chkRequiereRecalculo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRequiereRecalculo.GotFocus
		XGotFocus(Me, chkRequiereRecalculo)
	End Sub
	
	Private Sub chkRequiereRecalculo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRequiereRecalculo.LostFocus
		XLostFocus(Me, chkRequiereRecalculo)
	End Sub
	
	Private Sub frmIndicadoresParametrosVis_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadoresParametrosVis_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadoresParametrosVis_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmIndicadoresParametrosVis_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtGrupoIndicadores_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupoIndicadores.GotFocus
		XGotFocus(Me, txtGrupoIndicadores)
	End Sub
	
	Private Sub txtGrupoIndicadores_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupoIndicadores.DoubleClick
		ConsultaTaula(Me, txtGrupoIndicadores)
	End Sub
	
	Private Sub txtGrupoIndicadores_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrupoIndicadores.LostFocus
		If txtGrupoIndicadores.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtGrupoIndicadores)
	End Sub
	
	Private Sub txtGrupoIndicadores_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtGrupoIndicadores.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtIndicador_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIndicador.GotFocus
		XGotFocus(Me, txtIndicador)
	End Sub
	
	Private Sub txtIndicador_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIndicador.DoubleClick
		ConsultaTaula(Me, txtIndicador,  , txtGrupoIndicadores.Text)
	End Sub
	
	Private Sub txtIndicador_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIndicador.LostFocus
		If txtIndicador.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtIndicador)
	End Sub
	
	Private Sub txtIndicador_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtIndicador.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtGrupoIndicadores.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtParametro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro.GotFocus
		XGotFocus(Me, txtParametro)
	End Sub
	
	Private Sub txtParametro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParametro.LostFocus
		XLostFocus(Me, txtParametro)
	End Sub
	
	Private Sub txtParametro_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtParametro.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacionesParametro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesParametro.GotFocus
		XGotFocus(Me, txtObservacionesParametro)
	End Sub
	
	Private Sub txtObservacionesParametro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesParametro.LostFocus
		XLostFocus(Me, txtObservacionesParametro)
	End Sub
	
	Private Sub txtObservacionesParametro_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacionesParametro.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
