<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmIndicadoresParametrizacion
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtGrupoIndicadores As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcionIndicador As System.Windows.Forms.TextBox
	Public WithEvents txtObservacionesIndicador As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmIndicadoresParametrizacion))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtGrupoIndicadores = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtDescripcionIndicador = New System.Windows.Forms.TextBox
		Me.txtObservacionesIndicador = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Parametrizaci�n indicadores"
		Me.ClientSize = New System.Drawing.Size(705, 223)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-INDICADORES PARAMETRIZACION"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmIndicadoresParametrizacion"
		Me.txtGrupoIndicadores.AutoSize = False
		Me.txtGrupoIndicadores.Size = New System.Drawing.Size(31, 19)
		Me.txtGrupoIndicadores.Location = New System.Drawing.Point(130, 21)
		Me.txtGrupoIndicadores.Maxlength = 3
		Me.txtGrupoIndicadores.TabIndex = 1
		Me.txtGrupoIndicadores.Tag = "*1"
		Me.txtGrupoIndicadores.AcceptsReturn = True
		Me.txtGrupoIndicadores.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGrupoIndicadores.BackColor = System.Drawing.SystemColors.Window
		Me.txtGrupoIndicadores.CausesValidation = True
		Me.txtGrupoIndicadores.Enabled = True
		Me.txtGrupoIndicadores.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGrupoIndicadores.HideSelection = True
		Me.txtGrupoIndicadores.ReadOnly = False
		Me.txtGrupoIndicadores.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGrupoIndicadores.MultiLine = False
		Me.txtGrupoIndicadores.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGrupoIndicadores.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGrupoIndicadores.TabStop = True
		Me.txtGrupoIndicadores.Visible = True
		Me.txtGrupoIndicadores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGrupoIndicadores.Name = "txtGrupoIndicadores"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(528, 19)
		Me.Text1.Location = New System.Drawing.Point(164, 21)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(31, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(130, 45)
		Me.txtCodigo.Maxlength = 3
		Me.txtCodigo.TabIndex = 4
		Me.txtCodigo.Tag = "*2"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(528, 19)
		Me.Text2.Location = New System.Drawing.Point(164, 45)
		Me.Text2.TabIndex = 5
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtDescripcionIndicador.AutoSize = False
		Me.txtDescripcionIndicador.Size = New System.Drawing.Size(563, 19)
		Me.txtDescripcionIndicador.Location = New System.Drawing.Point(130, 79)
		Me.txtDescripcionIndicador.Maxlength = 80
		Me.txtDescripcionIndicador.TabIndex = 7
		Me.txtDescripcionIndicador.Tag = "3"
		Me.txtDescripcionIndicador.AcceptsReturn = True
		Me.txtDescripcionIndicador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcionIndicador.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcionIndicador.CausesValidation = True
		Me.txtDescripcionIndicador.Enabled = True
		Me.txtDescripcionIndicador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcionIndicador.HideSelection = True
		Me.txtDescripcionIndicador.ReadOnly = False
		Me.txtDescripcionIndicador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcionIndicador.MultiLine = False
		Me.txtDescripcionIndicador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcionIndicador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcionIndicador.TabStop = True
		Me.txtDescripcionIndicador.Visible = True
		Me.txtDescripcionIndicador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcionIndicador.Name = "txtDescripcionIndicador"
		Me.txtObservacionesIndicador.AutoSize = False
		Me.txtObservacionesIndicador.Size = New System.Drawing.Size(563, 69)
		Me.txtObservacionesIndicador.Location = New System.Drawing.Point(130, 103)
		Me.txtObservacionesIndicador.Maxlength = 250
		Me.txtObservacionesIndicador.MultiLine = True
		Me.txtObservacionesIndicador.TabIndex = 9
		Me.txtObservacionesIndicador.Tag = "4"
		Me.txtObservacionesIndicador.AcceptsReturn = True
		Me.txtObservacionesIndicador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesIndicador.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesIndicador.CausesValidation = True
		Me.txtObservacionesIndicador.Enabled = True
		Me.txtObservacionesIndicador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesIndicador.HideSelection = True
		Me.txtObservacionesIndicador.ReadOnly = False
		Me.txtObservacionesIndicador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesIndicador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesIndicador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesIndicador.TabStop = True
		Me.txtObservacionesIndicador.Visible = True
		Me.txtObservacionesIndicador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesIndicador.Name = "txtObservacionesIndicador"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(521, 189)
		Me.cmdAceptar.TabIndex = 10
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(611, 189)
		Me.cmdGuardar.TabIndex = 11
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Grupo indicadores"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(6, 25)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "C�digo"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(6, 49)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Descripci�n indicador"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(6, 83)
		Me.lbl3.TabIndex = 6
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Observaciones indicador"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(6, 107)
		Me.lbl4.TabIndex = 8
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 188)
		Me.lblLock.TabIndex = 12
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 182
		Me.Line1.Y2 = 182
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 183
		Me.Line2.Y2 = 183
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtGrupoIndicadores)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtDescripcionIndicador)
		Me.Controls.Add(txtObservacionesIndicador)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
