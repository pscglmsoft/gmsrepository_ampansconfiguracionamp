Option Strict Off
Option Explicit On
Friend Class FrmLlistatMails
	Inherits FormParent
	Dim Opcions As String
	''OKPublic FlagConsulta As Boolean
	
	
	Private Sub chkVerExcluidos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVerExcluidos.GotFocus
		If chkVerExcluidos.Value = XtremeSuiteControls.CheckBoxValue.xtpGrayed Then chkVerExcluidos.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
	End Sub
	
	Private Sub cmbModulo_DropDownClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbModulo.DropDownClosed
		Dim ModuloC As String
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto cmbModulo.Columns().Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ModuloC = cmbModulo.Columns(1).Value
		If ModuloC = "" Then Exit Sub
		CarregaComboNou(Me, cmbCodigoMail, "S MOD1=" & C & ModuloC & C,  , e_SinContenido.Excloure)
		'CarregaComboNou Me, cmbCodigoMail, "S MOD1=" & C & ModuloC & C, , Excloure, "I '$D(^WCOMBO(MOD1,66,$P(%REGC,S,1))) S WL=1"
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If Permis(Me, e_Permisos.Impresions) = False Then Exit Sub
		If cmbModulo.Columns(1).Value = "" Then
			xMsgBox("Debe informarse el m�dulo", MsgBoxStyle.Information, Me.Text)
			cmbModulo.Focus()
			Exit Sub
		End If
		If cmbCodigoMail.Columns(1).Value = "" Then
			xMsgBox("Debe informarse el tipo de e-mail", MsgBoxStyle.Information, Me.Text)
			cmbCodigoMail.Focus()
			Exit Sub
		End If
		If txtEmpresa.Text = "" Then
			xMsgBox("Debe informarse la empresa", MsgBoxStyle.Information, Me.Text)
			txtEmpresa.Focus()
			Exit Sub
		End If
		If chkVerExcluidos.Value = XtremeSuiteControls.CheckBoxValue.xtpGrayed Then
			xMsgBox("Debe informarse se si desea o no listar los usuarios excluidos", MsgBoxStyle.Information, Me.Text)
			chkVerExcluidos.Focus()
			Exit Sub
		End If
		
		ImpresioPDF("GCORREUS", cmbModulo.Columns(1).Value & S & cmbCodigoMail.Columns(1).Value & S & txtEmpresa.Text & S & chkVerExcluidos.Value)
		
	End Sub
	
	Private Sub cmdCancelar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelar.ClickEvent
		Me.Unload()
	End Sub
	
	Private Sub FrmLlistatMails_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub FrmLlistatMails_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub FrmLlistatMails_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		IniciForm(Me)
		CarregaComboNou(Me, cmbModulo,  ,  ,  , "S WL=$$FiltraModulEnt^ENT.G.AUX0001($P(%REGC,S,1))")
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		eventArgs.Cancel = Cancel
	End Sub
End Class
