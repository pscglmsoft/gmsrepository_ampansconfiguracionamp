<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmNivellsEstructura
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents cmdCarregar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents grdCentresCost As FlexCell.Grid
	Public WithEvents trvEstructura As System.Windows.Forms.TreeView
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents cmdCerrar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents ImageList3 As System.Windows.Forms.ImageList
	Public WithEvents cmbNivellFinal As DataControl.GmsCombo
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmNivellsEstructura))
		Me.components = New System.ComponentModel.Container()
		Me.cmdCarregar = New AxXtremeSuiteControls.AxPushButton
		Me.grdCentresCost = New FlexCell.Grid
		Me.trvEstructura = New System.Windows.Forms.TreeView
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.cmdCerrar = New AxXtremeSuiteControls.AxPushButton
		Me.ImageList3 = New System.Windows.Forms.ImageList
		Me.cmbNivellFinal = New DataControl.GmsCombo
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdCarregar, System.ComponentModel.ISupportInitialize).BeginInit()

		CType(Me.cmdCerrar, System.ComponentModel.ISupportInitialize).BeginInit()

		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Niveles de estructura"
		Me.ClientSize = New System.Drawing.Size(790, 523)
		Me.Location = New System.Drawing.Point(317, 380)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmNivellsEstructura"
		cmdCarregar.OcxState = CType(resources.GetObject("cmdCarregar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCarregar.Size = New System.Drawing.Size(27, 27)
		Me.cmdCarregar.Location = New System.Drawing.Point(744, 8)
		Me.cmdCarregar.TabIndex = 2
		Me.cmdCarregar.Name = "cmdCarregar"

		Me.grdCentresCost.Size = New System.Drawing.Size(449, 434)
		Me.grdCentresCost.Location = New System.Drawing.Point(324, 44)
Me.grdCentresCost.BorderStyle = FlexCell.BorderStyleEnum.FixedSingle
		Me.grdCentresCost.TabIndex = 6
		Me.grdCentresCost.Name = "grdCentresCost"
		Me.trvEstructura.CausesValidation = True
		Me.trvEstructura.Size = New System.Drawing.Size(287, 433)
		Me.trvEstructura.Location = New System.Drawing.Point(20, 44)
		Me.trvEstructura.TabIndex = 5
		Me.trvEstructura.HideSelection = False
		Me.trvEstructura.Indent = 2
		Me.trvEstructura.LabelEdit = False
		Me.trvEstructura.ImageList = ImageList3
		Me.trvEstructura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.trvEstructura.Name = "trvEstructura"
		Me.Text2.AutoSize = False
		Me.Text2.Enabled = False
		Me.Text2.Size = New System.Drawing.Size(267, 19)
		Me.Text2.Location = New System.Drawing.Point(172, 12)
		Me.Text2.TabIndex = 4
		Me.Text2.Tag = "^1"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.BackColor = System.Drawing.SystemColors.Window
		Me.Text2.CausesValidation = True
		Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(61, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(108, 12)
		Me.txtEmpresa.TabIndex = 0
		Me.txtEmpresa.Tag = "1####G-EMPRESAS#1#1#"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Maxlength = 0
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		cmdCerrar.OcxState = CType(resources.GetObject("cmdCerrar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCerrar.Size = New System.Drawing.Size(83, 27)
		Me.cmdCerrar.Location = New System.Drawing.Point(690, 486)
		Me.cmdCerrar.TabIndex = 7
		Me.cmdCerrar.Name = "cmdCerrar"
		Me.ImageList3.ImageSize = New System.Drawing.Size(17, 15)
		Me.ImageList3.TransparentColor = System.Drawing.Color.FromARGB(192, 192, 192)
Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList3.ImageStream = CType(resources.GetObject("ImageList3.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList3.Images.SetKeyName(0, "")
		Me.ImageList3.Images.SetKeyName(1, "")
		Me.ImageList3.Images.SetKeyName(2, "")
		Me.ImageList3.Images.SetKeyName(3, "")
		Me.ImageList3.Images.SetKeyName(4, "")
		Me.ImageList3.Images.SetKeyName(5, "")
		Me.ImageList3.Images.SetKeyName(6, "")

		Me.cmbNivellFinal.Size = New System.Drawing.Size(145, 19)
		Me.cmbNivellFinal.Location = New System.Drawing.Point(562, 12)
		Me.cmbNivellFinal.TabIndex = 1
Me.cmbNivellFinal.Tag = "######1###^WCOMBO(""ENT-IMP"",2,*)#1"
		Me.cmbNivellFinal.Name = "cmbNivellFinal"
		Me.Label2.Text = "Nivel Final"
		Me.Label2.Size = New System.Drawing.Size(77, 15)
		Me.Label2.Location = New System.Drawing.Point(482, 14)
		Me.Label2.TabIndex = 8
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Empresa"
		Me.Label1.Size = New System.Drawing.Size(87, 15)
		Me.Label1.Location = New System.Drawing.Point(16, 14)
		Me.Label1.TabIndex = 3
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"

		CType(Me.cmdCerrar, System.ComponentModel.ISupportInitialize).EndInit()

		CType(Me.cmdCarregar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(cmdCarregar)
		Me.Controls.Add(grdCentresCost)
		Me.Controls.Add(trvEstructura)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(cmdCerrar)
		Me.Controls.Add(cmbNivellFinal)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
