Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmIndicadorsCentros
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	Public Area As String
	Public Activitat As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then Me.Unload()
	End Sub
	
	Private Sub frmIndicadorsCentros_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		FlagExtern = False
		Area = ""
		Activitat = ""
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmIndicadorsCentros_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		txtArea.Text = Area
		txtActivitat.Text = Activitat
	End Sub
	
	Private Sub frmIndicadorsCentros_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmIndicadorsCentros_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtArea_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArea.GotFocus
		XGotFocus(Me, txtArea)
	End Sub
	
	Private Sub txtArea_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArea.DoubleClick
		ConsultaTaula(Me, txtArea)
	End Sub
	
	Private Sub txtArea_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtArea.LostFocus
		If txtArea.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtArea)
	End Sub
	
	Private Sub txtArea_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtArea.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtActivitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActivitat.GotFocus
		XGotFocus(Me, txtActivitat)
	End Sub
	
	Private Sub txtActivitat_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActivitat.DoubleClick
		ConsultaTaula(Me, txtActivitat,  , txtArea.Text)
	End Sub
	
	Private Sub txtActivitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtActivitat.LostFocus
		If txtActivitat.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtActivitat)
	End Sub
	
	Private Sub txtActivitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtActivitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtArea.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentro.GotFocus
		XGotFocus(Me, txtCentro)
	End Sub
	
	Private Sub txtCentro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentro.LostFocus
		If txtCentro.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCentro)
	End Sub
	
	Private Sub txtCentro_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentro.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
