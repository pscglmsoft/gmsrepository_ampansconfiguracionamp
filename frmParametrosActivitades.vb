Option Strict Off
Option Explicit On
Friend Class frmParametrosActivitades
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub frmParametrosActivitades_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmParametrosActivitades_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmParametrosActivitades_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmParametrosActivitades_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		If txtEmpresa.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtHoraInicial_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraInicial.GotFocus
		XGotFocus(Me, txtHoraInicial)
	End Sub
	
	Private Sub txtHoraInicial_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraInicial.LostFocus
		XLostFocus(Me, txtHoraInicial)
	End Sub
	
	Private Sub txtHoraFinal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraFinal.GotFocus
		XGotFocus(Me, txtHoraFinal)
	End Sub
	
	Private Sub txtHoraFinal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraFinal.LostFocus
		XLostFocus(Me, txtHoraFinal)
	End Sub
	
	Private Sub txtMinutosIntervalo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMinutosIntervalo.GotFocus
		XGotFocus(Me, txtMinutosIntervalo)
	End Sub
	
	Private Sub txtMinutosIntervalo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMinutosIntervalo.LostFocus
		XLostFocus(Me, txtMinutosIntervalo)
	End Sub
End Class
