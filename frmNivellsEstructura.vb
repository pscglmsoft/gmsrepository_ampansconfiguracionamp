Option Strict Off
Option Explicit On
Friend Class frmNivellsEstructura
	Inherits FormParent
	''OKPublic FlagConsulta As Boolean
	Private FlagDrag As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		GetAllNodes(trvEstructura).Clear()
	End Sub
	
	Private Sub cmdCarregar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCarregar.ClickEvent
		MontaArbre()
		If txtEmpresa.Text <> "" And cmbNivellFinal.Columns(1).Text <> "" Then
			txtEmpresa.Enabled = False
			cmbNivellFinal.Enabled = False
		End If
	End Sub
	
	Private Sub cmdCerrar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCerrar.ClickEvent
		Me.Unload()
	End Sub
	
	Private Sub frmNivellsEstructura_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmNivellsEstructura_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmNivellsEstructura_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		IniciForm(Me)
		Text2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
		DefinirGrids()
		cmdCarregar.Picture = SetIcon("Grid2")
		'UPGRADE_ISSUE: VBControlExtender propiedad grdCentresCost.DragIcon no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
''MIGRAR		grdCentresCost.DragIcon = SetIcon("New", 16)
		CarregaComboNou(Me, cmbNivellFinal)
	End Sub
	
	Private Sub grdCentresCost_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdCentresCost.MouseMove
		If grdCentresCost.Rows < 2 Then Exit Sub
		If eventArgs.Button = MouseButtons.Left And FlagDrag = False Then
			FlagDrag = True
			'UPGRADE_ISSUE: No se actualiz� la constante vbBeginDrag. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'UPGRADE_ISSUE: VBControlExtender m�todo grdCentresCost.Drag no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			''MIGRARgrdCentresCost.Drag(vbBeginDrag)
		End If
		If eventArgs.Button <> MouseButtons.Left And FlagDrag = True Then
			FlagDrag = False
			'UPGRADE_ISSUE: No se actualiz� la constante vbEndDrag. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'UPGRADE_ISSUE: VBControlExtender m�todo grdCentresCost.Drag no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			''MIGRARgrdCentresCost.Drag(vbEndDrag)
		End If
	End Sub
	
	'UPGRADE_ISSUE: El evento MSComctlLib.TreeView trvEstructura.DragDrop no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub trvEstructura_DragDrop(ByRef Source As System.Windows.Forms.Control, ByRef X As Single, ByRef Y As Single)
		Dim Key As String
		Dim Centre As String
		Dim Nom As String
		Dim Fila As Integer
		'UPGRADE_ISSUE: MSComctlLib.TreeView propiedad trvEstructura.DropHighlight no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'UPGRADE_NOTE: El objeto trvEstructura.DropHighlight no se puede destruir hasta que no se realice la recolecci�n de los elementos no utilizados. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		DropHighlight = Nothing
		If trvEstructura.GetNodeAt(X, Y) Is Nothing = False Then
			Key = trvEstructura.GetNodeAt(X, Y).Name
			If LPiece(Key, "|") = 3 Then
				Fila = grdCentresCost.ActiveCell.Row
				Centre = grdCentresCost.Cell(Fila, 1).Text
				CacheNetejaParametres()
				MCache.P1 = txtEmpresa.Text
				MCache.P2 = Key
				MCache.P3 = Centre
				MCache.P4 = cmbNivellFinal.Columns(1).Text
				Nom = CacheXecute("D GRAB^ENT.G.INDIAUX")
				grdCentresCost.RemoveItem(Fila)
				grdCentresCost.Cell(Fila - 1, 2).SetFocus()
				'UPGRADE_WARNING: El comportamiento del m�todo Add ha cambiado Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DBD08912-7C17-401D-9BE9-BA85E7772B99"'
				GetAllNodes(trvEstructura).Find(Key, True)(0).Nodes.Add("I" & Centre, Nom, 3, 3)
				'UPGRADE_ISSUE: No se actualiz� la constante vbEndDrag. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
				'UPGRADE_ISSUE: VBControlExtender m�todo grdCentresCost.Drag no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				''MIGRARgrdCentresCost.Drag(vbEndDrag)
				MarcaRowFG(grdCentresCost, Fila - 1)
			End If
		End If
	End Sub
	
	'UPGRADE_ISSUE: El evento MSComctlLib.TreeView trvEstructura.DragOver no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub trvEstructura_DragOver(ByRef Source As System.Windows.Forms.Control, ByRef X As Single, ByRef Y As Single, ByRef State As Short)
		If Source.Name <> "grdCentresCost" Then Exit Sub
		'UPGRADE_ISSUE: MSComctlLib.TreeView propiedad trvEstructura.DropHighlight no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		DropHighlight = trvEstructura.GetNodeAt(X, Y)
	End Sub
	
	Private Sub trvEstructura_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles trvEstructura.MouseUp
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = VB6.PixelsToTwipsX(eventArgs.X)
		Dim Y As Single = VB6.PixelsToTwipsY(eventArgs.Y)
		If Button <> MouseButtons.Right Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("trvEstructura", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(gmsPopUpCode.PopMenuItemStyle.tsMenuCaption, "Eliminar",  , Strings.Left(trvEstructura.SelectedNode.Name, 1) = "I", XPIcon("Delete"),  ,  ,  ,  , "Del")
			End With
		End With
		XpExecutaMenu(Me, "trvEstructura")
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		Cancel = DisplayDescripcio(Me, eventSender)
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub MontaArbre()
		CarregaTreeView(trvEstructura, "CT^ENT.G.INDIAUX", txtEmpresa.Text & PC & cmbNivellFinal.Columns(1).Text, True)
		CarregaFGrid(grdCentresCost, "CG^ENT.G.INDIAUX", txtEmpresa.Text & PC & cmbNivellFinal.Columns(1).Text)
	End Sub
	
	Private Sub DefinirGrids()
		IniciFGrid(grdCentresCost, False)
		With grdCentresCost
			.Cols = 4
			.Rows = 1
			.SelectionMode = SelectionModeEnum.ByRow
.Locked = True
			
			.Cell(0, 2).Text = "Codi"
			.Cell(0, 3).Text = "Descripci�"
			
			.Column(2).Mask = MaskEnum.Numeric
			.Column(2).DecimalLength = 0
			.Column(2).FormatString = "#"
			
			.Column(0).Width = 0
			.Column(1).Width = 0
			.Column(2).Width = 60
			.Column(3).Width = 60
			.Column(2).Alignment = AlignmentEnum.CenterCenter
		End With
	End Sub
	
	'UPGRADE_NOTE: Menu se actualiz� a Menu_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub ResMenu(ByRef Menu As String, ByRef Key As String)
		Dim Resposta As String
		Dim Fila As Integer
		Select Case Menu
			Case "trvEstructura"
				Select Case Key
					Case "Del"
						CacheNetejaParametres()
						MCache.P1 = txtEmpresa.Text
						MCache.P2 = trvEstructura.SelectedNode.Parent.Name
						MCache.P3 = trvEstructura.SelectedNode.Name
						MCache.P4 = cmbNivellFinal.Columns(1).Text
						Resposta = CacheXecute("D DEL^ENT.G.INDIAUX")
						If Resposta <> "" Then
							'UPGRADE_WARNING: MSComctlLib.Nodes m�todo trvEstructura.Nodes.Remove tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
							GetAllNodes(trvEstructura).RemoveAt(trvEstructura.SelectedNode.Index)
							grdCentresCost.AddItem("")
							Fila = grdCentresCost.Rows - 1
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							grdCentresCost.Cell(Fila, 1).Text = Piece(Resposta, S, 1)
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							grdCentresCost.Cell(Fila, 2).Text = Piece(Resposta, S, 1)
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							grdCentresCost.Cell(Fila, 3).Text = Piece(Resposta, S, 2)
						End If
				End Select
		End Select
	End Sub
End Class
