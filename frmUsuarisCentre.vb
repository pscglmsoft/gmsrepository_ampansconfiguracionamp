Option Strict Off
Option Explicit On
Friend Class frmUsuarisCentre
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	
	Private Sub chkTodosCentros_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTodosCentros.ClickEvent
		If chkTodosCentros.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			FGCheckGrid(grdCentresTreball, True, 4)
			FGCheckGrid(grdDivisions, True, 4)
			chkVerAltasFuturas.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked
			chkVerBajas.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked = XtremeSuiteControls.CheckBoxValue.xtpChecked
		End If
	End Sub
	
	Private Sub chkVerAltasFuturas_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVerAltasFuturas.ClickEvent
		If chkVerAltasFuturas.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked Then chkTodosCentros.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
	End Sub
	
	Private Sub chkVerBajas_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVerBajas.ClickEvent
		If chkVerBajas.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked Then chkTodosCentros.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
	End Sub
	
	Private Sub cmdCancelar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelar.ClickEvent
		Inici()
	End Sub
	
	Private Sub grdCentresTreball_CellChange(ByVal eventSender As System.Object, ByVal eventArgs As Grid.CellChangeEventArgs) Handles grdCentresTreball.CellChange
		If eventArgs.Col <> 4 Then Exit Sub
		If grdCentresTreball.Cell(eventArgs.Row, eventArgs.Col).BooleanValue = False Then chkTodosCentros.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
	End Sub
	
	Private Sub grdCentresTreball_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdCentresTreball.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdCentresTreball", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Marca tots",  ,  , XPIcon("Plus"),  ,  ,  ,  , "M")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Desmarca tots",  ,  , XPIcon("Minus"),  ,  ,  ,  , "D")
			End With
		End With
		XpExecutaMenu(Me, "grdCentresTreball", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub grdCentresTreball_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdCentresTreball.Click
		MarcaRowFG(grdCentresTreball)
	End Sub
	
	Private Sub grdCentresTreball_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdCentresTreball.DoubleClick
		If grdCentresTreball.MouseRow < 1 Then Exit Sub
		If grdCentresTreball.ActiveCell.Row < 1 Then Exit Sub
		If grdCentresTreball.Cell(grdCentresTreball.ActiveCell.Row, 4).Text = "1" Then
			grdCentresTreball.Cell(grdCentresTreball.ActiveCell.Row, 4).Text = "0"
		Else
			grdCentresTreball.Cell(grdCentresTreball.ActiveCell.Row, 4).Text = "1"
		End If
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim a As String
		Dim I As Short
		
		Select Case NomMenu
			Case "grdCentresTreball"
				Select Case KeyMenu
					Case "M"
						For I = 1 To grdCentresTreball.Rows - 1
							grdCentresTreball.Cell(I, 4).Text = CStr(1)
						Next I
					Case "D"
						For I = 1 To grdCentresTreball.Rows - 1
							grdCentresTreball.Cell(I, 4).Text = CStr(0)
						Next I
				End Select
		End Select
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim I As Short
		Dim Nod As String
		If txtUsuari.Text <> "" Then
			CacheNetejaParametres()
			MCache.P1 = txtUsuari.Text
			MCache.P2 = txtEmpresa.Text
			MCache.P3 = FGMontaKeysSeleccio(grdCentresTreball, 4)
			MCache.P4 = FGMontaKeysSeleccio(grdDivisions, 4)
			MCache.P5 = IIf(chkTodosCentros.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked, 1, 0) & S & IIf(chkVerAltasFuturas.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked, 1, 0) & S & IIf(chkVerBajas.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked, 1, 0)
			CacheXecute("D GPermis^ENT.G.SEGURETAT")
		End If
		Inici()
	End Sub
	
	Private Sub frmUsuarisCentre_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuarisCentre_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	Private Sub frmUsuarisCentre_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		Text2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
	End Sub
	
	Public Overrides Sub Inici()
		txtUsuari.Enabled = True
		ResetForm(Me)
	End Sub
	
	Private Sub grdDivisions_CellChange(ByVal eventSender As System.Object, ByVal eventArgs As Grid.CellChangeEventArgs) Handles grdDivisions.CellChange
		If eventArgs.Col <> 4 Then Exit Sub
		If grdDivisions.Cell(eventArgs.Row, eventArgs.Col).BooleanValue = False Then chkTodosCentros.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		If txtEmpresa.Text <> "" And txtUsuari.Text <> "" Then
			txtEmpresa.Enabled = False
			txtUsuari.Enabled = False
			CarregaCentres()
		End If
	End Sub
	Private Sub txtUsuari_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.DoubleClick
		ConsultaTaula(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtUsuari_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.LostFocus
		DisplayDescripcio(Me, txtUsuari)
		If txtUsuari.Text <> "" And txtEmpresa.Text <> "" Then
			txtUsuari.Enabled = False
			txtEmpresa.Enabled = False
			CarregaCentres()
		End If
	End Sub
	
	Private Sub CarregaCentres()
		'UPGRADE_NOTE: Registre se actualiz� a Registre_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim Registre_Renamed As String
		CacheNetejaParametres()
		MCache.P1 = txtEmpresa.Text
		MCache.P2 = txtUsuari.Text
		Registre_Renamed = CacheXecute("S VALUE=$G(^ENT.SEGCD(P1,P2))")
		CarregaFGrid(grdCentresTreball, "GCentres^ENT.G.SEGURETAT", txtUsuari.Text & S & txtEmpresa.Text, CStr(2))
		CarregaFGrid(grdDivisions, "GDivisions^ENT.G.SEGURETAT", txtUsuari.Text & S & txtEmpresa.Text, CStr(2))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		chkTodosCentros.Value = Val(Piece(Registre_Renamed, S, 1))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		chkVerAltasFuturas.Value = Val(Piece(Registre_Renamed, S, 2))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		chkVerBajas.Value = Val(Piece(Registre_Renamed, S, 3))
	End Sub
End Class
