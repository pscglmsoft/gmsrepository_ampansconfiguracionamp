<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmUsuarisPerColaboradors
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents chkTancarFeines As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkTancarTasques As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkIncidencies As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkPermBloqFact As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkPermisModCCCC As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents TxtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtMail As System.Windows.Forms.TextBox
	Public WithEvents chkAdmin As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtCodigoCol As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtCodigoUsu As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUsuarisPerColaboradors))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.chkTancarFeines = New AxXtremeSuiteControls.AxCheckBox
		Me.chkTancarTasques = New AxXtremeSuiteControls.AxCheckBox
		Me.chkIncidencies = New AxXtremeSuiteControls.AxCheckBox
		Me.chkPermBloqFact = New AxXtremeSuiteControls.AxCheckBox
		Me.chkPermisModCCCC = New AxXtremeSuiteControls.AxCheckBox
		Me.TxtEmpresa = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtMail = New System.Windows.Forms.TextBox
		Me.chkAdmin = New AxXtremeSuiteControls.AxCheckBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtCodigoCol = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtCodigoUsu = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkTancarFeines, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkTancarTasques, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkIncidencies, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPermBloqFact, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPermisModCCCC, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkAdmin, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Usuaris per colaboradors"
		Me.ClientSize = New System.Drawing.Size(529, 284)
		Me.Location = New System.Drawing.Point(433, 283)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-USUxCOL"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmUsuarisPerColaboradors"
		chkTancarFeines.OcxState = CType(resources.GetObject("chkTancarFeines.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTancarFeines.Size = New System.Drawing.Size(289, 17)
		Me.chkTancarFeines.Location = New System.Drawing.Point(26, 220)
		Me.chkTancarFeines.TabIndex = 9
Me.chkTancarFeines.Tag = "10"
		Me.chkTancarFeines.Name = "chkTancarFeines"
		chkTancarTasques.OcxState = CType(resources.GetObject("chkTancarTasques.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTancarTasques.Size = New System.Drawing.Size(289, 17)
		Me.chkTancarTasques.Location = New System.Drawing.Point(26, 200)
		Me.chkTancarTasques.TabIndex = 8
Me.chkTancarTasques.Tag = "9"
		Me.chkTancarTasques.Name = "chkTancarTasques"
		chkIncidencies.OcxState = CType(resources.GetObject("chkIncidencies.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkIncidencies.Size = New System.Drawing.Size(289, 17)
		Me.chkIncidencies.Location = New System.Drawing.Point(26, 180)
		Me.chkIncidencies.TabIndex = 7
Me.chkIncidencies.Tag = "8"
		Me.chkIncidencies.Name = "chkIncidencies"
		chkPermBloqFact.OcxState = CType(resources.GetObject("chkPermBloqFact.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPermBloqFact.Size = New System.Drawing.Size(289, 17)
		Me.chkPermBloqFact.Location = New System.Drawing.Point(26, 160)
		Me.chkPermBloqFact.TabIndex = 6
Me.chkPermBloqFact.Tag = "7"
		Me.chkPermBloqFact.Name = "chkPermBloqFact"
		chkPermisModCCCC.OcxState = CType(resources.GetObject("chkPermisModCCCC.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPermisModCCCC.Size = New System.Drawing.Size(289, 17)
		Me.chkPermisModCCCC.Location = New System.Drawing.Point(26, 140)
		Me.chkPermisModCCCC.TabIndex = 5
Me.chkPermisModCCCC.Tag = "6"
		Me.chkPermisModCCCC.Name = "chkPermisModCCCC"
		Me.TxtEmpresa.AutoSize = False
		Me.TxtEmpresa.Size = New System.Drawing.Size(103, 19)
		Me.TxtEmpresa.Location = New System.Drawing.Point(140, 24)
		Me.TxtEmpresa.Maxlength = 20
		Me.TxtEmpresa.TabIndex = 0
		Me.TxtEmpresa.Tag = "*1"
		Me.TxtEmpresa.AcceptsReturn = True
		Me.TxtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.TxtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.TxtEmpresa.CausesValidation = True
		Me.TxtEmpresa.Enabled = True
		Me.TxtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.TxtEmpresa.HideSelection = True
		Me.TxtEmpresa.ReadOnly = False
		Me.TxtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.TxtEmpresa.MultiLine = False
		Me.TxtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.TxtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.TxtEmpresa.TabStop = True
		Me.TxtEmpresa.Visible = True
		Me.TxtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.TxtEmpresa.Name = "TxtEmpresa"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(259, 19)
		Me.Text3.Location = New System.Drawing.Point(246, 24)
		Me.Text3.TabIndex = 18
		Me.Text3.Tag = "^1"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtMail.AutoSize = False
		Me.txtMail.Size = New System.Drawing.Size(364, 19)
		Me.txtMail.Location = New System.Drawing.Point(140, 96)
		Me.txtMail.Maxlength = 35
		Me.txtMail.TabIndex = 3
		Me.txtMail.Tag = "5"
		Me.txtMail.AcceptsReturn = True
		Me.txtMail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMail.BackColor = System.Drawing.SystemColors.Window
		Me.txtMail.CausesValidation = True
		Me.txtMail.Enabled = True
		Me.txtMail.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMail.HideSelection = True
		Me.txtMail.ReadOnly = False
		Me.txtMail.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMail.MultiLine = False
		Me.txtMail.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMail.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMail.TabStop = True
		Me.txtMail.Visible = True
		Me.txtMail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMail.Name = "txtMail"
		chkAdmin.OcxState = CType(resources.GetObject("chkAdmin.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkAdmin.Size = New System.Drawing.Size(289, 17)
		Me.chkAdmin.Location = New System.Drawing.Point(26, 120)
		Me.chkAdmin.TabIndex = 4
Me.chkAdmin.Tag = "4"
		Me.chkAdmin.Name = "chkAdmin"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(259, 19)
		Me.Text2.Location = New System.Drawing.Point(246, 72)
		Me.Text2.TabIndex = 16
		Me.Text2.Tag = "^3"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtCodigoCol.AutoSize = False
		Me.txtCodigoCol.Size = New System.Drawing.Size(104, 19)
		Me.txtCodigoCol.Location = New System.Drawing.Point(140, 72)
		Me.txtCodigoCol.Maxlength = 10
		Me.txtCodigoCol.TabIndex = 2
		Me.txtCodigoCol.Tag = "3"
		Me.txtCodigoCol.AcceptsReturn = True
		Me.txtCodigoCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoCol.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoCol.CausesValidation = True
		Me.txtCodigoCol.Enabled = True
		Me.txtCodigoCol.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoCol.HideSelection = True
		Me.txtCodigoCol.ReadOnly = False
		Me.txtCodigoCol.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoCol.MultiLine = False
		Me.txtCodigoCol.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoCol.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoCol.TabStop = True
		Me.txtCodigoCol.Visible = True
		Me.txtCodigoCol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoCol.Name = "txtCodigoCol"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(259, 19)
		Me.Text1.Location = New System.Drawing.Point(246, 48)
		Me.Text1.TabIndex = 14
		Me.Text1.Tag = "^2"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtCodigoUsu.AutoSize = False
		Me.txtCodigoUsu.Size = New System.Drawing.Size(103, 19)
		Me.txtCodigoUsu.Location = New System.Drawing.Point(140, 48)
		Me.txtCodigoUsu.Maxlength = 20
		Me.txtCodigoUsu.TabIndex = 1
		Me.txtCodigoUsu.Tag = "*2"
		Me.txtCodigoUsu.AcceptsReturn = True
		Me.txtCodigoUsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoUsu.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoUsu.CausesValidation = True
		Me.txtCodigoUsu.Enabled = True
		Me.txtCodigoUsu.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoUsu.HideSelection = True
		Me.txtCodigoUsu.ReadOnly = False
		Me.txtCodigoUsu.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoUsu.MultiLine = False
		Me.txtCodigoUsu.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoUsu.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoUsu.TabStop = True
		Me.txtCodigoUsu.Visible = True
		Me.txtCodigoUsu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoUsu.Name = "txtCodigoUsu"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(330, 250)
		Me.cmdAceptar.TabIndex = 10
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(420, 250)
		Me.cmdGuardar.TabIndex = 11
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "Empresa"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(26, 28)
		Me.Label1.TabIndex = 19
		Me.Label1.Tag = "1"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl4.Text = "Mail"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(26, 100)
		Me.lbl4.TabIndex = 17
		Me.lbl4.Tag = "5"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl2.Text = "Codigo col"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(26, 76)
		Me.lbl2.TabIndex = 15
		Me.lbl2.Tag = "3"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl1.Text = "Codigo usu"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(26, 52)
		Me.lbl1.TabIndex = 13
		Me.lbl1.Tag = "2"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(32, 250)
		Me.lblLock.TabIndex = 12
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 32
		Me.Line1.X2 = 91
		Me.Line1.Y1 = 244
		Me.Line1.Y2 = 244
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 32
		Me.Line2.X2 = 91
		Me.Line2.Y1 = 245
		Me.Line2.Y2 = 245
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkAdmin, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPermisModCCCC, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPermBloqFact, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkIncidencies, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkTancarTasques, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkTancarFeines, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(chkTancarFeines)
		Me.Controls.Add(chkTancarTasques)
		Me.Controls.Add(chkIncidencies)
		Me.Controls.Add(chkPermBloqFact)
		Me.Controls.Add(chkPermisModCCCC)
		Me.Controls.Add(TxtEmpresa)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtMail)
		Me.Controls.Add(chkAdmin)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtCodigoCol)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtCodigoUsu)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
