<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParametresERP
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = MDI
		MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public WithEvents txtMarges As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtsucursal As System.Windows.Forms.TextBox
	Public WithEvents txtClient As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtArticle As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtSubactivitat As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtPathFicheros As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdExplorar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParametresERP))
		Me.components = New System.ComponentModel.Container()
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtMarges = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtsucursal = New System.Windows.Forms.TextBox
		Me.txtClient = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtArticle = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtSubactivitat = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtPathFicheros = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdExplorar = New AxXtremeSuiteControls.AxPushButton
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdExplorar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Par�metros Gesti�n CET"
		Me.ClientSize = New System.Drawing.Size(562, 237)
		Me.Location = New System.Drawing.Point(373, 238)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Tag = "I-ERP"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParametresERP"
		Me.txtMarges.AutoSize = False
		Me.txtMarges.Size = New System.Drawing.Size(90, 19)
		Me.txtMarges.Location = New System.Drawing.Point(192, 162)
		Me.txtMarges.Maxlength = 5
		Me.txtMarges.TabIndex = 22
		Me.txtMarges.Tag = "7"
		Me.txtMarges.AcceptsReturn = True
		Me.txtMarges.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMarges.BackColor = System.Drawing.SystemColors.Window
		Me.txtMarges.CausesValidation = True
		Me.txtMarges.Enabled = True
		Me.txtMarges.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMarges.HideSelection = True
		Me.txtMarges.ReadOnly = False
		Me.txtMarges.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMarges.MultiLine = False
		Me.txtMarges.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMarges.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMarges.TabStop = True
		Me.txtMarges.Visible = True
		Me.txtMarges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMarges.Name = "txtMarges"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(236, 19)
		Me.Text4.Location = New System.Drawing.Point(285, 162)
		Me.Text4.TabIndex = 21
		Me.Text4.Tag = "^7"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(236, 19)
		Me.Text6.Location = New System.Drawing.Point(285, 138)
		Me.Text6.TabIndex = 19
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtsucursal.AutoSize = False
		Me.txtsucursal.Size = New System.Drawing.Size(90, 19)
		Me.txtsucursal.Location = New System.Drawing.Point(192, 138)
		Me.txtsucursal.Maxlength = 5
		Me.txtsucursal.TabIndex = 18
		Me.txtsucursal.Tag = "6"
		Me.txtsucursal.AcceptsReturn = True
		Me.txtsucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtsucursal.BackColor = System.Drawing.SystemColors.Window
		Me.txtsucursal.CausesValidation = True
		Me.txtsucursal.Enabled = True
		Me.txtsucursal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtsucursal.HideSelection = True
		Me.txtsucursal.ReadOnly = False
		Me.txtsucursal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtsucursal.MultiLine = False
		Me.txtsucursal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtsucursal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtsucursal.TabStop = True
		Me.txtsucursal.Visible = True
		Me.txtsucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtsucursal.Name = "txtsucursal"
		Me.txtClient.AutoSize = False
		Me.txtClient.Size = New System.Drawing.Size(90, 19)
		Me.txtClient.Location = New System.Drawing.Point(192, 114)
		Me.txtClient.Maxlength = 5
		Me.txtClient.TabIndex = 16
		Me.txtClient.Tag = "5"
		Me.txtClient.AcceptsReturn = True
		Me.txtClient.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtClient.BackColor = System.Drawing.SystemColors.Window
		Me.txtClient.CausesValidation = True
		Me.txtClient.Enabled = True
		Me.txtClient.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClient.HideSelection = True
		Me.txtClient.ReadOnly = False
		Me.txtClient.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClient.MultiLine = False
		Me.txtClient.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClient.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtClient.TabStop = True
		Me.txtClient.Visible = True
		Me.txtClient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtClient.Name = "txtClient"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(236, 19)
		Me.Text2.Location = New System.Drawing.Point(285, 114)
		Me.Text2.TabIndex = 15
		Me.Text2.Tag = "^5"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(236, 19)
		Me.Text5.Location = New System.Drawing.Point(285, 90)
		Me.Text5.TabIndex = 13
		Me.Text5.Tag = "^4"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtArticle.AutoSize = False
		Me.txtArticle.Size = New System.Drawing.Size(90, 19)
		Me.txtArticle.Location = New System.Drawing.Point(192, 90)
		Me.txtArticle.Maxlength = 5
		Me.txtArticle.TabIndex = 4
		Me.txtArticle.Tag = "4"
		Me.txtArticle.AcceptsReturn = True
		Me.txtArticle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtArticle.BackColor = System.Drawing.SystemColors.Window
		Me.txtArticle.CausesValidation = True
		Me.txtArticle.Enabled = True
		Me.txtArticle.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtArticle.HideSelection = True
		Me.txtArticle.ReadOnly = False
		Me.txtArticle.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtArticle.MultiLine = False
		Me.txtArticle.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtArticle.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtArticle.TabStop = True
		Me.txtArticle.Visible = True
		Me.txtArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtArticle.Name = "txtArticle"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(274, 19)
		Me.Text3.Location = New System.Drawing.Point(247, 66)
		Me.Text3.TabIndex = 11
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtSubactivitat.AutoSize = False
		Me.txtSubactivitat.Size = New System.Drawing.Size(52, 19)
		Me.txtSubactivitat.Location = New System.Drawing.Point(192, 66)
		Me.txtSubactivitat.Maxlength = 5
		Me.txtSubactivitat.TabIndex = 3
		Me.txtSubactivitat.Tag = "3"
		Me.txtSubactivitat.AcceptsReturn = True
		Me.txtSubactivitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubactivitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubactivitat.CausesValidation = True
		Me.txtSubactivitat.Enabled = True
		Me.txtSubactivitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubactivitat.HideSelection = True
		Me.txtSubactivitat.ReadOnly = False
		Me.txtSubactivitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubactivitat.MultiLine = False
		Me.txtSubactivitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubactivitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubactivitat.TabStop = True
		Me.txtSubactivitat.Visible = True
		Me.txtSubactivitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubactivitat.Name = "txtSubactivitat"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(52, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(192, 19)
		Me.txtEmpresa.Maxlength = 5
		Me.txtEmpresa.TabIndex = 1
		Me.txtEmpresa.Tag = "*1"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(274, 19)
		Me.Text1.Location = New System.Drawing.Point(247, 19)
		Me.Text1.TabIndex = 5
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtPathFicheros.AutoSize = False
		Me.txtPathFicheros.Size = New System.Drawing.Size(329, 19)
		Me.txtPathFicheros.Location = New System.Drawing.Point(192, 43)
		Me.txtPathFicheros.Maxlength = 50
		Me.txtPathFicheros.TabIndex = 2
		Me.txtPathFicheros.Tag = "2"
		Me.txtPathFicheros.AcceptsReturn = True
		Me.txtPathFicheros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPathFicheros.BackColor = System.Drawing.SystemColors.Window
		Me.txtPathFicheros.CausesValidation = True
		Me.txtPathFicheros.Enabled = True
		Me.txtPathFicheros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPathFicheros.HideSelection = True
		Me.txtPathFicheros.ReadOnly = False
		Me.txtPathFicheros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPathFicheros.MultiLine = False
		Me.txtPathFicheros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPathFicheros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPathFicheros.TabStop = True
		Me.txtPathFicheros.Visible = True
		Me.txtPathFicheros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPathFicheros.Name = "txtPathFicheros"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(348, 201)
		Me.cmdAceptar.TabIndex = 7
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(438, 201)
		Me.cmdGuardar.TabIndex = 8
		Me.cmdGuardar.Name = "cmdGuardar"
		cmdExplorar.OcxState = CType(resources.GetObject("cmdExplorar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdExplorar.Size = New System.Drawing.Size(27, 27)
		Me.cmdExplorar.Location = New System.Drawing.Point(524, 40)
		Me.cmdExplorar.TabIndex = 10
		Me.cmdExplorar.Name = "cmdExplorar"
		Me.Label5.Text = "Taula de Marges"
		Me.Label5.Size = New System.Drawing.Size(111, 15)
		Me.Label5.Location = New System.Drawing.Point(10, 166)
		Me.Label5.TabIndex = 23
		Me.Label5.Tag = "7"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Sucursal"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(10, 142)
		Me.Label4.TabIndex = 20
		Me.Label4.Tag = "6"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "Client"
		Me.Label3.Size = New System.Drawing.Size(111, 15)
		Me.Label3.Location = New System.Drawing.Point(10, 118)
		Me.Label3.TabIndex = 17
		Me.Label3.Tag = "5"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Article"
		Me.Label2.Size = New System.Drawing.Size(111, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 94)
		Me.Label2.TabIndex = 14
		Me.Label2.Tag = "4"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Subactivitat"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 70)
		Me.Label1.TabIndex = 12
		Me.Label1.Tag = "3"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 6
		Me.Line2.X2 = 65
		Me.Line2.Y1 = 191
		Me.Line2.Y2 = 191
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 6
		Me.Line1.X2 = 65
		Me.Line1.Y1 = 190
		Me.Line1.Y2 = 190
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.lbl1.Text = "Empresa"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 23)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Path documents adjunts pressupost"
		Me.lbl2.Size = New System.Drawing.Size(177, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 47)
		Me.lbl2.TabIndex = 6
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 198)
		Me.lblLock.TabIndex = 9
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		CType(Me.cmdExplorar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtMarges)
		Me.Controls.Add(Text4)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtsucursal)
		Me.Controls.Add(txtClient)
		Me.Controls.Add(Text2)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtArticle)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtSubactivitat)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtPathFicheros)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(cmdExplorar)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lblLock)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
